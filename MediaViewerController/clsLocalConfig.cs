﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OntologyAppDBConnector;
using ImportExport_Module;
using OntologyClasses.BaseClasses;
using System.Reflection;
using OntologyClasses.Interfaces;
using System.Runtime.InteropServices;

namespace MediaViewerController
{
    public class clsLocalConfig : ILocalConfig
    {
    private const string cstrID_Ontology = "1ef425afe14e4be7a4a2ea2926fe0f8d";
    private ImportWorker objImport;

    public Globals Globals { get; set; }

    private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
    public clsOntologyItem OItem_BaseConfig { get; set; }

    private OntologyModDBConnector objDBLevel_Config1;
    private OntologyModDBConnector objDBLevel_Config2;

        //Attributes
    public clsOntologyItem OItem_attribute_comment { get; set; }
    public clsOntologyItem OItem_attribute_datetimestamp { get; set; }
    public clsOntologyItem OItem_attribute_datetimestamp__create_ { get; set; }
    public clsOntologyItem OItem_attribute_dbpostfix { get; set; }
    public clsOntologyItem OItem_attribute_id { get; set; }
    public clsOntologyItem OItem_attribute_l_nge__minuten_ { get; set; }
    public clsOntologyItem OItem_attribute_media_position { get; set; }
    public clsOntologyItem OItem_attribute_taking { get; set; }
    public clsOntologyItem OItem_attribute_titel { get; set; }
    public clsOntologyItem OItem_attribute_xml_text { get; set; }
        public clsOntologyItem OItem_attributetype_second { get; set; }

        public clsOntologyItem OItem_type_album { get; set; }
        public clsOntologyItem OItem_type_band { get; set; }
        public clsOntologyItem OItem_type_bauwerke { get; set; }
        public clsOntologyItem OItem_type_day { get; set; }
        public clsOntologyItem OItem_type_file { get; set; }
        public clsOntologyItem OItem_type_filetypes { get; set; }
        public clsOntologyItem OItem_type_genre { get; set; }
        public clsOntologyItem OItem_type_haustier { get; set; }
        public clsOntologyItem OItem_type_image_module { get; set; }
        public clsOntologyItem OItem_type_image_objects { get; set; }
        public clsOntologyItem OItem_type_image_objects__no_objects_ { get; set; }
        public clsOntologyItem OItem_type_images__graphic_ { get; set; }
        public clsOntologyItem OItem_type_kunst { get; set; }
        public clsOntologyItem OItem_type_landscape { get; set; }
        public clsOntologyItem OItem_type_language { get; set; }
        public clsOntologyItem OItem_type_logentry { get; set; }
        public clsOntologyItem OItem_type_media { get; set; }
        public clsOntologyItem OItem_type_media_item { get; set; }
        public clsOntologyItem OItem_type_media_item_bookmark { get; set; }
        public clsOntologyItem OItem_type_media_item_range { get; set; }
        public clsOntologyItem OItem_type_module { get; set; }
        public clsOntologyItem OItem_type_month { get; set; }
        public clsOntologyItem OItem_type_mp3_file { get; set; }
        public clsOntologyItem OItem_type_ort { get; set; }
        public clsOntologyItem OItem_type_partner { get; set; }
        public clsOntologyItem OItem_type_pdf_documents { get; set; }
        public clsOntologyItem OItem_type_pflanzenarten { get; set; }
        public clsOntologyItem OItem_type_search_template { get; set; }
        public clsOntologyItem OItem_type_tierarten { get; set; }
        public clsOntologyItem OItem_type_url { get; set; }
        public clsOntologyItem OItem_type_user { get; set; }
        public clsOntologyItem OItem_type_wetterlage { get; set; }
        public clsOntologyItem OItem_type_wichtige_ereignisse { get; set; }
        public clsOntologyItem OItem_type_year { get; set; }
        public clsOntologyItem OItem_class_logstate { get; set; }

        //Classes
        public clsOntologyItem OItem_class_media_item_objects { get; set; }

        //RelationTypes
    public clsOntologyItem OItem_relationtype_artist { get; set; }
    public clsOntologyItem OItem_relationtype_belonging { get; set; }
    public clsOntologyItem OItem_relationtype_belonging_done { get; set; }
    public clsOntologyItem OItem_relationtype_belonging_source { get; set; }
    public clsOntologyItem OItem_relationtype_belongsto { get; set; }
    public clsOntologyItem OItem_relationtype_composer { get; set; }
    public clsOntologyItem OItem_relationtype_contains { get; set; }
    public clsOntologyItem OItem_relationtype_disc { get; set; }
    public clsOntologyItem OItem_relationtype_finished_with { get; set; }
    public clsOntologyItem OItem_relationtype_from { get; set; }
    public clsOntologyItem OItem_relationtype_has { get; set; }
    public clsOntologyItem OItem_relationtype_is { get; set; }
    public clsOntologyItem OItem_relationtype_is_of_type { get; set; }
    public clsOntologyItem OItem_relationtype_is_used_by { get; set; }
    public clsOntologyItem OItem_relationtype_isdescribedby { get; set; }
    public clsOntologyItem OItem_relationtype_located_in { get; set; }
    public clsOntologyItem OItem_relationtype_media_webservice_url { get; set; }
    public clsOntologyItem OItem_relationtype_offered_by { get; set; }
    public clsOntologyItem OItem_relationtype_offers { get; set; }
    public clsOntologyItem OItem_relationtype_started_with { get; set; }
    public clsOntologyItem OItem_relationtype_taking_at { get; set; }
    public clsOntologyItem OItem_relationtype_wascreatedby { get; set; }

        //objects
    public clsOntologyItem OItem_token_extensions_image { get; set; }
    public clsOntologyItem OItem_token_extensions_mod { get; set; }
    public clsOntologyItem OItem_token_extensions_pdf { get; set; }
    public clsOntologyItem OItem_token_image_objects__no_objects__no_address { get; set; }
    public clsOntologyItem OItem_token_image_objects__no_objects__no_artwork { get; set; }
    public clsOntologyItem OItem_token_image_objects__no_objects__no_buildings { get; set; }
    public clsOntologyItem OItem_token_image_objects__no_objects__no_landscape { get; set; }
    public clsOntologyItem OItem_token_image_objects__no_objects__no_location { get; set; }
    public clsOntologyItem OItem_token_image_objects__no_objects__no_persons { get; set; }
    public clsOntologyItem OItem_token_image_objects__no_objects__no_pets { get; set; }
    public clsOntologyItem OItem_token_image_objects__no_objects__no_plant_species { get; set; }
    public clsOntologyItem OItem_token_image_objects__no_objects__no_species { get; set; }
    public clsOntologyItem OItem_token_image_objects__no_objects__no_symbol { get; set; }
    public clsOntologyItem OItem_token_image_objects__no_objects__weather_condition { get; set; }
    public clsOntologyItem OItem_token_logstate_last_position { get; set; }
    public clsOntologyItem OItem_token_logstate_pause { get; set; }
    public clsOntologyItem OItem_token_logstate_start { get; set; }
    public clsOntologyItem OItem_token_logstate_stop { get; set; }
    public clsOntologyItem OItem_token_logstate_unassigned { get; set; }
    public clsOntologyItem OItem_token_logstate_user_defined { get; set; }
    public clsOntologyItem OItem_token_search_template_name_ { get; set; }
    public clsOntologyItem OItem_token_variable_itemcount { get; set; }
    public clsOntologyItem OItem_token_variable_mediasrc { get; set; }
    public clsOntologyItem OItem_token_variable_title { get; set; }
    public clsOntologyItem OItem_token_variable_url_mediasrc { get; set; }
    public clsOntologyItem OItem_token_xml_windows_playlist_1_0 { get; set; }
    public clsOntologyItem OItem_token_xml_windows_playlist_1_0_mediasrc { get; set; }
        public clsOntologyItem OItem_object_position { get; set; }




        private void get_Data_DevelopmentConfig()
    {
        var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology,
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID,
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

        var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
        if (objOItem_Result.GUID == Globals.LState_Success.GUID)
        {
            if (objDBLevel_Config1.ObjectRels.Any())
            {

                objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingAttribute.GUID
                }).ToList();

                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingClass.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingObject.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                }));

                objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
                if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                {
                    if (!objDBLevel_Config2.ObjectRels.Any())
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }
            }
            else
            {
                throw new Exception("Config-Error");
            }

        }

    }

    public clsLocalConfig()
    {
        Globals = new Globals();
        set_DBConnection();
        get_Config();
    }

    public clsLocalConfig(Globals Globals)
    {
        this.Globals = Globals;
        set_DBConnection();
        get_Config();
    }

    private void set_DBConnection()
    {
        objDBLevel_Config1 = new OntologyModDBConnector(Globals);
        objDBLevel_Config2 = new OntologyModDBConnector(Globals);
        objImport = new ImportWorker(Globals);
    }

    private void get_Config()
    {
        try
        {
            get_Data_DevelopmentConfig();
            get_Config_AttributeTypes();
            get_Config_RelationTypes();
            get_Config_Classes();
            get_Config_Objects();

                objDBLevel_Config1 = null;
                objDBLevel_Config2 = null;
            }
        catch (Exception ex)
        {
            var objAssembly = Assembly.GetExecutingAssembly();
            AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            var strTitle = "Unbekannt";
            if (objCustomAttributes.Length == 1)
            {
                strTitle = objCustomAttributes.First().Title;
            }
            
            var objOItem_Result = objImport.ImportTemplates(objAssembly);
            if (objOItem_Result.GUID != Globals.LState_Error.GUID)
            {
                get_Data_DevelopmentConfig();
                get_Config_AttributeTypes();
                get_Config_RelationTypes();
                get_Config_Classes();
                get_Config_Objects();
            }
            else
            {
                throw new Exception("Config not importable");
            }
            
        }
    }

    private void get_Config_AttributeTypes()
    {
            var objOList_attributetype_second = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "attributetype_second".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                 select objRef).ToList();

            if (objOList_attributetype_second.Any())
            {
                OItem_attributetype_second = new clsOntologyItem()
                {
                    GUID = objOList_attributetype_second.First().ID_Other,
                    Name = objOList_attributetype_second.First().Name_Other,
                    GUID_Parent = objOList_attributetype_second.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_comment = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "attribute_comment".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                          select objRef).ToList();

        if (objOList_attribute_comment.Any())
        {
            OItem_attribute_comment = new clsOntologyItem()
            {
                GUID = objOList_attribute_comment.First().ID_Other,
                Name = objOList_attribute_comment.First().Name_Other,
                GUID_Parent = objOList_attribute_comment.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_datetimestamp = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "attribute_datetimestamp".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                select objRef).ToList();

        if (objOList_attribute_datetimestamp.Any())
        {
            OItem_attribute_datetimestamp = new clsOntologyItem()
            {
                GUID = objOList_attribute_datetimestamp.First().ID_Other,
                Name = objOList_attribute_datetimestamp.First().Name_Other,
                GUID_Parent = objOList_attribute_datetimestamp.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_datetimestamp__create_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                         where objOItem.ID_Object == cstrID_Ontology
                                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                         where objRef.Name_Object.ToLower() == "attribute_datetimestamp__create_".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                         select objRef).ToList();

        if (objOList_attribute_datetimestamp__create_.Any())
        {
            OItem_attribute_datetimestamp__create_ = new clsOntologyItem()
            {
                GUID = objOList_attribute_datetimestamp__create_.First().ID_Other,
                Name = objOList_attribute_datetimestamp__create_.First().Name_Other,
                GUID_Parent = objOList_attribute_datetimestamp__create_.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_dbpostfix = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "attribute_dbpostfix".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                            select objRef).ToList();

        if (objOList_attribute_dbpostfix.Any())
        {
            OItem_attribute_dbpostfix = new clsOntologyItem()
            {
                GUID = objOList_attribute_dbpostfix.First().ID_Other,
                Name = objOList_attribute_dbpostfix.First().Name_Other,
                GUID_Parent = objOList_attribute_dbpostfix.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_id = (from objOItem in objDBLevel_Config1.ObjectRels
                                     where objOItem.ID_Object == cstrID_Ontology
                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                     where objRef.Name_Object.ToLower() == "attribute_id".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                     select objRef).ToList();

        if (objOList_attribute_id.Any())
        {
            OItem_attribute_id = new clsOntologyItem()
            {
                GUID = objOList_attribute_id.First().ID_Other,
                Name = objOList_attribute_id.First().Name_Other,
                GUID_Parent = objOList_attribute_id.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_l_nge__minuten_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "attribute_l_nge__minuten_".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                  select objRef).ToList();

        if (objOList_attribute_l_nge__minuten_.Any())
        {
            OItem_attribute_l_nge__minuten_ = new clsOntologyItem()
            {
                GUID = objOList_attribute_l_nge__minuten_.First().ID_Other,
                Name = objOList_attribute_l_nge__minuten_.First().Name_Other,
                GUID_Parent = objOList_attribute_l_nge__minuten_.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_media_position = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "attribute_media_position".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                 select objRef).ToList();

        if (objOList_attribute_media_position.Any())
        {
            OItem_attribute_media_position = new clsOntologyItem()
            {
                GUID = objOList_attribute_media_position.First().ID_Other,
                Name = objOList_attribute_media_position.First().Name_Other,
                GUID_Parent = objOList_attribute_media_position.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_taking = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "attribute_taking".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                         select objRef).ToList();

        if (objOList_attribute_taking.Any())
        {
            OItem_attribute_taking = new clsOntologyItem()
            {
                GUID = objOList_attribute_taking.First().ID_Other,
                Name = objOList_attribute_taking.First().Name_Other,
                GUID_Parent = objOList_attribute_taking.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_titel = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "attribute_titel".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                        select objRef).ToList();

        if (objOList_attribute_titel.Any())
        {
            OItem_attribute_titel = new clsOntologyItem()
            {
                GUID = objOList_attribute_titel.First().ID_Other,
                Name = objOList_attribute_titel.First().Name_Other,
                GUID_Parent = objOList_attribute_titel.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_xml_text = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "attribute_xml_text".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                           select objRef).ToList();

        if (objOList_attribute_xml_text.Any())
        {
            OItem_attribute_xml_text = new clsOntologyItem()
            {
                GUID = objOList_attribute_xml_text.First().ID_Other,
                Name = objOList_attribute_xml_text.First().Name_Other,
                GUID_Parent = objOList_attribute_xml_text.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_RelationTypes()
    {
        var objOList_relationtype_artist = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "relationtype_artist".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                            select objRef).ToList();

        if (objOList_relationtype_artist.Any())
        {
            OItem_relationtype_artist = new clsOntologyItem()
            {
                GUID = objOList_relationtype_artist.First().ID_Other,
                Name = objOList_relationtype_artist.First().Name_Other,
                GUID_Parent = objOList_relationtype_artist.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belonging = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_belonging".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_belonging.Any())
        {
            OItem_relationtype_belonging = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belonging.First().ID_Other,
                Name = objOList_relationtype_belonging.First().Name_Other,
                GUID_Parent = objOList_relationtype_belonging.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belonging_done = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_belonging_done".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

        if (objOList_relationtype_belonging_done.Any())
        {
            OItem_relationtype_belonging_done = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belonging_done.First().ID_Other,
                Name = objOList_relationtype_belonging_done.First().Name_Other,
                GUID_Parent = objOList_relationtype_belonging_done.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belonging_source = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "relationtype_belonging_source".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                      select objRef).ToList();

        if (objOList_relationtype_belonging_source.Any())
        {
            OItem_relationtype_belonging_source = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belonging_source.First().ID_Other,
                Name = objOList_relationtype_belonging_source.First().Name_Other,
                GUID_Parent = objOList_relationtype_belonging_source.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belongsto = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_belongsto".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_belongsto.Any())
        {
            OItem_relationtype_belongsto = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belongsto.First().ID_Other,
                Name = objOList_relationtype_belongsto.First().Name_Other,
                GUID_Parent = objOList_relationtype_belongsto.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_composer = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_composer".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

        if (objOList_relationtype_composer.Any())
        {
            OItem_relationtype_composer = new clsOntologyItem()
            {
                GUID = objOList_relationtype_composer.First().ID_Other,
                Name = objOList_relationtype_composer.First().Name_Other,
                GUID_Parent = objOList_relationtype_composer.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_contains = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_contains".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

        if (objOList_relationtype_contains.Any())
        {
            OItem_relationtype_contains = new clsOntologyItem()
            {
                GUID = objOList_relationtype_contains.First().ID_Other,
                Name = objOList_relationtype_contains.First().Name_Other,
                GUID_Parent = objOList_relationtype_contains.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_disc = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "relationtype_disc".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                          select objRef).ToList();

        if (objOList_relationtype_disc.Any())
        {
            OItem_relationtype_disc = new clsOntologyItem()
            {
                GUID = objOList_relationtype_disc.First().ID_Other,
                Name = objOList_relationtype_disc.First().Name_Other,
                GUID_Parent = objOList_relationtype_disc.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_finished_with = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_finished_with".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

        if (objOList_relationtype_finished_with.Any())
        {
            OItem_relationtype_finished_with = new clsOntologyItem()
            {
                GUID = objOList_relationtype_finished_with.First().ID_Other,
                Name = objOList_relationtype_finished_with.First().Name_Other,
                GUID_Parent = objOList_relationtype_finished_with.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_from = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "relationtype_from".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                          select objRef).ToList();

        if (objOList_relationtype_from.Any())
        {
            OItem_relationtype_from = new clsOntologyItem()
            {
                GUID = objOList_relationtype_from.First().ID_Other,
                Name = objOList_relationtype_from.First().Name_Other,
                GUID_Parent = objOList_relationtype_from.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_has = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "relationtype_has".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                         select objRef).ToList();

        if (objOList_relationtype_has.Any())
        {
            OItem_relationtype_has = new clsOntologyItem()
            {
                GUID = objOList_relationtype_has.First().ID_Other,
                Name = objOList_relationtype_has.First().Name_Other,
                GUID_Parent = objOList_relationtype_has.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_is = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "relationtype_is".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                        select objRef).ToList();

        if (objOList_relationtype_is.Any())
        {
            OItem_relationtype_is = new clsOntologyItem()
            {
                GUID = objOList_relationtype_is.First().ID_Other,
                Name = objOList_relationtype_is.First().Name_Other,
                GUID_Parent = objOList_relationtype_is.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_is_of_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_is_of_type".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_is_of_type.Any())
        {
            OItem_relationtype_is_of_type = new clsOntologyItem()
            {
                GUID = objOList_relationtype_is_of_type.First().ID_Other,
                Name = objOList_relationtype_is_of_type.First().Name_Other,
                GUID_Parent = objOList_relationtype_is_of_type.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_is_used_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_is_used_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_is_used_by.Any())
        {
            OItem_relationtype_is_used_by = new clsOntologyItem()
            {
                GUID = objOList_relationtype_is_used_by.First().ID_Other,
                Name = objOList_relationtype_is_used_by.First().Name_Other,
                GUID_Parent = objOList_relationtype_is_used_by.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_isdescribedby = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_isdescribedby".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

        if (objOList_relationtype_isdescribedby.Any())
        {
            OItem_relationtype_isdescribedby = new clsOntologyItem()
            {
                GUID = objOList_relationtype_isdescribedby.First().ID_Other,
                Name = objOList_relationtype_isdescribedby.First().Name_Other,
                GUID_Parent = objOList_relationtype_isdescribedby.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_located_in = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_located_in".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_located_in.Any())
        {
            OItem_relationtype_located_in = new clsOntologyItem()
            {
                GUID = objOList_relationtype_located_in.First().ID_Other,
                Name = objOList_relationtype_located_in.First().Name_Other,
                GUID_Parent = objOList_relationtype_located_in.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_media_webservice_url = (from objOItem in objDBLevel_Config1.ObjectRels
                                                          where objOItem.ID_Object == cstrID_Ontology
                                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                          where objRef.Name_Object.ToLower() == "relationtype_media_webservice_url".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                          select objRef).ToList();

        if (objOList_relationtype_media_webservice_url.Any())
        {
            OItem_relationtype_media_webservice_url = new clsOntologyItem()
            {
                GUID = objOList_relationtype_media_webservice_url.First().ID_Other,
                Name = objOList_relationtype_media_webservice_url.First().Name_Other,
                GUID_Parent = objOList_relationtype_media_webservice_url.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_offered_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_offered_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_offered_by.Any())
        {
            OItem_relationtype_offered_by = new clsOntologyItem()
            {
                GUID = objOList_relationtype_offered_by.First().ID_Other,
                Name = objOList_relationtype_offered_by.First().Name_Other,
                GUID_Parent = objOList_relationtype_offered_by.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_offers = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "relationtype_offers".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                            select objRef).ToList();

        if (objOList_relationtype_offers.Any())
        {
            OItem_relationtype_offers = new clsOntologyItem()
            {
                GUID = objOList_relationtype_offers.First().ID_Other,
                Name = objOList_relationtype_offers.First().Name_Other,
                GUID_Parent = objOList_relationtype_offers.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_started_with = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_started_with".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

        if (objOList_relationtype_started_with.Any())
        {
            OItem_relationtype_started_with = new clsOntologyItem()
            {
                GUID = objOList_relationtype_started_with.First().ID_Other,
                Name = objOList_relationtype_started_with.First().Name_Other,
                GUID_Parent = objOList_relationtype_started_with.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_taking_at = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_taking_at".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_taking_at.Any())
        {
            OItem_relationtype_taking_at = new clsOntologyItem()
            {
                GUID = objOList_relationtype_taking_at.First().ID_Other,
                Name = objOList_relationtype_taking_at.First().Name_Other,
                GUID_Parent = objOList_relationtype_taking_at.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_wascreatedby = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_wascreatedby".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

        if (objOList_relationtype_wascreatedby.Any())
        {
            OItem_relationtype_wascreatedby = new clsOntologyItem()
            {
                GUID = objOList_relationtype_wascreatedby.First().ID_Other,
                Name = objOList_relationtype_wascreatedby.First().Name_Other,
                GUID_Parent = objOList_relationtype_wascreatedby.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_Objects()
    {
            var objOList_object_position = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "object_position".ToLower() && objRef.Ontology == Globals.Type_Object
                                            select objRef).ToList();

            if (objOList_object_position.Any())
            {
                OItem_object_position = new clsOntologyItem()
                {
                    GUID = objOList_object_position.First().ID_Other,
                    Name = objOList_object_position.First().Name_Other,
                    GUID_Parent = objOList_object_position.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }


            var objOList_token_extensions_image = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "token_extensions_image".ToLower() && objRef.Ontology == Globals.Type_Object
                                               select objRef).ToList();

        if (objOList_token_extensions_image.Any())
        {
            OItem_token_extensions_image = new clsOntologyItem()
            {
                GUID = objOList_token_extensions_image.First().ID_Other,
                Name = objOList_token_extensions_image.First().Name_Other,
                GUID_Parent = objOList_token_extensions_image.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_extensions_mod = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "token_extensions_mod".ToLower() && objRef.Ontology == Globals.Type_Object
                                             select objRef).ToList();

        if (objOList_token_extensions_mod.Any())
        {
            OItem_token_extensions_mod = new clsOntologyItem()
            {
                GUID = objOList_token_extensions_mod.First().ID_Other,
                Name = objOList_token_extensions_mod.First().Name_Other,
                GUID_Parent = objOList_token_extensions_mod.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_extensions_pdf = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "token_extensions_pdf".ToLower() && objRef.Ontology == Globals.Type_Object
                                             select objRef).ToList();

        if (objOList_token_extensions_pdf.Any())
        {
            OItem_token_extensions_pdf = new clsOntologyItem()
            {
                GUID = objOList_token_extensions_pdf.First().ID_Other,
                Name = objOList_token_extensions_pdf.First().Name_Other,
                GUID_Parent = objOList_token_extensions_pdf.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_image_objects__no_objects__no_address = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                    where objOItem.ID_Object == cstrID_Ontology
                                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                    where objRef.Name_Object.ToLower() == "token_image_objects__no_objects__no_address".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                    select objRef).ToList();

        if (objOList_token_image_objects__no_objects__no_address.Any())
        {
            OItem_token_image_objects__no_objects__no_address = new clsOntologyItem()
            {
                GUID = objOList_token_image_objects__no_objects__no_address.First().ID_Other,
                Name = objOList_token_image_objects__no_objects__no_address.First().Name_Other,
                GUID_Parent = objOList_token_image_objects__no_objects__no_address.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_image_objects__no_objects__no_artwork = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                    where objOItem.ID_Object == cstrID_Ontology
                                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                    where objRef.Name_Object.ToLower() == "token_image_objects__no_objects__no_artwork".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                    select objRef).ToList();

        if (objOList_token_image_objects__no_objects__no_artwork.Any())
        {
            OItem_token_image_objects__no_objects__no_artwork = new clsOntologyItem()
            {
                GUID = objOList_token_image_objects__no_objects__no_artwork.First().ID_Other,
                Name = objOList_token_image_objects__no_objects__no_artwork.First().Name_Other,
                GUID_Parent = objOList_token_image_objects__no_objects__no_artwork.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_image_objects__no_objects__no_buildings = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                      where objOItem.ID_Object == cstrID_Ontology
                                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                      where objRef.Name_Object.ToLower() == "token_image_objects__no_objects__no_buildings".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                      select objRef).ToList();

        if (objOList_token_image_objects__no_objects__no_buildings.Any())
        {
            OItem_token_image_objects__no_objects__no_buildings = new clsOntologyItem()
            {
                GUID = objOList_token_image_objects__no_objects__no_buildings.First().ID_Other,
                Name = objOList_token_image_objects__no_objects__no_buildings.First().Name_Other,
                GUID_Parent = objOList_token_image_objects__no_objects__no_buildings.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_image_objects__no_objects__no_landscape = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                      where objOItem.ID_Object == cstrID_Ontology
                                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                      where objRef.Name_Object.ToLower() == "token_image_objects__no_objects__no_landscape".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                      select objRef).ToList();

        if (objOList_token_image_objects__no_objects__no_landscape.Any())
        {
            OItem_token_image_objects__no_objects__no_landscape = new clsOntologyItem()
            {
                GUID = objOList_token_image_objects__no_objects__no_landscape.First().ID_Other,
                Name = objOList_token_image_objects__no_objects__no_landscape.First().Name_Other,
                GUID_Parent = objOList_token_image_objects__no_objects__no_landscape.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_image_objects__no_objects__no_location = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                     where objOItem.ID_Object == cstrID_Ontology
                                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                     where objRef.Name_Object.ToLower() == "token_image_objects__no_objects__no_location".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                     select objRef).ToList();

        if (objOList_token_image_objects__no_objects__no_location.Any())
        {
            OItem_token_image_objects__no_objects__no_location = new clsOntologyItem()
            {
                GUID = objOList_token_image_objects__no_objects__no_location.First().ID_Other,
                Name = objOList_token_image_objects__no_objects__no_location.First().Name_Other,
                GUID_Parent = objOList_token_image_objects__no_objects__no_location.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_image_objects__no_objects__no_persons = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                    where objOItem.ID_Object == cstrID_Ontology
                                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                    where objRef.Name_Object.ToLower() == "token_image_objects__no_objects__no_persons".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                    select objRef).ToList();

        if (objOList_token_image_objects__no_objects__no_persons.Any())
        {
            OItem_token_image_objects__no_objects__no_persons = new clsOntologyItem()
            {
                GUID = objOList_token_image_objects__no_objects__no_persons.First().ID_Other,
                Name = objOList_token_image_objects__no_objects__no_persons.First().Name_Other,
                GUID_Parent = objOList_token_image_objects__no_objects__no_persons.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_image_objects__no_objects__no_pets = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                 where objOItem.ID_Object == cstrID_Ontology
                                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                 where objRef.Name_Object.ToLower() == "token_image_objects__no_objects__no_pets".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                 select objRef).ToList();

        if (objOList_token_image_objects__no_objects__no_pets.Any())
        {
            OItem_token_image_objects__no_objects__no_pets = new clsOntologyItem()
            {
                GUID = objOList_token_image_objects__no_objects__no_pets.First().ID_Other,
                Name = objOList_token_image_objects__no_objects__no_pets.First().Name_Other,
                GUID_Parent = objOList_token_image_objects__no_objects__no_pets.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_image_objects__no_objects__no_plant_species = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                          where objOItem.ID_Object == cstrID_Ontology
                                                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                          where objRef.Name_Object.ToLower() == "token_image_objects__no_objects__no_plant_species".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                          select objRef).ToList();

        if (objOList_token_image_objects__no_objects__no_plant_species.Any())
        {
            OItem_token_image_objects__no_objects__no_plant_species = new clsOntologyItem()
            {
                GUID = objOList_token_image_objects__no_objects__no_plant_species.First().ID_Other,
                Name = objOList_token_image_objects__no_objects__no_plant_species.First().Name_Other,
                GUID_Parent = objOList_token_image_objects__no_objects__no_plant_species.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_image_objects__no_objects__no_species = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                    where objOItem.ID_Object == cstrID_Ontology
                                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                    where objRef.Name_Object.ToLower() == "token_image_objects__no_objects__no_species".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                    select objRef).ToList();

        if (objOList_token_image_objects__no_objects__no_species.Any())
        {
            OItem_token_image_objects__no_objects__no_species = new clsOntologyItem()
            {
                GUID = objOList_token_image_objects__no_objects__no_species.First().ID_Other,
                Name = objOList_token_image_objects__no_objects__no_species.First().Name_Other,
                GUID_Parent = objOList_token_image_objects__no_objects__no_species.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_image_objects__no_objects__no_symbol = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                   where objOItem.ID_Object == cstrID_Ontology
                                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                   where objRef.Name_Object.ToLower() == "token_image_objects__no_objects__no_symbol".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                   select objRef).ToList();

        if (objOList_token_image_objects__no_objects__no_symbol.Any())
        {
            OItem_token_image_objects__no_objects__no_symbol = new clsOntologyItem()
            {
                GUID = objOList_token_image_objects__no_objects__no_symbol.First().ID_Other,
                Name = objOList_token_image_objects__no_objects__no_symbol.First().Name_Other,
                GUID_Parent = objOList_token_image_objects__no_objects__no_symbol.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_image_objects__no_objects__weather_condition = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                           where objOItem.ID_Object == cstrID_Ontology
                                                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                           where objRef.Name_Object.ToLower() == "token_image_objects__no_objects__weather_condition".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                           select objRef).ToList();

        if (objOList_token_image_objects__no_objects__weather_condition.Any())
        {
            OItem_token_image_objects__no_objects__weather_condition = new clsOntologyItem()
            {
                GUID = objOList_token_image_objects__no_objects__weather_condition.First().ID_Other,
                Name = objOList_token_image_objects__no_objects__weather_condition.First().Name_Other,
                GUID_Parent = objOList_token_image_objects__no_objects__weather_condition.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_last_position = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "token_logstate_last_position".ToLower() && objRef.Ontology == Globals.Type_Object
                                                     select objRef).ToList();

        if (objOList_token_logstate_last_position.Any())
        {
            OItem_token_logstate_last_position = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_last_position.First().ID_Other,
                Name = objOList_token_logstate_last_position.First().Name_Other,
                GUID_Parent = objOList_token_logstate_last_position.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_pause = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "token_logstate_pause".ToLower() && objRef.Ontology == Globals.Type_Object
                                             select objRef).ToList();

        if (objOList_token_logstate_pause.Any())
        {
            OItem_token_logstate_pause = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_pause.First().ID_Other,
                Name = objOList_token_logstate_pause.First().Name_Other,
                GUID_Parent = objOList_token_logstate_pause.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_start = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "token_logstate_start".ToLower() && objRef.Ontology == Globals.Type_Object
                                             select objRef).ToList();

        if (objOList_token_logstate_start.Any())
        {
            OItem_token_logstate_start = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_start.First().ID_Other,
                Name = objOList_token_logstate_start.First().Name_Other,
                GUID_Parent = objOList_token_logstate_start.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_stop = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "token_logstate_stop".ToLower() && objRef.Ontology == Globals.Type_Object
                                            select objRef).ToList();

        if (objOList_token_logstate_stop.Any())
        {
            OItem_token_logstate_stop = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_stop.First().ID_Other,
                Name = objOList_token_logstate_stop.First().Name_Other,
                GUID_Parent = objOList_token_logstate_stop.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_unassigned = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "token_logstate_unassigned".ToLower() && objRef.Ontology == Globals.Type_Object
                                                  select objRef).ToList();

        if (objOList_token_logstate_unassigned.Any())
        {
            OItem_token_logstate_unassigned = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_unassigned.First().ID_Other,
                Name = objOList_token_logstate_unassigned.First().Name_Other,
                GUID_Parent = objOList_token_logstate_unassigned.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_user_defined = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "token_logstate_user_defined".ToLower() && objRef.Ontology == Globals.Type_Object
                                                    select objRef).ToList();

        if (objOList_token_logstate_user_defined.Any())
        {
            OItem_token_logstate_user_defined = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_user_defined.First().ID_Other,
                Name = objOList_token_logstate_user_defined.First().Name_Other,
                GUID_Parent = objOList_token_logstate_user_defined.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_search_template_name_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "token_search_template_name_".ToLower() && objRef.Ontology == Globals.Type_Object
                                                    select objRef).ToList();

        if (objOList_token_search_template_name_.Any())
        {
            OItem_token_search_template_name_ = new clsOntologyItem()
            {
                GUID = objOList_token_search_template_name_.First().ID_Other,
                Name = objOList_token_search_template_name_.First().Name_Other,
                GUID_Parent = objOList_token_search_template_name_.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_variable_itemcount = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "token_variable_itemcount".ToLower() && objRef.Ontology == Globals.Type_Object
                                                 select objRef).ToList();

        if (objOList_token_variable_itemcount.Any())
        {
            OItem_token_variable_itemcount = new clsOntologyItem()
            {
                GUID = objOList_token_variable_itemcount.First().ID_Other,
                Name = objOList_token_variable_itemcount.First().Name_Other,
                GUID_Parent = objOList_token_variable_itemcount.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_variable_mediasrc = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "token_variable_mediasrc".ToLower() && objRef.Ontology == Globals.Type_Object
                                                select objRef).ToList();

        if (objOList_token_variable_mediasrc.Any())
        {
            OItem_token_variable_mediasrc = new clsOntologyItem()
            {
                GUID = objOList_token_variable_mediasrc.First().ID_Other,
                Name = objOList_token_variable_mediasrc.First().Name_Other,
                GUID_Parent = objOList_token_variable_mediasrc.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_variable_title = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "token_variable_title".ToLower() && objRef.Ontology == Globals.Type_Object
                                             select objRef).ToList();

        if (objOList_token_variable_title.Any())
        {
            OItem_token_variable_title = new clsOntologyItem()
            {
                GUID = objOList_token_variable_title.First().ID_Other,
                Name = objOList_token_variable_title.First().Name_Other,
                GUID_Parent = objOList_token_variable_title.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_variable_url_mediasrc = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "token_variable_url_mediasrc".ToLower() && objRef.Ontology == Globals.Type_Object
                                                    select objRef).ToList();

        if (objOList_token_variable_url_mediasrc.Any())
        {
            OItem_token_variable_url_mediasrc = new clsOntologyItem()
            {
                GUID = objOList_token_variable_url_mediasrc.First().ID_Other,
                Name = objOList_token_variable_url_mediasrc.First().Name_Other,
                GUID_Parent = objOList_token_variable_url_mediasrc.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_xml_windows_playlist_1_0 = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "token_xml_windows_playlist_1_0".ToLower() && objRef.Ontology == Globals.Type_Object
                                                       select objRef).ToList();

        if (objOList_token_xml_windows_playlist_1_0.Any())
        {
            OItem_token_xml_windows_playlist_1_0 = new clsOntologyItem()
            {
                GUID = objOList_token_xml_windows_playlist_1_0.First().ID_Other,
                Name = objOList_token_xml_windows_playlist_1_0.First().Name_Other,
                GUID_Parent = objOList_token_xml_windows_playlist_1_0.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_xml_windows_playlist_1_0_mediasrc = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                where objOItem.ID_Object == cstrID_Ontology
                                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                where objRef.Name_Object.ToLower() == "token_xml_windows_playlist_1_0_mediasrc".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                select objRef).ToList();

        if (objOList_token_xml_windows_playlist_1_0_mediasrc.Any())
        {
            OItem_token_xml_windows_playlist_1_0_mediasrc = new clsOntologyItem()
            {
                GUID = objOList_token_xml_windows_playlist_1_0_mediasrc.First().ID_Other,
                Name = objOList_token_xml_windows_playlist_1_0_mediasrc.First().Name_Other,
                GUID_Parent = objOList_token_xml_windows_playlist_1_0_mediasrc.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_Classes()
    {
            var objOList_class_logstate = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "class_logstate".ToLower() && objRef.Ontology == Globals.Type_Class
                                           select objRef).ToList();

            if (objOList_class_logstate.Any())
            {
                OItem_class_logstate = new clsOntologyItem()
                {
                    GUID = objOList_class_logstate.First().ID_Other,
                    Name = objOList_class_logstate.First().Name_Other,
                    GUID_Parent = objOList_class_logstate.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_media_item_objects = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "class_media_item_objects".ToLower() && objRef.Ontology == Globals.Type_Class
                                                 select objRef).ToList();

        if (objOList_class_media_item_objects.Any())
        {
            OItem_class_media_item_objects = new clsOntologyItem()
            {
                GUID = objOList_class_media_item_objects.First().ID_Other,
                Name = objOList_class_media_item_objects.First().Name_Other,
                GUID_Parent = objOList_class_media_item_objects.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_album = (from objOItem in objDBLevel_Config1.ObjectRels
                                   where objOItem.ID_Object == cstrID_Ontology
                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                   where objRef.Name_Object.ToLower() == "type_album".ToLower() && objRef.Ontology == Globals.Type_Class
                                   select objRef).ToList();

        if (objOList_type_album.Any())
        {
            OItem_type_album = new clsOntologyItem()
            {
                GUID = objOList_type_album.First().ID_Other,
                Name = objOList_type_album.First().Name_Other,
                GUID_Parent = objOList_type_album.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_band = (from objOItem in objDBLevel_Config1.ObjectRels
                                  where objOItem.ID_Object == cstrID_Ontology
                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                  where objRef.Name_Object.ToLower() == "type_band".ToLower() && objRef.Ontology == Globals.Type_Class
                                  select objRef).ToList();

        if (objOList_type_band.Any())
        {
            OItem_type_band = new clsOntologyItem()
            {
                GUID = objOList_type_band.First().ID_Other,
                Name = objOList_type_band.First().Name_Other,
                GUID_Parent = objOList_type_band.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_bauwerke = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_bauwerke".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

        if (objOList_type_bauwerke.Any())
        {
            OItem_type_bauwerke = new clsOntologyItem()
            {
                GUID = objOList_type_bauwerke.First().ID_Other,
                Name = objOList_type_bauwerke.First().Name_Other,
                GUID_Parent = objOList_type_bauwerke.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_day = (from objOItem in objDBLevel_Config1.ObjectRels
                                 where objOItem.ID_Object == cstrID_Ontology
                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                 where objRef.Name_Object.ToLower() == "type_day".ToLower() && objRef.Ontology == Globals.Type_Class
                                 select objRef).ToList();

        if (objOList_type_day.Any())
        {
            OItem_type_day = new clsOntologyItem()
            {
                GUID = objOList_type_day.First().ID_Other,
                Name = objOList_type_day.First().Name_Other,
                GUID_Parent = objOList_type_day.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_file = (from objOItem in objDBLevel_Config1.ObjectRels
                                  where objOItem.ID_Object == cstrID_Ontology
                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                  where objRef.Name_Object.ToLower() == "type_file".ToLower() && objRef.Ontology == Globals.Type_Class
                                  select objRef).ToList();

        if (objOList_type_file.Any())
        {
            OItem_type_file = new clsOntologyItem()
            {
                GUID = objOList_type_file.First().ID_Other,
                Name = objOList_type_file.First().Name_Other,
                GUID_Parent = objOList_type_file.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_filetypes = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "type_filetypes".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

        if (objOList_type_filetypes.Any())
        {
            OItem_type_filetypes = new clsOntologyItem()
            {
                GUID = objOList_type_filetypes.First().ID_Other,
                Name = objOList_type_filetypes.First().Name_Other,
                GUID_Parent = objOList_type_filetypes.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_genre = (from objOItem in objDBLevel_Config1.ObjectRels
                                   where objOItem.ID_Object == cstrID_Ontology
                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                   where objRef.Name_Object.ToLower() == "type_genre".ToLower() && objRef.Ontology == Globals.Type_Class
                                   select objRef).ToList();

        if (objOList_type_genre.Any())
        {
            OItem_type_genre = new clsOntologyItem()
            {
                GUID = objOList_type_genre.First().ID_Other,
                Name = objOList_type_genre.First().Name_Other,
                GUID_Parent = objOList_type_genre.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_haustier = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_haustier".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

        if (objOList_type_haustier.Any())
        {
            OItem_type_haustier = new clsOntologyItem()
            {
                GUID = objOList_type_haustier.First().ID_Other,
                Name = objOList_type_haustier.First().Name_Other,
                GUID_Parent = objOList_type_haustier.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_image_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "type_image_module".ToLower() && objRef.Ontology == Globals.Type_Class
                                          select objRef).ToList();

        if (objOList_type_image_module.Any())
        {
            OItem_type_image_module = new clsOntologyItem()
            {
                GUID = objOList_type_image_module.First().ID_Other,
                Name = objOList_type_image_module.First().Name_Other,
                GUID_Parent = objOList_type_image_module.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_image_objects = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "type_image_objects".ToLower() && objRef.Ontology == Globals.Type_Class
                                           select objRef).ToList();

        if (objOList_type_image_objects.Any())
        {
            OItem_type_image_objects = new clsOntologyItem()
            {
                GUID = objOList_type_image_objects.First().ID_Other,
                Name = objOList_type_image_objects.First().Name_Other,
                GUID_Parent = objOList_type_image_objects.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_image_objects__no_objects_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                        where objOItem.ID_Object == cstrID_Ontology
                                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                        where objRef.Name_Object.ToLower() == "type_image_objects__no_objects_".ToLower() && objRef.Ontology == Globals.Type_Class
                                                        select objRef).ToList();

        if (objOList_type_image_objects__no_objects_.Any())
        {
            OItem_type_image_objects__no_objects_ = new clsOntologyItem()
            {
                GUID = objOList_type_image_objects__no_objects_.First().ID_Other,
                Name = objOList_type_image_objects__no_objects_.First().Name_Other,
                GUID_Parent = objOList_type_image_objects__no_objects_.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_images__graphic_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "type_images__graphic_".ToLower() && objRef.Ontology == Globals.Type_Class
                                              select objRef).ToList();

        if (objOList_type_images__graphic_.Any())
        {
            OItem_type_images__graphic_ = new clsOntologyItem()
            {
                GUID = objOList_type_images__graphic_.First().ID_Other,
                Name = objOList_type_images__graphic_.First().Name_Other,
                GUID_Parent = objOList_type_images__graphic_.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_kunst = (from objOItem in objDBLevel_Config1.ObjectRels
                                   where objOItem.ID_Object == cstrID_Ontology
                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                   where objRef.Name_Object.ToLower() == "type_kunst".ToLower() && objRef.Ontology == Globals.Type_Class
                                   select objRef).ToList();

        if (objOList_type_kunst.Any())
        {
            OItem_type_kunst = new clsOntologyItem()
            {
                GUID = objOList_type_kunst.First().ID_Other,
                Name = objOList_type_kunst.First().Name_Other,
                GUID_Parent = objOList_type_kunst.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_landscape = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "type_landscape".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

        if (objOList_type_landscape.Any())
        {
            OItem_type_landscape = new clsOntologyItem()
            {
                GUID = objOList_type_landscape.First().ID_Other,
                Name = objOList_type_landscape.First().Name_Other,
                GUID_Parent = objOList_type_landscape.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_language = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_language".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

        if (objOList_type_language.Any())
        {
            OItem_type_language = new clsOntologyItem()
            {
                GUID = objOList_type_language.First().ID_Other,
                Name = objOList_type_language.First().Name_Other,
                GUID_Parent = objOList_type_language.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_logentry = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_logentry".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

        if (objOList_type_logentry.Any())
        {
            OItem_type_logentry = new clsOntologyItem()
            {
                GUID = objOList_type_logentry.First().ID_Other,
                Name = objOList_type_logentry.First().Name_Other,
                GUID_Parent = objOList_type_logentry.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_media = (from objOItem in objDBLevel_Config1.ObjectRels
                                   where objOItem.ID_Object == cstrID_Ontology
                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                   where objRef.Name_Object.ToLower() == "type_media".ToLower() && objRef.Ontology == Globals.Type_Class
                                   select objRef).ToList();

        if (objOList_type_media.Any())
        {
            OItem_type_media = new clsOntologyItem()
            {
                GUID = objOList_type_media.First().ID_Other,
                Name = objOList_type_media.First().Name_Other,
                GUID_Parent = objOList_type_media.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_media_item = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "type_media_item".ToLower() && objRef.Ontology == Globals.Type_Class
                                        select objRef).ToList();

        if (objOList_type_media_item.Any())
        {
            OItem_type_media_item = new clsOntologyItem()
            {
                GUID = objOList_type_media_item.First().ID_Other,
                Name = objOList_type_media_item.First().Name_Other,
                GUID_Parent = objOList_type_media_item.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_media_item_bookmark = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "type_media_item_bookmark".ToLower() && objRef.Ontology == Globals.Type_Class
                                                 select objRef).ToList();

        if (objOList_type_media_item_bookmark.Any())
        {
            OItem_type_media_item_bookmark = new clsOntologyItem()
            {
                GUID = objOList_type_media_item_bookmark.First().ID_Other,
                Name = objOList_type_media_item_bookmark.First().Name_Other,
                GUID_Parent = objOList_type_media_item_bookmark.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_media_item_range = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "type_media_item_range".ToLower() && objRef.Ontology == Globals.Type_Class
                                              select objRef).ToList();

        if (objOList_type_media_item_range.Any())
        {
            OItem_type_media_item_range = new clsOntologyItem()
            {
                GUID = objOList_type_media_item_range.First().ID_Other,
                Name = objOList_type_media_item_range.First().Name_Other,
                GUID_Parent = objOList_type_media_item_range.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                    where objOItem.ID_Object == cstrID_Ontology
                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                    where objRef.Name_Object.ToLower() == "type_module".ToLower() && objRef.Ontology == Globals.Type_Class
                                    select objRef).ToList();

        if (objOList_type_module.Any())
        {
            OItem_type_module = new clsOntologyItem()
            {
                GUID = objOList_type_module.First().ID_Other,
                Name = objOList_type_module.First().Name_Other,
                GUID_Parent = objOList_type_module.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_month = (from objOItem in objDBLevel_Config1.ObjectRels
                                   where objOItem.ID_Object == cstrID_Ontology
                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                   where objRef.Name_Object.ToLower() == "type_month".ToLower() && objRef.Ontology == Globals.Type_Class
                                   select objRef).ToList();

        if (objOList_type_month.Any())
        {
            OItem_type_month = new clsOntologyItem()
            {
                GUID = objOList_type_month.First().ID_Other,
                Name = objOList_type_month.First().Name_Other,
                GUID_Parent = objOList_type_month.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_mp3_file = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_mp3_file".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

        if (objOList_type_mp3_file.Any())
        {
            OItem_type_mp3_file = new clsOntologyItem()
            {
                GUID = objOList_type_mp3_file.First().ID_Other,
                Name = objOList_type_mp3_file.First().Name_Other,
                GUID_Parent = objOList_type_mp3_file.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_ort = (from objOItem in objDBLevel_Config1.ObjectRels
                                 where objOItem.ID_Object == cstrID_Ontology
                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                 where objRef.Name_Object.ToLower() == "type_ort".ToLower() && objRef.Ontology == Globals.Type_Class
                                 select objRef).ToList();

        if (objOList_type_ort.Any())
        {
            OItem_type_ort = new clsOntologyItem()
            {
                GUID = objOList_type_ort.First().ID_Other,
                Name = objOList_type_ort.First().Name_Other,
                GUID_Parent = objOList_type_ort.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_partner = (from objOItem in objDBLevel_Config1.ObjectRels
                                     where objOItem.ID_Object == cstrID_Ontology
                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                     where objRef.Name_Object.ToLower() == "type_partner".ToLower() && objRef.Ontology == Globals.Type_Class
                                     select objRef).ToList();

        if (objOList_type_partner.Any())
        {
            OItem_type_partner = new clsOntologyItem()
            {
                GUID = objOList_type_partner.First().ID_Other,
                Name = objOList_type_partner.First().Name_Other,
                GUID_Parent = objOList_type_partner.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_pdf_documents = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "type_pdf_documents".ToLower() && objRef.Ontology == Globals.Type_Class
                                           select objRef).ToList();

        if (objOList_type_pdf_documents.Any())
        {
            OItem_type_pdf_documents = new clsOntologyItem()
            {
                GUID = objOList_type_pdf_documents.First().ID_Other,
                Name = objOList_type_pdf_documents.First().Name_Other,
                GUID_Parent = objOList_type_pdf_documents.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_pflanzenarten = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "type_pflanzenarten".ToLower() && objRef.Ontology == Globals.Type_Class
                                           select objRef).ToList();

        if (objOList_type_pflanzenarten.Any())
        {
            OItem_type_pflanzenarten = new clsOntologyItem()
            {
                GUID = objOList_type_pflanzenarten.First().ID_Other,
                Name = objOList_type_pflanzenarten.First().Name_Other,
                GUID_Parent = objOList_type_pflanzenarten.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_search_template = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "type_search_template".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

        if (objOList_type_search_template.Any())
        {
            OItem_type_search_template = new clsOntologyItem()
            {
                GUID = objOList_type_search_template.First().ID_Other,
                Name = objOList_type_search_template.First().Name_Other,
                GUID_Parent = objOList_type_search_template.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_tierarten = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "type_tierarten".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

        if (objOList_type_tierarten.Any())
        {
            OItem_type_tierarten = new clsOntologyItem()
            {
                GUID = objOList_type_tierarten.First().ID_Other,
                Name = objOList_type_tierarten.First().Name_Other,
                GUID_Parent = objOList_type_tierarten.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_url = (from objOItem in objDBLevel_Config1.ObjectRels
                                 where objOItem.ID_Object == cstrID_Ontology
                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                 where objRef.Name_Object.ToLower() == "type_url".ToLower() && objRef.Ontology == Globals.Type_Class
                                 select objRef).ToList();

        if (objOList_type_url.Any())
        {
            OItem_type_url = new clsOntologyItem()
            {
                GUID = objOList_type_url.First().ID_Other,
                Name = objOList_type_url.First().Name_Other,
                GUID_Parent = objOList_type_url.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_user = (from objOItem in objDBLevel_Config1.ObjectRels
                                  where objOItem.ID_Object == cstrID_Ontology
                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                  where objRef.Name_Object.ToLower() == "type_user".ToLower() && objRef.Ontology == Globals.Type_Class
                                  select objRef).ToList();

        if (objOList_type_user.Any())
        {
            OItem_type_user = new clsOntologyItem()
            {
                GUID = objOList_type_user.First().ID_Other,
                Name = objOList_type_user.First().Name_Other,
                GUID_Parent = objOList_type_user.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_wetterlage = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "type_wetterlage".ToLower() && objRef.Ontology == Globals.Type_Class
                                        select objRef).ToList();

        if (objOList_type_wetterlage.Any())
        {
            OItem_type_wetterlage = new clsOntologyItem()
            {
                GUID = objOList_type_wetterlage.First().ID_Other,
                Name = objOList_type_wetterlage.First().Name_Other,
                GUID_Parent = objOList_type_wetterlage.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_wichtige_ereignisse = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "type_wichtige_ereignisse".ToLower() && objRef.Ontology == Globals.Type_Class
                                                 select objRef).ToList();

        if (objOList_type_wichtige_ereignisse.Any())
        {
            OItem_type_wichtige_ereignisse = new clsOntologyItem()
            {
                GUID = objOList_type_wichtige_ereignisse.First().ID_Other,
                Name = objOList_type_wichtige_ereignisse.First().Name_Other,
                GUID_Parent = objOList_type_wichtige_ereignisse.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_year = (from objOItem in objDBLevel_Config1.ObjectRels
                                  where objOItem.ID_Object == cstrID_Ontology
                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                  where objRef.Name_Object.ToLower() == "type_year".ToLower() && objRef.Ontology == Globals.Type_Class
                                  select objRef).ToList();

        if (objOList_type_year.Any())
        {
            OItem_type_year = new clsOntologyItem()
            {
                GUID = objOList_type_year.First().ID_Other,
                Name = objOList_type_year.First().Name_Other,
                GUID_Parent = objOList_type_year.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

        public string IdLocalConfig
        {
            get
            {
                var attrib =
                      Assembly.GetExecutingAssembly()
                          .GetCustomAttributes(true)
                          .FirstOrDefault(objAttribute => objAttribute is GuidAttribute);
                if (attrib != null)
                {
                    return ((GuidAttribute)attrib).Value;
                }
                else
                {
                    return null;
                }
            }
        }
    }

}