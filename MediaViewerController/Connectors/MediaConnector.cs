﻿using MediaViewerController.Factories;
using MediaViewerController.Models;
using MediaViewerController.Services;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Connectors
{
    public enum MediaType
    {
        Image = 0,
        Audio = 1,
        Video = 2,
        PDF = 3
    }
    public class MediaConnector : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;
        private clsRelationConfig relationConfig;
        private clsTransaction transaction;

        private MediaServiceAgent_Elastic mediaServiceAgentElastic;
        private ViewMediaFactory viewMediaFactory;

        private object connectorLocker = new object();

        public clsOntologyItem OItemImageClass
        {
            get
            {
                return localConfig.OItem_type_images__graphic_;
            }
        }

        public clsOntologyItem OItemPDFClass
        {
            get
            {
                return localConfig.OItem_type_pdf_documents;
            }
        }

        public clsOntologyItem OItemMediaItemClass
        {
            get
            {
                return localConfig.OItem_type_media_item;
            }
        }

        private ResultCreateMultimediaItems resultCreateMultimediaItems;
        public ResultCreateMultimediaItems ResultCreateMultimediaItems
        {
            get { return resultCreateMultimediaItems; }
            set
            {
                resultCreateMultimediaItems = value;
                RaisePropertyChanged(nameof(ResultCreateMultimediaItems));
            }
        }

        private ResultMultimediaSessionFile resultMultimediaSessionFile;
        public ResultMultimediaSessionFile ResultMultimediaSessionFile
        {
            get
            {
                lock(connectorLocker)
                {
                    return resultMultimediaSessionFile;
                }
            }
            set
            {
                lock(connectorLocker)
                {
                    resultMultimediaSessionFile = value;
                }
                RaisePropertyChanged(nameof(ResultMultimediaSessionFile));
            }
        }


        public async Task<ResultMultimediaSessionFile> GetMediaSessionFile(clsOntologyItem oItemMultimediaItem, WebsocketServiceAgent serviceAgent)
        {
            viewMediaFactory = new ViewMediaFactory(localConfig, 500, 500, serviceAgent.oItemUser, serviceAgent.oItemGroup);

            var result = new ResultMultimediaSessionFile
            {
                OMultimediaItem = oItemMultimediaItem,
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var resultTask = mediaServiceAgentElastic.GetDataOfMultimediaItem(oItemMultimediaItem);
            resultTask.Wait();


            result.Result = resultTask.Result.Result;

            if (result.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultMultimediaSessionFile = result;
                return result;
            }

            
            var resultTaskFactory = viewMediaFactory.SaveMultimediaFiles(resultTask.Result.MediaRels, serviceAgent);
            resultTaskFactory.Wait();

            result.Result = resultTaskFactory.Result.Result;
            if (result.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultMultimediaSessionFile = result;
                return result;
            }

            result.SessionFile = resultTaskFactory.Result.SessionFiles.FirstOrDefault();

            if (oItemMultimediaItem.GUID_Parent == localConfig.OItem_type_media_item.GUID)
            {
                if (result.SessionFile.FileUri.AbsoluteUri.EndsWith(Properties.Settings.Default.Extension3GP) ||
                    result.SessionFile.FileUri.AbsoluteUri.EndsWith(Properties.Settings.Default.ExtensionAVI) ||
                    result.SessionFile.FileUri.AbsoluteUri.EndsWith(Properties.Settings.Default.ExtensionMOD) ||
                    result.SessionFile.FileUri.AbsoluteUri.EndsWith(Properties.Settings.Default.ExtensionMP4) ||
                    result.SessionFile.FileUri.AbsoluteUri.EndsWith(Properties.Settings.Default.ExtensionMTS) ||
                    result.SessionFile.FileUri.AbsoluteUri.EndsWith(Properties.Settings.Default.ExtensionVMW))
                {
                    result.MediaType = MediaType.Video;
                }
                else
                {
                    result.MediaType = MediaType.Audio;
                }
            }
            else if (oItemMultimediaItem.GUID_Parent == localConfig.OItem_type_pdf_documents.GUID)
            {
                result.MediaType = MediaType.PDF;
            }
            else if (oItemMultimediaItem.GUID_Parent == localConfig.OItem_type_images__graphic_.GUID)
            {
                result.MediaType = MediaType.Image;
            }

            ResultMultimediaSessionFile = result;
            return result;
        }

        public async Task<ResultCreateMultimediaItems> CreateMultimediaItems(MediaType mediaType, List<clsOntologyItem> fileItems, clsOntologyItem refItem)
        {
            var resultItem = new ResultCreateMultimediaItems
            {
                Result = localConfig.Globals.LState_Success.Clone(),
                MultimediaItems = new List<MultimediaItem>()
            };

            foreach(var fileItem in fileItems)
            {
                var oItemMultiMediaItem = new clsOntologyItem
                {
                    GUID = localConfig.Globals.NewGUID,
                    Name = fileItem.Name,
                    GUID_Parent = Converters.MediaTypeEnumToMediaTypeClassId.Convert(mediaType, localConfig),
                    Type = localConfig.Globals.Type_Object

                };

                transaction.ClearItems();
                resultItem.Result = transaction.do_Transaction(oItemMultiMediaItem);

                if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    ResultCreateMultimediaItems = resultItem;
                    return resultItem;
                }

                var relFile = relationConfig.Rel_ObjectRelation(oItemMultiMediaItem, fileItem, localConfig.OItem_relationtype_belonging_source);

                resultItem.Result = transaction.do_Transaction(relFile);

                if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    transaction.rollback();
                    ResultCreateMultimediaItems = resultItem;
                    return resultItem;
                }

                var relRef = relationConfig.Rel_ObjectRelation(oItemMultiMediaItem, refItem, localConfig.OItem_relationtype_belongsto);

                resultItem.Result = transaction.do_Transaction(relRef);

                if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    transaction.rollback();
                    ResultCreateMultimediaItems = resultItem;
                    return resultItem;
                }

                var multimediaItem = new MultimediaItem
                {
                    IdMultimediaItem = oItemMultiMediaItem.GUID,
                    NameMultimediaItem = oItemMultiMediaItem.Name,
                    IdClassMultimediaItem = oItemMultiMediaItem.GUID_Parent,
                    IdFile = fileItem.GUID,
                    NameFile = fileItem.Name,
                    IdRef = refItem.GUID,
                    NameRef = refItem.Name
                };

                resultItem.MultimediaItems.Add(multimediaItem);
            }

            ResultCreateMultimediaItems = resultItem;
            return resultItem;
        }

        public MediaConnector(Globals globals)
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
        }

        private void Initialize()
        {
            relationConfig = new clsRelationConfig(localConfig.Globals);
            transaction = new clsTransaction(localConfig.Globals);
            mediaServiceAgentElastic = new MediaServiceAgent_Elastic(localConfig);
            
        }
    }

    public class ResultCreateMultimediaItems
    {
        public clsOntologyItem Result { get; set; }
        public List<MultimediaItem> MultimediaItems { get; set; }
    }

    public class ResultMultimediaSessionFile
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem OMultimediaItem { get; set; }
        public MediaType MediaType { get; set; }
        public SessionFile SessionFile { get; set; }

    }
}
