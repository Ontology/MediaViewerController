﻿using MediaViewerController.Models;
using MediaViewerController.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Controllers
{
    public class VideoPlayerViewModel : OntoMsg_Module.Base.ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(nameof(IsSuccessful_Login));

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
        public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(nameof(IsToggled_Listen));

            }
        }

        private bool isenabled_NavPrevious;
        [ViewModel(Send = true)]
        public bool IsEnabled_NavPrevious
        {
            get { return isenabled_NavPrevious; }
            set
            {
                if (isenabled_NavPrevious == value) return;

                isenabled_NavPrevious = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavPrevious);

            }
        }

        private int width_Canvas;
        [ViewModel(Send = true)]
        public int Width_Canvas
        {
            get { return width_Canvas; }
            set
            {
                if (width_Canvas == value) return;

                width_Canvas = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Width_Canvas);

            }
        }

        private int count_ToSave;
        [ViewModel(Send = true)]
        public int Count_ToSave
        {
            get { return count_ToSave; }
            set
            {
                if (count_ToSave == value) return;

                count_ToSave = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Count_ToSave);

            }
        }

        private bool isenabled_NavNext;
        [ViewModel(Send = true)]
        public bool IsEnabled_NavNext
        {
            get { return isenabled_NavNext; }
            set
            {
                if (isenabled_NavNext == value) return;

                isenabled_NavNext = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavNext);

            }
        }

        private bool isenabled_NavLast;
        [ViewModel(Send = true)]
        public bool IsEnabled_NavLast
        {
            get { return isenabled_NavLast; }
            set
            {
                if (isenabled_NavLast == value) return;

                isenabled_NavLast = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavLast);

            }
        }

        private string label_Nav;
        [ViewModel(Send = true)]
        public string Label_Nav
        {
            get { return label_Nav; }
            set
            {
                if (label_Nav == value) return;

                label_Nav = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Nav);

            }
        }

        private int position_VideoItems;
        [ViewModel(Send = true)]
        public int Position_VideoItems
        {
            get { return position_VideoItems; }
            set
            {
                if (position_VideoItems == value) return;

                position_VideoItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Position_VideoItems);

            }
        }

        private string url_Video;
        [ViewModel(Send = true)]
        public string Url_Video
        {
            get { return url_Video; }
            set
            {

                url_Video = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_Video);

            }
        }

        private int count_PlaylistItems;
        [ViewModel(Send = true)]
        public int Count_PlaylistItems
        {
            get { return count_PlaylistItems; }
            set
            {
                if (count_PlaylistItems == value) return;

                count_PlaylistItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Count_PlaylistItems);

            }
        }

        private string label_MediaItem;
        [ViewModel(Send = true)]
        public string Label_MediaItem
        {
            get { return label_MediaItem; }
            set
            {

                label_MediaItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_MediaItem);

            }
        }

        private string datatext_MediaMime;
        [ViewModel(Send = true)]
        public string DataText_MediaMime
        {
            get { return datatext_MediaMime; }
            set
            {
                if (datatext_MediaMime == value) return;

                datatext_MediaMime = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_MediaMime);

            }
        }

        private bool ischecked_AutoPlay;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.CheckBox, ViewItemId = "isCheckedAutoPlay", ViewItemType = ViewItemType.Checked)]
		public bool IsChecked_AutoPlay
        {
            get { return ischecked_AutoPlay; }
            set
            {

                ischecked_AutoPlay = value;

                RaisePropertyChanged(nameof(IsChecked_AutoPlay));

            }
        }

        private string datatext_LastSavedBookmark;
        [ViewModel(Send = true)]
        public string DataText_LastSavedBookmark
        {
            get { return datatext_LastSavedBookmark; }
            set
            {
                if (datatext_LastSavedBookmark == value) return;

                datatext_LastSavedBookmark = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_LastSavedBookmark);

            }
        }

        private ViewMessage error_BookMark;
        [ViewModel(Send = true)]
        public ViewMessage Error_BookMark
        {
            get { return error_BookMark; }
            set
            {
                if (error_BookMark == value) return;

                error_BookMark = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Error_BookMark);

            }
        }

        private string datatext_ViewId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewId", ViewItemType = ViewItemType.Other)]
        public string DataText_ViewId
        {
            get { return datatext_ViewId; }
            set
            {
                if (datatext_ViewId == value) return;

                datatext_ViewId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ViewId);

            }
        }

        private string datatext_AttributeTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "attributeTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_AttributeTypeId
        {
            get { return datatext_AttributeTypeId; }
            set
            {
                if (datatext_AttributeTypeId == value) return;

                datatext_AttributeTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_AttributeTypeId);

            }
        }

        private string datatext_RelationTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "relationTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_RelationTypeId
        {
            get { return datatext_RelationTypeId; }
            set
            {
                if (datatext_RelationTypeId == value) return;

                datatext_RelationTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_RelationTypeId);

            }
        }

        private string datatext_ObjectId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "objectId", ViewItemType = ViewItemType.Other)]
        public string DataText_ObjectId
        {
            get { return datatext_ObjectId; }
            set
            {
                if (datatext_ObjectId == value) return;

                datatext_ObjectId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ObjectId);

            }
        }

        private string datatext_ClassId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "classId", ViewItemType = ViewItemType.Other)]
        public string DataText_ClassId
        {
            get { return datatext_ClassId; }
            set
            {
                if (datatext_ClassId == value) return;

                datatext_ClassId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ClassId);

            }
        }

        public KendoMediaItem itemToAdd;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "itemToAdd", ViewItemType = ViewItemType.Other)]
        public KendoMediaItem ItemToAdd
        {
            get { return itemToAdd; }
            set
            {
                itemToAdd = value;
                RaisePropertyChanged(nameof(ItemToAdd));
            }
        }

        private bool istoggled_ToggleBookmarks;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "toggleBookmarks", ViewItemType = ViewItemType.Checked)]
        public bool IsToggled_ToggleBookmarks
        {
            get { return istoggled_ToggleBookmarks; }
            set
            {

                istoggled_ToggleBookmarks = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_ToggleBookmarks);

            }
        }
    }
}
