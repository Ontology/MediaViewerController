﻿using MediaViewerController.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Controllers
{
    public class MediaListViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
		public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
		public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private JqxColumnList columnconfig_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.ColumnConfig)]
		public JqxColumnList ColumnConfig_Grid
        {
            get { return columnconfig_Grid; }
            set
            {
                if (columnconfig_Grid == value) return;

                columnconfig_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ColumnConfig_Grid);

            }
        }

        private JqxDataSource jqxdatasource_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.DataSource)]
		public JqxDataSource JqxDataSource_Grid
        {
            get { return jqxdatasource_Grid; }
            set
            {
                if (jqxdatasource_Grid == value) return;

                jqxdatasource_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_Grid);

            }
        }

        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
		public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(NotifyChanges.View_Text_View);

            }
        }

        private string url_Player;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.IFrame, ViewItemId = "iframePlayer", ViewItemType = ViewItemType.Content)]
		public string Url_Player
        {
            get { return url_Player; }
            set
            {
                if (url_Player == value) return;

                url_Player = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_Player);

            }
        }

        private string url_Upload;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.IFrame, ViewItemId = "iframeUpload", ViewItemType = ViewItemType.Content)]
		public string Url_Upload
        {
            get { return url_Upload; }
            set
            {

                url_Upload = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_Upload);

            }
        }

        private ColumnSortItem columnsortitem_ColumnSortItem;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.Sort)]
		public ColumnSortItem ColumnSortItem_ColumnSortItem
        {
            get { return columnsortitem_ColumnSortItem; }
            set
            {
                if (columnsortitem_ColumnSortItem == value) return;

                columnsortitem_ColumnSortItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ColumnSortItem_ColumnSortItem);

            }
        }

    }
}
