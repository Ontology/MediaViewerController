﻿using MediaViewerController.Factories;
using MediaViewerController.Models;
using MediaViewerController.Services;
using MediaViewerController.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.Services;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.ComponentModel;

namespace MediaViewerController.Controllers
{
    public class AudioPlayerViewController : AudioPlayerViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private MediaServiceAgent_Elastic serviceAgentElastic;
        private ViewMediaFactory viewMediaFactory;
        private bool isCurrentAudioPrepared;

        private clsOntologyItem referenceItem;

        private bool audiosLoadedFromDb;

        private List<MultimediaViewItem> multimediaViewItems = new List<MultimediaViewItem>();

        private Timer errorHideTimer;

        private clsLocalConfig localConfig;
        private double lastPosSaved = 0;

        private MultimediaViewItem currentItem;
        private CultureInfo positionFormatCulture = CultureInfo.CreateSpecificCulture("en-US");
        private NumberStyles positionNumberStyle = NumberStyles.Number;

        private double playFromSec;
        private bool doPlayFromSec;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public AudioPlayerViewController()
        {
            
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            errorHideTimer = new Timer();
            errorHideTimer.Interval = 3000;
            errorHideTimer.Elapsed += ErrorHideTimer_Elapsed;

            PropertyChanged += AudioPlayerViewController_PropertyChanged;
        }

        private void ErrorHideTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            errorHideTimer.Stop();
            Error_BookMark = null;
        }

        private void AudioPlayerViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            if (e.PropertyName == Notifications.NotifyChanges.ViewModel_IsSuccessful_Login ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_NavFirst ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_NavLast ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_NavNext ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_NavPrevious ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Label_Nav ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Url_Audio ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Count_ToSave ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Label_MediaItem ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_DataText_MediaMime ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsChecked_AutoPlay ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_DataText_LastSavedBookmark ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Error_BookMark ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsToggled_Listen ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Sec_MoveTo ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsToggled_ToggleBookmarks ||
                e.PropertyName == nameof(MultimediaItem))
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_Position_AudioItems)
            {
                NavMultiMediaItems();
            }
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new MediaServiceAgent_Elastic(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;

        }

        private void StateMachine_closedSocket()
        {
            viewMediaFactory?.StopFactory();
            webSocketServiceAgent?.RemoveAllResources();
            viewMediaFactory = null;
            serviceAgentElastic = null;
            translationController = null;
            multimediaViewItems = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {

            IsToggled_Listen = true;

            if (DataText_AttributeTypeId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_AttributeTypeId, localConfig.Globals.Type_AttributeType);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }
            else if (DataText_RelationTypeId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_RelationTypeId, localConfig.Globals.Type_RelationType);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }
            else if (DataText_ClassId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_ClassId, localConfig.Globals.Type_Class);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }
            else if (DataText_ObjectId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_ObjectId, localConfig.Globals.Type_Object);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }

           
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.SelectedObject,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Notifications.NotifyChanges.Channel_SelectedBookmark,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            var parameterRequest = new InterServiceMessage
            {
                ChannelId = Channels.ParameterList,
                SenderId = webSocketServiceAgent.EndpointId
            };
            webSocketServiceAgent.SendInterModMessage(parameterRequest);

            IsToggled_ToggleBookmarks = true;
           
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void ViewMediaItemFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.ViewMediaFactory_NextMultimediaItem)
            {
                if (multimediaViewItems == null)
                {
                    multimediaViewItems = new List<MultimediaViewItem>();
                }
                multimediaViewItems.Add(viewMediaFactory.NextMultimediaItem);
                Count_PlaylistItems = multimediaViewItems.Count;
                PrepareNextMultimediaItem();
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewMediaFactory_MultimediaItemCountToSave)
            {
                Count_ToSave = viewMediaFactory.MultiMediaCountToSave;
            }
        }

        private void NavMultiMediaItems()
        {
            currentItem = multimediaViewItems[Position_AudioItems];
            if (currentItem.NameFile.ToLower().EndsWith("." + Properties.Settings.Default.ExtensionMP3))
            {
                DataText_MediaMime = Properties.Settings.Default.MediaType_MP3;
            }
            else
            {
                DataText_MediaMime = Properties.Settings.Default.MediaType_WAV;
            }
            
            Url_Audio = currentItem.MultimediaUrl;
            Label_MediaItem = currentItem.Name;
            lastPosSaved = 0;

            ConfigureNav();
        }

        private void PrepareNextMultimediaItem()
        {

            lastPosSaved = 0;
            ConfigureNav();
            //if (isCurrentAudioPrepared) return;

            currentItem = multimediaViewItems.LastOrDefault();
            if (currentItem == null) return;
            MultimediaItem = currentItem;
            Label_MediaItem = currentItem != null ? currentItem.Name : "";

            isCurrentAudioPrepared = true;

        }

        private void ConfigureNav()
        {
            var toggleBack = false;
            var toggleForward = false;

            if (IsPossibleBackward())
            {
                toggleBack = true;
            }

            if (IsPossibleForward())
            {
                toggleForward = true;
            }

            ToggleNavBack(toggleBack);
            ToggleNavForward(toggleForward);

            var ix = (multimediaViewItems == null || !multimediaViewItems.Any() ? 0 : Position_AudioItems + 1);
            var count = (multimediaViewItems == null ? 0 : multimediaViewItems.Count);

            Label_Nav = ix + "/" + count;
        }

        private bool IsPossibleBackward()
        {
            return Position_AudioItems > 0;
        }

        private bool IsPossibleForward()
        {
            return multimediaViewItems != null && multimediaViewItems.Any() && Position_AudioItems < multimediaViewItems.Count - 1;
        }

        private void ToggleNavBack(bool toggleTo)
        {
            IsEnabled_NavFirst = IsEnabled_NavPrevious = toggleTo;
        }

        private void ToggleNavForward(bool toggleTo)
        {
            IsEnabled_NavLast = IsEnabled_NavNext = toggleTo;
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultData)
            {


                if (serviceAgentElastic.ResultData.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return;


                }
                audiosLoadedFromDb = true;
                TestInitializationFactory();

            }


        }

        private void TestInitializationFactory()
        {
            if (audiosLoadedFromDb)
            {
                if (viewMediaFactory == null)
                {
                    viewMediaFactory = new ViewMediaFactory(localConfig, MultimediaItemType.Audio, webSocketServiceAgent.oItemUser, webSocketServiceAgent.oItemGroup);
                    viewMediaFactory.PropertyChanged += ViewMediaItemFactory_PropertyChanged;
                }

                var result = viewMediaFactory.SaveMultimediaFiles(serviceAgentElastic.RefToMediafiles,
                    serviceAgentElastic.MediaAttributes,
                    serviceAgentElastic.MediaFiles,
                    webSocketServiceAgent.GetFileSystemObject(),
                    webSocketServiceAgent.DataText_SessionId,
                    IsChecked_Random);

            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;
            
        }

     

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavFirst)
                {
                    

                    Position_AudioItems = 0;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavPrevious)
                {
                    Position_AudioItems--;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavNext)
                {
                    if (IsChecked_Random)
                    {
                        Random r = new Random();
                        int rNumber = r.Next(0, multimediaViewItems.Count - 1);
                        Position_AudioItems = rNumber;
                    }
                    else
                    {
                        Position_AudioItems++;
                    }
                    
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavLast)
                {
                    Position_AudioItems = multimediaViewItems.Count - 1;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_Stopped && IsToggled_ToggleBookmarks)
                {
                    
                    //if (IsChecked_AutoPlay && IsPossibleForward())
                    //{
                    //    Position_AudioItems++;
                    //}
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_CurPos && IsToggled_ToggleBookmarks)
                {
                    
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_Started && IsToggled_ToggleBookmarks)
                {
                    
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_Paused && IsToggled_ToggleBookmarks)
                {
                    
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_Loaded && IsToggled_ToggleBookmarks)
                {
                    if (doPlayFromSec)
                    {
                        Sec_MoveTo = playFromSec;
                    }
                }
                


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.ViewModel_IsChecked_AutoPlay)
                {
                    bool checkedCheck;
                    if (bool.TryParse(webSocketServiceAgent.ChangedProperty.Value.ToString(),out checkedCheck))
                    {
                        IsChecked_AutoPlay = checkedCheck;
                    }
                    
                }
                else if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.ViewModel_IsChecked_Random)
                {
                    bool checkedCheck;
                    if (bool.TryParse(webSocketServiceAgent.ChangedProperty.Value.ToString(), out checkedCheck))
                    {
                        IsChecked_Random = checkedCheck;
                    }

                }
                
                else if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.ViewModel_IsToggled_Listen && webSocketServiceAgent.ChangedProperty.Value != null)
                {
                    bool checkValue;
                    if (bool.TryParse(webSocketServiceAgent.ChangedProperty.Value.ToString(),out checkValue))
                    {
                        IsToggled_Listen = checkValue;
                    }
                }
            }
            else if (e.PropertyName == NotifyChanges.Websocket_DedicatedSenderArgument)
            {
                if (webSocketServiceAgent.DedicatedSenderArgument != null)
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.ReceiverOfDedicatedSender,
                        ReceiverId = webSocketServiceAgent.DedicatedSenderArgument.Value
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(viewItem =>
                {
                    if (viewItem.ViewItemId == Notifications.NotifyChanges.Command_Started )
                    {
                        var item = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(viewItem.ViewItemValue.ToString());
                        SaveBookmark(localConfig.OItem_token_logstate_start, item);
                    }
                    else if (viewItem.ViewItemId == Notifications.NotifyChanges.Command_CurPos && IsToggled_ToggleBookmarks)
                    {
                        //var item = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(viewItem.ViewItemValue.ToString());
                        //SaveBookmark(localConfig.OItem_object_position, item);
                    }
                    else if (viewItem.ViewItemId == Notifications.NotifyChanges.Command_Paused && IsToggled_ToggleBookmarks)
                    {
                        var item = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(viewItem.ViewItemValue.ToString());
                        SaveBookmark(localConfig.OItem_token_logstate_pause, item);
                    }
                    else if (viewItem.ViewItemId == Notifications.NotifyChanges.Command_Stopped && IsToggled_ToggleBookmarks)
                    {
                        var item = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(viewItem.ViewItemValue.ToString());
                        SaveBookmark(localConfig.OItem_token_logstate_stop, item);
                    }


                }); 
            }


        }

        private void SaveBookmark(clsOntologyItem logState, Dictionary<string, object> item)
        {
            var mediaItem = multimediaViewItems.FirstOrDefault(itm => itm.Id == item["id"].ToString());
            double pos = (double)item["currentTime"];
            
            if (logState.GUID != localConfig.OItem_object_position.GUID || (logState.GUID == localConfig.OItem_object_position.GUID && pos - lastPosSaved > 10))
            {
                var result = serviceAgentElastic.SavePos(pos, mediaItem.OMultimediaItem, webSocketServiceAgent.oItemUser, referenceItem, logState);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    Error_BookMark = new OntoMsg_Module.Models.ViewMessage("No Bookmark", "Save-Error", OntoMsg_Module.Models.ViewMessageType.Exclamation);
                    errorHideTimer.Start();

                }
                else
                {
                    DataText_LastSavedBookmark = "Last Bookmark:" + result.Val_Date.ToString();
                    lastPosSaved = pos;
                }

            }
            
        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void LoadMedia()
        {
            if (viewMediaFactory != null)
            {
                viewMediaFactory.StopFactory();
            }
            Position_AudioItems = 0;
            Count_PlaylistItems = 0;
            multimediaViewItems = new List<MultimediaViewItem>();
            Url_Audio = "";
            Count_ToSave = 0;
            isCurrentAudioPrepared = false;
            ConfigureNav();
            audiosLoadedFromDb = false;
            serviceAgentElastic.GetReferencedMediaItems(referenceItem);
        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.SelectedObject && IsToggled_Listen)
            {
                var objectItem = message.OItems.LastOrDefault();

                if (objectItem == null) return;

                if (referenceItem != null && referenceItem.GUID == objectItem.GUID) return;
                referenceItem = objectItem;
                LoadMedia();
            }
            else if (message.ChannelId == Notifications.NotifyChanges.Channel_SelectedBookmark)
            {
                var objectItem = message.OItems.LastOrDefault();

                if (objectItem == null) return;

                var selectedItem = multimediaViewItems.FirstOrDefault(mediaItem => mediaItem.Id == objectItem.GUID);
                

                if (selectedItem == null) return;
                playFromSec = objectItem.Val_Real.Value;
                doPlayFromSec = true;
                var position = multimediaViewItems.IndexOf(selectedItem);
                if (position == Position_AudioItems)
                {
                    Sec_MoveTo = playFromSec;
                }
                else
                {
                    Position_AudioItems = position;
                }
                
                
            }
            else if (message.ChannelId == Channels.ParameterList)
            {
                var optionsParam = message.GenericParameterItems.Where(param => param != null).FirstOrDefault(param => param.ToString().ToLower().StartsWith("options".ToLower()));
                if (optionsParam != null)
                {
                    var paramSplit = optionsParam.ToString().Split('=');
                    if (paramSplit.Count() == 2)
                    {
                        var options = paramSplit[1];
                        var optionsSplit = options.Split(',');
                        optionsSplit.ToList().ForEach(optionItem =>
                        {
                            if (optionItem.ToLower() == "autoplay".ToLower())
                            {
                                IsChecked_AutoPlay = true;
                            }
                            else if (optionItem.ToLower() == "random".ToLower())
                            {
                                IsChecked_Random = true;
                            }
                        });
                        
                    }
                }

            }
            else if (message.ChannelId == Notifications.NotifyChanges.Channel_MediaItemList)
            {
                if (message.OItems == null || message.OItems.Count != 1) return;

                referenceItem = message.OItems.First();
                var genericParameters = message.GenericParameterItems;

                if (genericParameters == null) return;

                var firstParam = genericParameters.FirstOrDefault();
                

                if (firstParam == null) return;

                var mediaListItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(firstParam.ToString()).Select(dynItm => new MediaListItem
                {
                    IdAttributeCreated = dynItm.IdAttributeCreated,
                    BookmarkCount = dynItm.BookmarkCount,
                    Created = dynItm.Created,
                    IdFile = dynItm.IdFile,
                    NameFile = dynItm.NameFile,
                    IdItem = dynItm.IdItem,
                    IdRef = dynItm.IdRef,
                    IdRow = dynItm.IdRow,
                    NameItem = dynItm.NameItem,
                    NameRef = dynItm.NameRef,
                    OrderId = dynItm.OrderId,
                    Random = dynItm.Random
                }).ToList();

                

                audiosLoadedFromDb = true;
                if (audiosLoadedFromDb)
                {
                    if (viewMediaFactory == null)
                    {
                        viewMediaFactory = new ViewMediaFactory(localConfig, MultimediaItemType.Audio, webSocketServiceAgent.oItemUser, webSocketServiceAgent.oItemGroup);
                        viewMediaFactory.PropertyChanged += ViewMediaItemFactory_PropertyChanged;
                    }

                    var result = viewMediaFactory.SaveMultimediaFiles(mediaListItems, webSocketServiceAgent.GetFileSystemObject(), webSocketServiceAgent.DataText_SessionId);

                }
            }
        }

        
        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
