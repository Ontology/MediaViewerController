﻿using MediaViewerController.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Controllers
{
    public class PDFViewerViewModel : OntoMsg_Module.Base.ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool isenabled_NavFirst;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_NavFirst
        {
            get { return isenabled_NavFirst; }
            set
            {
                if (isenabled_NavFirst == value) return;

                isenabled_NavFirst = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavFirst);

            }
        }

        private bool isenabled_NavPrevious;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_NavPrevious
        {
            get { return isenabled_NavPrevious; }
            set
            {
                if (isenabled_NavPrevious == value) return;

                isenabled_NavPrevious = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavPrevious);

            }
        }

        private int count_ToSave;
        [ViewModel(Send = true)]
        public int Count_ToSave
        {
            get { return count_ToSave; }
            set
            {
                if (count_ToSave == value) return;

                count_ToSave = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Count_ToSave);

            }
        }

        private bool isenabled_NavNext;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_NavNext
        {
            get { return isenabled_NavNext; }
            set
            {
                if (isenabled_NavNext == value) return;

                isenabled_NavNext = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavNext);

            }
        }

        private bool isenabled_NavLast;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_NavLast
        {
            get { return isenabled_NavLast; }
            set
            {
                if (isenabled_NavLast == value) return;

                isenabled_NavLast = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavLast);

            }
        }

        private string label_Nav;
        [ViewModel(Send = true)]
        public string Label_Nav
        {
            get { return label_Nav; }
            set
            {
                if (label_Nav == value) return;

                label_Nav = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Nav);

            }
        }

        private int position_PDFItems;
        [ViewModel(Send = true)]
        public int Position_PDFItems
        {
            get { return position_PDFItems; }
            set
            {
                if (position_PDFItems == value) return;

                position_PDFItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Position_PDFItems);

            }
        }

        private string url_PDF;
        [ViewModel(Send = true)]
        public string Url_PDF
        {
            get { return url_PDF; }
            set
            {

                url_PDF = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_PDF);

            }
        }

        private int count_PlaylistItems;
        [ViewModel(Send = true)]
        public int Count_PlaylistItems
        {
            get { return count_PlaylistItems; }
            set
            {
                if (count_PlaylistItems == value) return;

                count_PlaylistItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Count_PlaylistItems);

            }
        }

        private string label_MediaItem;
        [ViewModel(Send = true)]
        public string Label_MediaItem
        {
            get { return label_MediaItem; }
            set
            {

                label_MediaItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_MediaItem);

            }
        }

        private string datatext_MediaMime;
        [ViewModel(Send = true)]
        public string DataText_MediaMime
        {
            get { return datatext_MediaMime; }
            set
            {
                if (datatext_MediaMime == value) return;

                datatext_MediaMime = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_MediaMime);

            }
        }

        private bool isvisible_OpenPdf;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_OpenPdf
        {
            get { return isvisible_OpenPdf; }
            set
            {
                if (isvisible_OpenPdf == value) return;

                isvisible_OpenPdf = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_OpenPdf);

            }
        }

        private bool isenabled_OpenPdf;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_OpenPdf
        {
            get { return isenabled_OpenPdf; }
            set
            {
                if (isenabled_OpenPdf == value) return;

                isenabled_OpenPdf = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_OpenPdf);

            }
        }

        private string label_OpenPdf;
        [ViewModel(Send = true)]
        public string Label_OpenPdf
        {
            get { return label_OpenPdf; }
            set
            {
                if (label_OpenPdf == value) return;

                label_OpenPdf = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_OpenPdf);

            }
        }

        private bool isvisible_AutoOpen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_AutoOpen
        {
            get { return isvisible_AutoOpen; }
            set
            {
                if (isvisible_AutoOpen == value) return;

                isvisible_AutoOpen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_AutoOpen);

            }
        }

        private bool isenabled_AutoOpen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_AutoOpen
        {
            get { return isenabled_AutoOpen; }
            set
            {
                if (isenabled_AutoOpen == value) return;

                isenabled_AutoOpen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_AutoOpen);

            }
        }

        private bool ischecked_AutoOpen;
        [ViewModel(Send = true)]
        public bool IsChecked_AutoOpen
        {
            get { return ischecked_AutoOpen; }
            set
            {
                if (ischecked_AutoOpen == value) return;

                ischecked_AutoOpen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsChecked_AutoOpen);

            }
        }

        private string datatext_ViewId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewId", ViewItemType = ViewItemType.Other)]
        public string DataText_ViewId
        {
            get { return datatext_ViewId; }
            set
            {
                if (datatext_ViewId == value) return;

                datatext_ViewId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ViewId);

            }
        }

        private string datatext_AttributeTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "attributeTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_AttributeTypeId
        {
            get { return datatext_AttributeTypeId; }
            set
            {
                if (datatext_AttributeTypeId == value) return;

                datatext_AttributeTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_AttributeTypeId);

            }
        }

        private string datatext_RelationTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "relationTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_RelationTypeId
        {
            get { return datatext_RelationTypeId; }
            set
            {
                if (datatext_RelationTypeId == value) return;

                datatext_RelationTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_RelationTypeId);

            }
        }

        private string datatext_ObjectId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "objectId", ViewItemType = ViewItemType.Other)]
        public string DataText_ObjectId
        {
            get { return datatext_ObjectId; }
            set
            {
                if (datatext_ObjectId == value) return;

                datatext_ObjectId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ObjectId);

            }
        }

        private string datatext_ClassId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "classId", ViewItemType = ViewItemType.Other)]
        public string DataText_ClassId
        {
            get { return datatext_ClassId; }
            set
            {
                if (datatext_ClassId == value) return;

                datatext_ClassId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ClassId);

            }
        }
    }
}
