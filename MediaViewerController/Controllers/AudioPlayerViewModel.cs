﻿using MediaViewerController.Models;
using MediaViewerController.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Controllers
{
    public class AudioPlayerViewModel : OntoMsg_Module.Base.ViewModelBase
    {
        private bool isToggled_Listen;
        [ViewModel(Send = true)]
        public bool IsToggled_Listen
        {
            get { return isToggled_Listen; }
            set
            {
                if (isToggled_Listen == value) return;

                isToggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private bool issuccessful_Login;
        [ViewModel(Send = true)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool isenabled_NavFirst;
        [ViewModel(Send = true)]
        public bool IsEnabled_NavFirst
        {
            get { return isenabled_NavFirst; }
            set
            {
                if (isenabled_NavFirst == value) return;

                isenabled_NavFirst = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavFirst);

            }
        }

        private bool isenabled_NavPrevious;
        [ViewModel(Send = true)]
        public bool IsEnabled_NavPrevious
        {
            get { return isenabled_NavPrevious; }
            set
            {
                if (isenabled_NavPrevious == value) return;

                isenabled_NavPrevious = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavPrevious);

            }
        }

        private int width_Canvas;
        [ViewModel(Send = true)]
        public int Width_Canvas
        {
            get { return width_Canvas; }
            set
            {
                if (width_Canvas == value) return;

                width_Canvas = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Width_Canvas);

            }
        }

        private int count_ToSave;
        [ViewModel(Send = true)]
        public int Count_ToSave
        {
            get { return count_ToSave; }
            set
            {
                if (count_ToSave == value) return;

                count_ToSave = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Count_ToSave);

            }
        }

        private bool isenabled_NavNext;
        [ViewModel(Send = true)]
        public bool IsEnabled_NavNext
        {
            get { return isenabled_NavNext; }
            set
            {
                if (isenabled_NavNext == value) return;

                isenabled_NavNext = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavNext);

            }
        }

        private bool isenabled_NavLast;
        [ViewModel(Send = true)]
        public bool IsEnabled_NavLast
        {
            get { return isenabled_NavLast; }
            set
            {
                if (isenabled_NavLast == value) return;

                isenabled_NavLast = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavLast);

            }
        }

        private string label_Nav;
        [ViewModel(Send = true)]
        public string Label_Nav
        {
            get { return label_Nav; }
            set
            {
                if (label_Nav == value) return;

                label_Nav = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Nav);

            }
        }

        private int position_AudioItems;
        [ViewModel(Send = true)]
        public int Position_AudioItems
        {
            get { return position_AudioItems; }
            set
            {
                if (position_AudioItems == value) return;

                position_AudioItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Position_AudioItems);

            }
        }

        private string url_Audio;
        [ViewModel(Send = true)]
        public string Url_Audio
        {
            get { return url_Audio; }
            set
            {
                if (url_Audio == value) return;

                url_Audio = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_Audio);

            }
        }

        private MultimediaViewItem multimediaItem;
        [ViewModel(Send = true)]
        public MultimediaViewItem MultimediaItem
        {
            get { return multimediaItem; }
            set
            {
                if (multimediaItem == value) return;

                multimediaItem = value;

                RaisePropertyChanged(nameof(MultimediaItem));
            }
        }


        private int count_PlaylistItems;
        [ViewModel(Send = true)]
        public int Count_PlaylistItems
        {
            get { return count_PlaylistItems; }
            set
            {
                if (count_PlaylistItems == value) return;

                count_PlaylistItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Count_PlaylistItems);

            }
        }

        private string label_MediaItem;
        [ViewModel(Send = true)]
        public string Label_MediaItem
        {
            get { return label_MediaItem; }
            set
            {

                label_MediaItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_MediaItem);

            }
        }

        private string datatext_MediaMime;
        [ViewModel(Send = true)]
        public string DataText_MediaMime
        {
            get { return datatext_MediaMime; }
            set
            {
                if (datatext_MediaMime == value) return;

                datatext_MediaMime = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_MediaMime);

            }
        }

        private bool ischecked_AutoPlay = false;
        [ViewModel(Send = true)]
        public bool IsChecked_AutoPlay
        {
            get { return ischecked_AutoPlay; }
            set
            {
                if (ischecked_AutoPlay == value) return;

                ischecked_AutoPlay = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsChecked_AutoPlay);

            }
        }

        private bool ischecked_Random = false;
        [ViewModel(Send = true)]
        public bool IsChecked_Random
        {
            get { return ischecked_Random; }
            set
            {
                if (ischecked_Random == value) return;

                ischecked_Random = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsChecked_Random);

            }
        }

        private string datatext_LastSavedBookmark;
        [ViewModel(Send = true)]
        public string DataText_LastSavedBookmark
        {
            get { return datatext_LastSavedBookmark; }
            set
            {
                if (datatext_LastSavedBookmark == value) return;

                datatext_LastSavedBookmark = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_LastSavedBookmark);

            }
        }

        private double sec_MoveTo;
        [ViewModel(Send = true)]
        public double Sec_MoveTo
        {
            get { return sec_MoveTo; }
            set
            {
                if (sec_MoveTo == value) return;

                sec_MoveTo = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Sec_MoveTo);

            }
        }

        private ViewMessage error_BookMark;
        [ViewModel(Send = true)]
        public ViewMessage Error_BookMark
        {
            get { return error_BookMark; }
            set
            {
                if (error_BookMark == value) return;

                error_BookMark = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Error_BookMark);

            }
        }

        private string datatext_ViewId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewId", ViewItemType = ViewItemType.Other)]
        public string DataText_ViewId
        {
            get { return datatext_ViewId; }
            set
            {
                if (datatext_ViewId == value) return;

                datatext_ViewId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ViewId);

            }
        }

        private string datatext_AttributeTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "attributeTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_AttributeTypeId
        {
            get { return datatext_AttributeTypeId; }
            set
            {
                if (datatext_AttributeTypeId == value) return;

                datatext_AttributeTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_AttributeTypeId);

            }
        }

        private string datatext_RelationTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "relationTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_RelationTypeId
        {
            get { return datatext_RelationTypeId; }
            set
            {
                if (datatext_RelationTypeId == value) return;

                datatext_RelationTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_RelationTypeId);

            }
        }

        private string datatext_ObjectId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "objectId", ViewItemType = ViewItemType.Other)]
        public string DataText_ObjectId
        {
            get { return datatext_ObjectId; }
            set
            {
                if (datatext_ObjectId == value) return;

                datatext_ObjectId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ObjectId);

            }
        }

        private string datatext_ClassId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "classId", ViewItemType = ViewItemType.Other)]
        public string DataText_ClassId
        {
            get { return datatext_ClassId; }
            set
            {
                if (datatext_ClassId == value) return;

                datatext_ClassId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ClassId);

            }
        }

        private bool istoggled_ToggleBookmarks;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "toggleBookmarks", ViewItemType = ViewItemType.Checked)]
		public bool IsToggled_ToggleBookmarks
        {
            get { return istoggled_ToggleBookmarks; }
            set
            {

                istoggled_ToggleBookmarks = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_ToggleBookmarks);

            }
        }
    }
}
