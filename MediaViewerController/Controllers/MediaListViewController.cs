﻿using MediaViewerController.Factories;
using MediaViewerController.Models;
using MediaViewerController.Services;
using MediaViewerController.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace MediaViewerController.Controllers
{
    public class MediaListViewController : MediaListViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private MediaListFactory mediaListFactory;

        private MediaViewerController.Connectors.MediaConnector mediaConnector;

        private MediaListServiceAgent serviceAgentElastic;

        private clsLocalConfig localConfig;

        private JsonFactory jsonFactory;

        private clsOntologyItem selectedObject;

        private string viewIdImageItemList = "cf260392e0a24d32a2f2162e9795de0a";
        private string viewIdAudioItemList = "aad87369aa564a439fe6bd727750e44b";
        private string viewIdVideoItemList = "0ecd03c9162c45839331260af4336af7";
        private string viewIdPDFItemList = "77ea69778d4e4d9fbd2ffee8bd122636";

        private List<string> subReceivers = new List<string>();

        private ViewMetaItem uploadView;
        private ViewMetaItem playerView;

        private string fileNameData;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public MediaListViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += MediaListViewController_PropertyChanged;
        }

        private void MediaListViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);

            if (e.PropertyName == Notifications.NotifyChanges.ViewModel_ColumnSortItem_ColumnSortItem)
            {
                if (ColumnSortItem_ColumnSortItem != null)
                {
                    var column = ColumnSortItem_ColumnSortItem.Column;
                    if (column == "NameItem")
                    {
                        if (ColumnSortItem_ColumnSortItem.Direction == "ascending")
                        {
                            mediaListFactory.MediaListItems = mediaListFactory.MediaListItems.OrderBy(medItm => medItm.NameItem).ToList();
                        }
                        else
                        {
                            mediaListFactory.MediaListItems = mediaListFactory.MediaListItems.OrderByDescending(medItm => medItm.NameItem).ToList();
                        }
                        
                    }
                    else if (column == "Created")
                    {
                        if (ColumnSortItem_ColumnSortItem.Direction == "ascending")
                        {
                            mediaListFactory.MediaListItems = mediaListFactory.MediaListItems.OrderBy(medItm => medItm.Created).ToList();
                        }
                        else
                        {
                            mediaListFactory.MediaListItems = mediaListFactory.MediaListItems.OrderByDescending(medItm => medItm.Created).ToList();
                        }

                    }
                    else if (column == "OrderId")
                    {
                        if (ColumnSortItem_ColumnSortItem.Direction == "ascending")
                        {
                            mediaListFactory.MediaListItems = mediaListFactory.MediaListItems.OrderBy(medItm => medItm.OrderId).ToList();
                        }
                        else
                        {
                            mediaListFactory.MediaListItems = mediaListFactory.MediaListItems.OrderByDescending(medItm => medItm.OrderId).ToList();
                        }

                    }
                    else if (column == "BookmarkCount")
                    {
                        if (ColumnSortItem_ColumnSortItem.Direction == "ascending")
                        {
                            mediaListFactory.MediaListItems = mediaListFactory.MediaListItems.OrderBy(medItm => medItm.BookmarkCount).ToList();
                        }
                        else
                        {
                            mediaListFactory.MediaListItems = mediaListFactory.MediaListItems.OrderByDescending(medItm => medItm.BookmarkCount).ToList();
                        }

                    }
                    else if (column == "Random")
                    {
                        if (ColumnSortItem_ColumnSortItem.Direction == "ascending")
                        {
                            mediaListFactory.MediaListItems = mediaListFactory.MediaListItems.OrderBy(medItm => medItm.Random).ToList();
                        }
                        else
                        {
                            mediaListFactory.MediaListItems = mediaListFactory.MediaListItems.OrderByDescending(medItm => medItm.Random).ToList();
                        }

                    }
                }
            }
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new MediaListServiceAgent(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;

            mediaListFactory = new MediaListFactory(localConfig);
            mediaListFactory.PropertyChanged += MediaListFactory_PropertyChanged;

            jsonFactory = new JsonFactory();

            mediaConnector = new Connectors.MediaConnector(localConfig.Globals);
            mediaConnector.PropertyChanged += MediaConnector_PropertyChanged;

        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();

            serviceAgentElastic.StopRead();
            serviceAgentElastic = null;
            translationController = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            IsToggled_Listen = true;

            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            fileNameData = Guid.NewGuid().ToString() + ".json";
            var dataSessionFile = webSocketServiceAgent.RequestWriteStream(fileNameData);
            if (dataSessionFile == null) return;

            using (dataSessionFile.StreamWriter)
            {
                using (var jsonWriter = new Newtonsoft.Json.JsonTextWriter(dataSessionFile.StreamWriter))
                {
                    jsonWriter.WriteStartArray();
                    jsonWriter.WriteEndArray();
                }
            }

            var columnConfig = GridFactory.CreateColumnList(typeof(MediaListItem));
            ColumnConfig_Grid = columnConfig;

            var dataSource = GridFactory.CreateJqxDataSource(typeof(MediaListItem), 0, 200, dataSessionFile.FileUri.AbsoluteUri);

            JqxDataSource_Grid = dataSource;

            var uploadView = ModuleDataExchanger.GetViewById("1389c3c175984954a1a38dcc83f7472c");
            if (uploadView != null)
            {
                var mimeAccept = GetMimeAccept();
                var url = ModuleDataExchanger.GetViewUrlById(uploadView.IdView, webSocketServiceAgent.EndpointId) + mimeAccept;


                Url_Upload = url;
            }

            var playerViewId = webSocketServiceAgent.IdEntryView == viewIdImageItemList ? "86ee9703e61944a5b3b900fbdf7f6bca" :
                    webSocketServiceAgent.IdEntryView == viewIdAudioItemList ? "17c5ef59635741a89b6defbc4df216b6" :
                    webSocketServiceAgent.IdEntryView == viewIdVideoItemList ? "8146ee43dabb490b9fcab9d8f10f6345" :
                    webSocketServiceAgent.IdEntryView == viewIdPDFItemList ? "2d0879607a74478c8cfef5a668b9db89" : null;

            if (playerViewId != null)
            {
                var playerView = ModuleDataExchanger.GetViewById(playerViewId);

                if (playerView != null)
                {
                    var url = webSocketServiceAgent.GetFileSystemObject().BaseUrl +
                        (webSocketServiceAgent.GetFileSystemObject().BaseUrl.EndsWith("/") || playerView.NameCommandLineRun.StartsWith("/") ? "" : "/") +
                        playerView.NameCommandLineRun + "?Sender=" + webSocketServiceAgent.EndpointId;

                    Url_Player = url;

                }
            }




            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void MediaConnector_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (mediaConnector.ResultCreateMultimediaItems.Result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                webSocketServiceAgent.SendCommand("CloseUploadView");
                InitializeWithSelectedObject();
            }
        }

        private void MediaListFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.MediaListFactory_MediaListItems)
            {
                var dataSessionFile = webSocketServiceAgent.RequestWriteStream(fileNameData);
                if (dataSessionFile == null) return;

                using (dataSessionFile.StreamWriter)
                {
                    using (var jsonWriter = new Newtonsoft.Json.JsonTextWriter(dataSessionFile.StreamWriter))
                    {
                        jsonFactory.CreateJsonFileOfItemList(typeof(MediaListItem), mediaListFactory.MediaListItems.ToList<object>(), dataSessionFile);
                        webSocketServiceAgent.SendCommand("reloadGrid");
                    }
                }
            }
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.ElasticService_ResultMediaItemList)
            {
                if (serviceAgentElastic.ResultMediaItemList.GUID == localConfig.Globals.LState_Success.GUID)
                {
                   
                    var result = mediaListFactory.GetMediaItemList(serviceAgentElastic.MediaItemsToRef,
                        serviceAgentElastic.MediaItemsToFiles,
                        serviceAgentElastic.FilesToCreateDate,
                        serviceAgentElastic.BookmarksToMediaItems);
                }
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;

        }

       
        private string GetMimeAccept()
        {
            if (webSocketServiceAgent.IdEntryView == viewIdImageItemList)
            {
                return "&mimeAccept=image/*";
            }

            if (webSocketServiceAgent.IdEntryView == viewIdAudioItemList)
            {
                return "&mimeAccept=audio/*";
            }

            if (webSocketServiceAgent.IdEntryView == viewIdVideoItemList)
            {
                return "&mimeAccept=video/*";
            }

            if (webSocketServiceAgent.IdEntryView == viewIdPDFItemList)
            {
                return "&mimeAccept=" + MimeTypes.application_pdf;
            }

            return null;
        }

       

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "UploadItems")
                {
                    
                    webSocketServiceAgent.SendCommand("OpenUploadView");
                    

                    
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "OpenPlayer")
                {

                    var mediaListItems = mediaListFactory.MediaListItems;

                    if (mediaListItems != null)
                    {
                        subReceivers.ForEach(subReceiver =>
                        {
                            var interServiceMessage = new InterServiceMessage
                            {
                                ChannelId = Notifications.NotifyChanges.Channel_MediaItemList,
                                ReceiverId = subReceiver,
                                GenericParameterItems = new List<object> { mediaListItems },
                                OItems = new List<clsOntologyItem> { selectedObject }
                            };

                            webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                        });
                        
                    }
                    webSocketServiceAgent.SendCommand("OpenPlayerView");
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "selectedMediaItem")
                {
                    var idItem = webSocketServiceAgent.Request["idItem"].ToString();

                    var oItem = serviceAgentElastic.GetOItem(idItem, localConfig.Globals.Type_Object);

                    if (oItem == null) return;

                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.ParameterList,
                        OItems = new List<clsOntologyItem>
                            {
                                oItem
                            }
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    if (changedItem.ViewItemId == "grid" && changedItem.ViewItemType == "Sort")
                    {
                        ColumnSortItem_ColumnSortItem = Newtonsoft.Json.JsonConvert.DeserializeObject<ColumnSortItem>(changedItem.ViewItemValue.ToString());

                    }
                    if (changedItem.ViewItemId == "grid" && changedItem.ViewItemType == ViewItemType.SelectedIndex.ToString())
                    {


                    }
                });

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ObjectArgument)
            {
                var objectParameter = webSocketServiceAgent.ObjectArgument;
                var oItemId = objectParameter.Value;
                if (string.IsNullOrEmpty(oItemId) || !localConfig.Globals.is_GUID(oItemId)) return;

                var oItem = serviceAgentElastic.GetOItem(oItemId, localConfig.Globals.Type_Object);
                if (oItem == null) return;

                selectedObject = oItem;

                var classItem = serviceAgentElastic.GetOItem(selectedObject.GUID_Parent, localConfig.Globals.Type_Class);
                Text_View = selectedObject.Name + " (" + classItem.Name + ")";

                InitializeWithSelectedObject();
            }

        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ReceiverOfDedicatedSender)
            {
                if (!subReceivers.Contains(message.SenderId))
                {
                    subReceivers.Add(message.SenderId);
                }

                var viewReadyMessage = new InterServiceMessage
                {
                    ChannelId = Channels.ViewReady,
                    ReceiverId = webSocketServiceAgent.DedicatedSenderArgument.Value,
                    ViewId = webSocketServiceAgent.IdEntryView
                };

                webSocketServiceAgent.SendInterModMessage(viewReadyMessage);
            }
            else if (message.ChannelId == Channels.ParameterList && IsToggled_Listen)
            {
                var oItem = message.OItems.FirstOrDefault();

                if (oItem == null) return;
                IsToggled_Listen = false;
                selectedObject = serviceAgentElastic.GetOItem(oItem.GUID, localConfig.Globals.Type_Object);

                var classItem = serviceAgentElastic.GetOItem(selectedObject.GUID_Parent, localConfig.Globals.Type_Class);
                Text_View = selectedObject.Name + " (" + classItem.Name + ")";
                InitializeWithSelectedObject();


            }
            else if (message.ChannelId == FileUploadController.Notifications.NotifyChanges.Channel_FilesUploaded && selectedObject != null)
            {
                var fileItems = message.OItems;

                if (fileItems == null || !fileItems.Any()) return;


                var mimeTypeToCheck = GetMimeTypeCheck();
                if (mimeTypeToCheck == null) return;

                if (!fileItems.All(fileItem => fileItem.Additional1.ToLower().StartsWith(mimeTypeToCheck.ToLower())))
                {
                    return;
                }

                var resultTask = mediaConnector.CreateMultimediaItems(Converters.ViewIdToMediaTypeEnumConverter.Convert(webSocketServiceAgent.IdEntryView),
                    fileItems,
                    selectedObject);
            }
        }

       

        private string GetMimeTypeCheck()
        {
            if (webSocketServiceAgent.IdEntryView == viewIdImageItemList)
            {
                return "image/";
            }
            else if (webSocketServiceAgent.IdEntryView == viewIdAudioItemList)
            {
                return "audio/";
            }
            else if (webSocketServiceAgent.IdEntryView == viewIdVideoItemList)
            {
                return "video/";
            }
            else if (webSocketServiceAgent.IdEntryView == viewIdPDFItemList)
            {
                return MimeTypes.application_pdf;
            }

            return null;
        }

        private void InitializeWithSelectedObject()
        {
            if (selectedObject == null) return;


            if (webSocketServiceAgent.IdEntryView == viewIdImageItemList)
            {
                var result = serviceAgentElastic.GetMediaItemList(selectedObject, localConfig.OItem_type_images__graphic_);
            }
            else if (webSocketServiceAgent.IdEntryView == viewIdAudioItemList)
            {
                var result = serviceAgentElastic.GetMediaItemList(selectedObject, localConfig.OItem_type_media_item);
            }
            else if (webSocketServiceAgent.IdEntryView == viewIdVideoItemList)
            {
                var result = serviceAgentElastic.GetMediaItemList(selectedObject, localConfig.OItem_type_media_item);
            }
            else if (webSocketServiceAgent.IdEntryView == viewIdPDFItemList)
            {
                var result = serviceAgentElastic.GetMediaItemList(selectedObject, localConfig.OItem_type_pdf_documents);
            }
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
