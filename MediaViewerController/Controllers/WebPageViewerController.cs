﻿using MediaViewerController.Factories;
using MediaViewerController.Services;
using MediaViewerController.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.ComponentModel;

namespace MediaViewerController.Controllers
{
    public class WebPageViewerController : WebPageViewerViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private WebPageAgent_Elastic serviceAgentElastic;
        private UrlFactory urlFactory;
        private TranslationController translationController = new TranslationController();

        private clsOntologyItem referenceItem;

        private List<string> urls = new List<string>();

        private Timer errorHideTimer;

        private clsLocalConfig localConfig;
        private double lastPosSaved = 0;

        private string currentItem;
        private CultureInfo positionFormatCulture = CultureInfo.CreateSpecificCulture("en-US");
        private NumberStyles positionNumberStyle = NumberStyles.Number;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public WebPageViewerController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            errorHideTimer = new Timer();
            errorHideTimer.Interval = 3000;
            errorHideTimer.Elapsed += ErrorHideTimer_Elapsed;

            PropertyChanged += WebPageViewerController_PropertyChanged;

        }

        private void ErrorHideTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            errorHideTimer.Stop();
        }

        private void WebPageViewerController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            if (e.PropertyName == Notifications.NotifyChanges.ViewModel_IsSuccessful_Login ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_NavFirst ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_NavLast ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_NavNext ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_NavPrevious ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Label_Nav ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Url_Open ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Count ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Label_Url ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsToggled_Listen ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_Open ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsChecked_AutoOpen)
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_Position_Urls)
            {
                NavUrls();
            }
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new WebPageAgent_Elastic(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;

        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
            urlFactory = null;
            serviceAgentElastic = null;
            translationController = null;
            urls = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            IsToggled_Listen = true;

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            if (DataText_AttributeTypeId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_AttributeTypeId, localConfig.Globals.Type_AttributeType);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }
            else if (DataText_RelationTypeId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_RelationTypeId, localConfig.Globals.Type_RelationType);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }
            else if (DataText_ClassId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_ClassId, localConfig.Globals.Type_Class);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }
            else if (DataText_ObjectId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_ObjectId, localConfig.Globals.Type_Object);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void urlFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
          
        }

        private void NavUrls()
        {
            currentItem = urls[Position_Urls];
            
            Url_Open = currentItem;
            Label_Url = (referenceItem != null ? referenceItem.Name + ": " : "") + currentItem;
            IsEnabled_Open = true;
            lastPosSaved = 0;

            ConfigureNav();
        }

        private void PrepareNextUrl()
        {

            lastPosSaved = 0;
            ConfigureNav();
            if (urls != null && urls.Any())
            {
                currentItem = urls[Position_Urls];
                if (currentItem != null)
                {
                    Url_Open = currentItem;
                    Label_Url = (referenceItem != null ? referenceItem.Name + ": " : "") + currentItem;
                    IsEnabled_Open = true;
                }
            }
            
            


        }

        private void ConfigureNav()
        {
            var toggleBack = false;
            var toggleForward = false;

            if (IsPossibleBackward())
            {
                toggleBack = true;
            }

            if (IsPossibleForward())
            {
                toggleForward = true;
            }

            ToggleNavBack(toggleBack);
            ToggleNavForward(toggleForward);

            var ix = (urls == null || !urls.Any() ? 0 : Position_Urls + 1);
            var count = (urls == null ? 0 : urls.Count);

            Label_Nav = ix + "/" + count;
        }

        private bool IsPossibleBackward()
        {
            return Position_Urls > 0;
        }

        private bool IsPossibleForward()
        {
            return urls != null && urls.Any() && Position_Urls < urls.Count - 1;
        }

        private void ToggleNavBack(bool toggleTo)
        {
            IsEnabled_NavFirst = IsEnabled_NavPrevious = toggleTo;
        }

        private void ToggleNavForward(bool toggleTo)
        {
            IsEnabled_NavLast = IsEnabled_NavNext = toggleTo;
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultData)
            {


                if (serviceAgentElastic.ResultData.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return;


                }
                TestInitializationFactory();

            }


        }

        private void TestInitializationFactory()
        {
            
            if (urlFactory == null)
            {
                urlFactory = new UrlFactory(localConfig);
                urlFactory.PropertyChanged += urlFactory_PropertyChanged;
            }

            urls = urlFactory.GetUrlList(serviceAgentElastic.LeftRightUrls,
                serviceAgentElastic.RightLeftUrls);

            PrepareNextUrl();
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;

        }

       
        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavFirst)
                {


                    Position_Urls = 0;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavPrevious)
                {
                    Position_Urls--;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavNext)
                {

                    Position_Urls++;
                    

                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavLast)
                {
                    Position_Urls = urls.Count - 1;
                }
                


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
               if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.ViewModel_IsToggled_Listen && webSocketServiceAgent.ChangedProperty.Value != null)
                {
                    bool checkValue;
                    if (bool.TryParse(webSocketServiceAgent.ChangedProperty.Value.ToString(), out checkValue))
                    {
                        IsToggled_Listen = checkValue;
                    }
                }
               else if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.ViewModel_IsChecked_AutoOpen && webSocketServiceAgent.ChangedProperty.Value != null)
                {
                    bool checkValue;
                    if (bool.TryParse(webSocketServiceAgent.ChangedProperty.Value.ToString(), out checkValue))
                    {
                        IsChecked_AutoOpen = checkValue;
                    }
                }
            }


        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void LoadMedia()
        {
            Position_Urls = 0;
            Count = 0;
            urls = new List<string>();
            Url_Open = "";
            ConfigureNav();
            
            serviceAgentElastic.GetDataUrls(referenceItem.GUID);
        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ParameterList && IsToggled_Listen)
            {
                var objectItem = message.OItems.LastOrDefault();

                if (objectItem == null) return;

                if (referenceItem != null && referenceItem.GUID == objectItem.GUID) return;
                referenceItem = objectItem;
                LoadMedia();

            }
        }

       
        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
