﻿using MediaViewerController.Models;
using MediaViewerController.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Controllers
{
    public class MediaBookmarkViewModel : OntoMsg_Module.Base.ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool isToggled_Listen;
        [ViewModel(Send = true)]
        public bool IsToggled_Listen
        {
            get { return isToggled_Listen; }
            set
            {
                if (isToggled_Listen == value) return;

                isToggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private string url_DateBookmarkItems;
        [ViewModel(Send = true)]
        public string Url_DateBookmarkItems
        {
            get { return url_DateBookmarkItems; }
            set
            {
                if (url_DateBookmarkItems == value) return;

                url_DateBookmarkItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_DateBookmarkItems);

            }
        }

        private bool isloading_Bookmarks;
        [ViewModel(Send = true)]
        public bool IsLoading_Bookmarks
        {
            get { return isloading_Bookmarks; }
            set
            {
                if (isloading_Bookmarks == value) return;

                isloading_Bookmarks = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsLoading_Bookmarks);

            }
        }

        private string text_SelectedId;
        [ViewModel(Send = true)]
        public string Text_SelectedId
        {
            get { return text_SelectedId; }
            set
            {
                if (text_SelectedId == value) return;

                text_SelectedId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_SelectedId);

            }
        }

        private string datatext_ViewId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewId", ViewItemType = ViewItemType.Other)]
        public string DataText_ViewId
        {
            get { return datatext_ViewId; }
            set
            {
                if (datatext_ViewId == value) return;

                datatext_ViewId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ViewId);

            }
        }

        private string datatext_AttributeTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "attributeTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_AttributeTypeId
        {
            get { return datatext_AttributeTypeId; }
            set
            {
                if (datatext_AttributeTypeId == value) return;

                datatext_AttributeTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_AttributeTypeId);

            }
        }

        private string datatext_RelationTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "relationTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_RelationTypeId
        {
            get { return datatext_RelationTypeId; }
            set
            {
                if (datatext_RelationTypeId == value) return;

                datatext_RelationTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_RelationTypeId);

            }
        }

        private string datatext_ObjectId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "objectId", ViewItemType = ViewItemType.Other)]
        public string DataText_ObjectId
        {
            get { return datatext_ObjectId; }
            set
            {
                if (datatext_ObjectId == value) return;

                datatext_ObjectId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ObjectId);

            }
        }

        private string datatext_ClassId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "classId", ViewItemType = ViewItemType.Other)]
        public string DataText_ClassId
        {
            get { return datatext_ClassId; }
            set
            {
                if (datatext_ClassId == value) return;

                datatext_ClassId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ClassId);

            }
        }
    }
}
