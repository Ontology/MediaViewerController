﻿using MediaViewerController.Models;
using MediaViewerController.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Controllers
{
    public class ImageViewerViewModel : OntoMsg_Module.Base.ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private string url_Image;
        [ViewModel(Send = true)]
        public string Url_Image
        {
            get { return url_Image; }
            set
            {
                if (url_Image == value) return;

                url_Image = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_Image);

            }
        }

        private string url_Thumb;
        [ViewModel(Send = true)]
        public string Url_Thumb
        {
            get { return url_Thumb; }
            set
            {
                if (url_Thumb == value) return;

                url_Thumb = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_Thumb);

            }
        }

        private int position_GalleryImage;
        [ViewModel(Send = true)]
        public int Position_GalleryImage
        {
            get { return position_GalleryImage; }
            set
            {
                if (position_GalleryImage == value) return;

                position_GalleryImage = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Position_GalleryImage);

            }
        }

        private int count_GalleryImage;
        [ViewModel(Send = true)]
        public int Count_GalleryImage
        {
            get { return count_GalleryImage; }
            set
            {
                if (count_GalleryImage == value) return;

                count_GalleryImage = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Count_GalleryImage);

            }
        }
        private bool isenabled_NavFirst;
        [ViewModel(Send = true)]
        public bool IsEnabled_NavFirst
        {
            get { return isenabled_NavFirst; }
            set
            {
                if (isenabled_NavFirst == value) return;

                isenabled_NavFirst = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavFirst);

            }
        }

        private bool isenabled_NavPrevious;
        [ViewModel(Send = true)]
        public bool IsEnabled_NavPrevious
        {
            get { return isenabled_NavPrevious; }
            set
            {
                if (isenabled_NavPrevious == value) return;

                isenabled_NavPrevious = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavPrevious);

            }
        }

        private bool isenabled_NavNext;
        [ViewModel(Send = true)]
        public bool IsEnabled_NavNext
        {
            get { return isenabled_NavNext; }
            set
            {
                if (isenabled_NavNext == value) return;

                isenabled_NavNext = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavNext);

            }
        }

        private bool isenabled_NavLast;
        [ViewModel(Send = true)]
        public bool IsEnabled_NavLast
        {
            get { return isenabled_NavLast; }
            set
            {
                if (isenabled_NavLast == value) return;

                isenabled_NavLast = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavLast);

            }
        }

        private MultimediaViewItem imageviewitem_ThumbNailItem;
        [ViewModel(Send = true)]
        public MultimediaViewItem ImageViewItem_ThumbNailItem
        {
            get { return imageviewitem_ThumbNailItem; }
            set
            {
                if (imageviewitem_ThumbNailItem == value) return;

                imageviewitem_ThumbNailItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ImageViewItem_ThumbNailItem);

            }
        }

        private int height_Canvas;
        [ViewModel(Send = true)]
        public int Height_Canvas
        {
            get { return height_Canvas; }
            set
            {
                if (height_Canvas == value) return;

                height_Canvas = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Height_Canvas);

            }
        }

        private int width_Canvas;
        [ViewModel(Send = true)]
        public int Width_Canvas
        {
            get { return width_Canvas; }
            set
            {
                if (width_Canvas == value) return;

                width_Canvas = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Width_Canvas);

            }
        }

        private string label_Nav;
        [ViewModel(Send = true)]
        public string Label_Nav
        {
            get { return label_Nav; }
            set
            {
                if (label_Nav == value) return;

                label_Nav = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Nav);

            }
        }

        private int count_ToSave;
        [ViewModel(Send = true)]
        public int Count_ToSave
        {
            get { return count_ToSave; }
            set
            {
                if (count_ToSave == value) return;

                count_ToSave = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Count_ToSave);

            }
        }

        private string datatext_ViewId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewId", ViewItemType = ViewItemType.Other)]
        public string DataText_ViewId
        {
            get { return datatext_ViewId; }
            set
            {
                if (datatext_ViewId == value) return;

                datatext_ViewId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ViewId);

            }
        }

        private string datatext_AttributeTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "attributeTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_AttributeTypeId
        {
            get { return datatext_AttributeTypeId; }
            set
            {
                if (datatext_AttributeTypeId == value) return;

                datatext_AttributeTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_AttributeTypeId);

            }
        }

        private string datatext_RelationTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "relationTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_RelationTypeId
        {
            get { return datatext_RelationTypeId; }
            set
            {
                if (datatext_RelationTypeId == value) return;

                datatext_RelationTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_RelationTypeId);

            }
        }

        private string datatext_ObjectId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "objectId", ViewItemType = ViewItemType.Other)]
        public string DataText_ObjectId
        {
            get { return datatext_ObjectId; }
            set
            {
                if (datatext_ObjectId == value) return;

                datatext_ObjectId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ObjectId);

            }
        }

        private string datatext_ClassId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "classId", ViewItemType = ViewItemType.Other)]
        public string DataText_ClassId
        {
            get { return datatext_ClassId; }
            set
            {
                if (datatext_ClassId == value) return;

                datatext_ClassId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ClassId);

            }
        }
    }
}
