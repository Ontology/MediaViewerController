﻿using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntoMsg_Module.WebSocketServices;
using OntoMsg_Module.Services;
using MediaViewerController.Services;
using MediaViewerController.Translations;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System.Runtime.InteropServices;
using System.Reflection;
using OntologyAppDBConnector;
using OntoMsg_Module.Notifications;
using MediaViewerController.Factories;
using MediaViewerController.Models;
using OntoMsg_Module.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.StateMachines;
using System.ComponentModel;

namespace MediaViewerController.Controllers
{
    public class ImageViewerController : ImageViewerViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private ImageServiceAgent_Elastic serviceAgentElastic;
        private ViewMediaFactory viewMediaFactory;
        private bool isCurrentImagePrepared;

        private clsOntologyItem referenceItem;

        private bool imagesLoadedFromDb;

        private clsLocalConfig localConfig;

        private List<MultimediaViewItem> imageViewItems = new List<MultimediaViewItem>();

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public ImageViewerController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += ImageViewerController_PropertyChanged;
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new ImageServiceAgent_Elastic(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;
            
        }

        private void StateMachine_closedSocket()
        {
            viewMediaFactory?.StopFactory();
            webSocketServiceAgent.RemoveAllResources();
            viewMediaFactory = null;
            serviceAgentElastic = null;
            translationController = null;
            imageViewItems = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            if (DataText_AttributeTypeId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_AttributeTypeId, localConfig.Globals.Type_AttributeType);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }
            else if (DataText_RelationTypeId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_RelationTypeId, localConfig.Globals.Type_RelationType);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }
            else if (DataText_ClassId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_ClassId, localConfig.Globals.Type_Class);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }
            else if (DataText_ObjectId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_ObjectId, localConfig.Globals.Type_Object);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }

            var interModMessage = new InterServiceMessage
            {
                ChannelId = Channels.ReceiverOfDedicatedSender
            };

            webSocketServiceAgent.SendInterModMessage(interModMessage);
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void ViewImageFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.ViewMediaFactory_NextMultimediaItem)
            {
                imageViewItems.Add(viewMediaFactory.NextMultimediaItem);
                Count_GalleryImage = imageViewItems.Count;
                PrepareNextImageItem();
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewMediaFactory_MultimediaItemCountToSave)
            {
                Count_ToSave = viewMediaFactory.MultiMediaCountToSave;
            }
        }

        private void NavMultiMediaItems()
        {
            var multiMediaItem = imageViewItems[Position_GalleryImage];
            Url_Image = multiMediaItem.MultimediaUrl;
            ConfigureNav();
        }

        private void PrepareNextImageItem()
        {
            
            var thumbNailItem = imageViewItems.Last();
            ImageViewItem_ThumbNailItem = thumbNailItem;

            ConfigureNav();

            var imageItem = imageViewItems[Position_GalleryImage];
            Url_Thumb = imageItem != null ? imageItem.ThumbImageUrl : "";
            Url_Image = imageItem != null ? imageItem.MultimediaUrl : "";
            

            isCurrentImagePrepared = true;
            
        }

        private void ConfigureNav()
        {
            var toggleBack = false;
            var toggleForward = false;
            
            if (Position_GalleryImage > 0)
            {
                toggleBack = true;
            }

            if (imageViewItems != null && imageViewItems.Any() && Position_GalleryImage < imageViewItems.Count-1)
            {
                toggleForward = true;
            }

            ToggleNavBack(toggleBack);
            ToggleNavForward(toggleForward);

            var ix = (imageViewItems == null || !imageViewItems.Any() ? 0 : Position_GalleryImage + 1);
            var count = (imageViewItems == null ? 0 : imageViewItems.Count);

            Label_Nav = ix + "/" + count;
        }

        private void ToggleNavBack(bool toggleTo)
        {
            IsEnabled_NavFirst = IsEnabled_NavPrevious = toggleTo;
        }

        private void ToggleNavForward(bool toggleTo)
        {
            IsEnabled_NavLast = IsEnabled_NavNext = toggleTo;
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultData)
            {


                if (serviceAgentElastic.ResultData.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return;


                }
                imagesLoadedFromDb = true;
                TestInitializationFactory();
               
            }

            
        }

        private void ImageViewerController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            if (e.PropertyName == Notifications.NotifyChanges.ViewModel_IsSuccessful_Login ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_NavFirst ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_NavLast ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_NavNext ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_NavPrevious ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Label_Nav ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Url_Image ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Count_ToSave ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_ImageViewItem_ThumbNailItem)
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_Width_Canvas ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Height_Canvas)
            {
                TestInitializationFactory();
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_Position_GalleryImage)
            {
                NavMultiMediaItems();
            }

        }

        private void TestInitializationFactory()
        {
            //if (imagesLoadedFromDb && Height_Canvas > 0 && Width_Canvas > 0)
            //{
            //    if (viewMediaFactory == null)
            //    {
            //        viewMediaFactory = new ViewMediaFactory(localConfig, Width_Canvas, Height_Canvas, webSocketServiceAgent.oItemUser, webSocketServiceAgent.oItemGroup);
            //        viewMediaFactory.PropertyChanged += ViewImageFactory_PropertyChanged;
            //    }
                
            //    var result = viewMediaFactory.SaveMultimediaFiles(serviceAgentElastic.RefToImages,
            //        serviceAgentElastic.ImageAttributes,
            //        serviceAgentElastic.ImageFiles,
            //        webSocketServiceAgent.FileSystemServiceAgent,
            //        webSocketServiceAgent.DataText_SessionId);

            //}

            if (imagesLoadedFromDb)
            {
                if (viewMediaFactory == null)
                {
                    viewMediaFactory = new ViewMediaFactory(localConfig, Width_Canvas, Height_Canvas, webSocketServiceAgent.oItemUser, webSocketServiceAgent.oItemGroup);
                    viewMediaFactory.PropertyChanged += ViewImageFactory_PropertyChanged;
                }

                var result = viewMediaFactory.SaveMultimediaFiles(serviceAgentElastic.RefToImages,
                    serviceAgentElastic.ImageAttributes,
                    serviceAgentElastic.ImageFiles,
                    webSocketServiceAgent.GetFileSystemObject(),
                    webSocketServiceAgent.DataText_SessionId);

            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;
            
        }

       
        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavFirst)
                {
                    Position_GalleryImage = 0;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavPrevious)
                {
                    Position_GalleryImage--;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavNext)
                {
                    Position_GalleryImage++;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavLast)
                {
                    Position_GalleryImage = imageViewItems.Count - 1;
                }


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.ViewModel_Width_Canvas)
                {
                    var value = webSocketServiceAgent.ChangedProperty.Value;

                    if (value == null) return;

                    int checkValue;
                    if (int.TryParse(value.ToString(), out checkValue))
                    {
                        Width_Canvas = checkValue;
                    }
                }
                else if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.ViewModel_Height_Canvas)
                {
                    var value = webSocketServiceAgent.ChangedProperty.Value;

                    if (value == null) return;

                    int checkValue;
                    if (int.TryParse(value.ToString(), out checkValue))
                    {
                        Height_Canvas = checkValue;
                    }
                }
            }
            else if (e.PropertyName == NotifyChanges.Websocket_DedicatedSenderArgument)
            {
                if (webSocketServiceAgent.DedicatedSenderArgument != null)
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.ReceiverOfDedicatedSender,
                        ReceiverId = webSocketServiceAgent.DedicatedSenderArgument.Value
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }
            }


        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void LoadMedia()
        {
            if (viewMediaFactory != null)
            {
                viewMediaFactory.StopFactory();
            }
            Position_GalleryImage = 0;
            Count_GalleryImage = 0;
            imageViewItems = new List<MultimediaViewItem>();
            Url_Image = "";
            Count_ToSave = 0;
            isCurrentImagePrepared = false;
            ConfigureNav();
            imagesLoadedFromDb = false;
            serviceAgentElastic.GetReferencedImages(referenceItem);
        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ParameterList)
            {
                var objectItem = message.OItems.LastOrDefault();

                if (objectItem == null) return;

                if (referenceItem != null && referenceItem.GUID == objectItem.GUID) return;
                referenceItem = objectItem;
                LoadMedia();
            }
            else if (message.ChannelId == Notifications.NotifyChanges.Channel_MediaItemList)
            {
                var genericParameters = message.GenericParameterItems;

                if (genericParameters == null) return;

                var firstParam = genericParameters.FirstOrDefault();

                if (firstParam == null) return;

                var mediaListItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(firstParam.ToString()).Select(dynItm => new MediaListItem
                {
                    IdAttributeCreated = dynItm.IdAttributeCreated,
                    BookmarkCount= dynItm.BookmarkCount,
                    Created = dynItm.Created,
                    IdFile = dynItm.IdFile,
                    NameFile = dynItm.NameFile,
                    IdItem = dynItm.IdItem,
                    IdRef = dynItm.IdRef,
                    IdRow = dynItm.IdRow,
                    NameItem = dynItm.NameItem,
                    NameRef = dynItm.NameRef,
                    OrderId = dynItm.OrderId,
                    Random = dynItm.Random
                }).ToList();

                imagesLoadedFromDb = true;
                if (imagesLoadedFromDb)
                {
                    if (viewMediaFactory == null)
                    {
                        viewMediaFactory = new ViewMediaFactory(localConfig, Width_Canvas, Height_Canvas, webSocketServiceAgent.oItemUser, webSocketServiceAgent.oItemGroup);
                        viewMediaFactory.PropertyChanged += ViewImageFactory_PropertyChanged;
                    }

                    var result = viewMediaFactory.SaveMultimediaFiles(mediaListItems, webSocketServiceAgent.GetFileSystemObject(), webSocketServiceAgent.DataText_SessionId);

                }
            }
        }

     

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }


    }
}
