﻿using MediaViewerController.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Controllers
{
    public class WebPageViewerViewModel : OntoMsg_Module.Base.ViewModelBase
    {
        private bool isToggled_Listen;
        [ViewModel(Send = true)]
        public bool IsToggled_Listen
        {
            get { return isToggled_Listen; }
            set
            {
                if (isToggled_Listen == value) return;

                isToggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private bool issuccessful_Login;
        [ViewModel(Send = true)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool isenabled_NavFirst;
        [ViewModel(Send = true)]
        public bool IsEnabled_NavFirst
        {
            get { return isenabled_NavFirst; }
            set
            {
                if (isenabled_NavFirst == value) return;

                isenabled_NavFirst = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavFirst);

            }
        }

        private bool isenabled_NavPrevious;
        [ViewModel(Send = true)]
        public bool IsEnabled_NavPrevious
        {
            get { return isenabled_NavPrevious; }
            set
            {
                if (isenabled_NavPrevious == value) return;

                isenabled_NavPrevious = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavPrevious);

            }
        }


        private bool isenabled_Open;
        [ViewModel(Send = true)]
        public bool IsEnabled_Open
        {
            get { return isenabled_Open; }
            set
            {
                if (isenabled_Open == value) return;

                isenabled_Open = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Open);

            }
        }
        private int count;
        [ViewModel(Send = true)]
        public int Count
        {
            get { return count; }
            set
            {
                if (count == value) return;

                count = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Count);

            }
        }

        private bool isenabled_NavNext;
        [ViewModel(Send = true)]
        public bool IsEnabled_NavNext
        {
            get { return isenabled_NavNext; }
            set
            {
                if (isenabled_NavNext == value) return;

                isenabled_NavNext = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavNext);

            }
        }

        private bool isenabled_NavLast;
        [ViewModel(Send = true)]
        public bool IsEnabled_NavLast
        {
            get { return isenabled_NavLast; }
            set
            {
                if (isenabled_NavLast == value) return;

                isenabled_NavLast = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_NavLast);

            }
        }

        private string label_Nav;
        [ViewModel(Send = true)]
        public string Label_Nav
        {
            get { return label_Nav; }
            set
            {
                if (label_Nav == value) return;

                label_Nav = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Nav);

            }
        }

        private int position_Urls;
        [ViewModel(Send = true)]
        public int Position_Urls
        {
            get { return position_Urls; }
            set
            {
                if (position_Urls == value) return;

                position_Urls = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Position_Urls);

            }
        }

        private string url_Open;
        [ViewModel(Send = true)]
        public string Url_Open
        {
            get { return url_Open; }
            set
            {
                if (url_Open == value) return;

                url_Open = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_Open);

            }
        }

        private string label_Url;
        [ViewModel(Send = true)]
        public string Label_Url
        {
            get { return label_Url; }
            set
            {

                label_Url = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Url);

            }
        }

        private bool ischecked_AutoOpen = false;
        [ViewModel(Send = true)]
        public bool IsChecked_AutoOpen
        {
            get { return ischecked_AutoOpen; }
            set
            {
                if (ischecked_AutoOpen == value) return;

                ischecked_AutoOpen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsChecked_AutoOpen);

            }
        }

        private string datatext_ViewId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewId", ViewItemType = ViewItemType.Other)]
        public string DataText_ViewId
        {
            get { return datatext_ViewId; }
            set
            {
                if (datatext_ViewId == value) return;

                datatext_ViewId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ViewId);

            }
        }

        private string datatext_AttributeTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "attributeTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_AttributeTypeId
        {
            get { return datatext_AttributeTypeId; }
            set
            {
                if (datatext_AttributeTypeId == value) return;

                datatext_AttributeTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_AttributeTypeId);

            }
        }

        private string datatext_RelationTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "relationTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_RelationTypeId
        {
            get { return datatext_RelationTypeId; }
            set
            {
                if (datatext_RelationTypeId == value) return;

                datatext_RelationTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_RelationTypeId);

            }
        }

        private string datatext_ObjectId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "objectId", ViewItemType = ViewItemType.Other)]
        public string DataText_ObjectId
        {
            get { return datatext_ObjectId; }
            set
            {
                if (datatext_ObjectId == value) return;

                datatext_ObjectId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ObjectId);

            }
        }

        private string datatext_ClassId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "classId", ViewItemType = ViewItemType.Other)]
        public string DataText_ClassId
        {
            get { return datatext_ClassId; }
            set
            {
                if (datatext_ClassId == value) return;

                datatext_ClassId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ClassId);

            }
        }
    }
}
