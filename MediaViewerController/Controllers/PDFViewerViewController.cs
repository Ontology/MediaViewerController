﻿using MediaViewerController.Factories;
using MediaViewerController.Models;
using MediaViewerController.Services;
using MediaViewerController.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.Services;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace MediaViewerController.Controllers
{
    public class PDFViewerViewController : PDFViewerViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private PDFServiceAgent_Elastic serviceAgentElastic;
        private ViewMediaFactory viewMediaFactory;
        private bool isCurrentPDFPrepared;

        private clsOntologyItem referenceItem;

        private bool PDFsLoadedFromDb;

        private List<MultimediaViewItem> multimediaViewItems = new List<MultimediaViewItem>();

        private clsLocalConfig localConfig;

        private string dedicatedSender = null;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public PDFViewerViewController()
        {
            
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            
            PropertyChanged += PDFPlayerViewController_PropertyChanged;
        }

        private void PDFPlayerViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            if (e.PropertyName == Notifications.NotifyChanges.ViewModel_Position_PDFItems)
            {
                NavMultiMediaItems();
            }
            else
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new PDFServiceAgent_Elastic(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;
            webSocketServiceAgent.SetEnable(false);
            webSocketServiceAgent.SetVisibility(true);

        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();

            viewMediaFactory = null;
            serviceAgentElastic = null;
            translationController = null;
            multimediaViewItems = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
           
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Notifications.NotifyChanges.Channel_PDFViewer,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            if (DataText_AttributeTypeId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_AttributeTypeId, localConfig.Globals.Type_AttributeType);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }
            else if (DataText_RelationTypeId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_RelationTypeId, localConfig.Globals.Type_RelationType);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }
            else if (DataText_ClassId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_ClassId, localConfig.Globals.Type_Class);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }
            else if (DataText_ObjectId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_ObjectId, localConfig.Globals.Type_Object);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }
           
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void ViewMediaItemFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.ViewMediaFactory_NextMultimediaItem)
            {
                multimediaViewItems.Add(viewMediaFactory.NextMultimediaItem);
                Count_PlaylistItems = multimediaViewItems.Count;
                PrepareNextMultimediaItem();
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewMediaFactory_MultimediaItemCountToSave)
            {
                Count_ToSave = viewMediaFactory.MultiMediaCountToSave;
            }
        }

        private void NavMultiMediaItems()
        {
            var multiMediaItem = multimediaViewItems[Position_PDFItems];
            if (multiMediaItem.NameFile.ToLower().EndsWith("." + Properties.Settings.Default.ExtensionMP3))
            {
                DataText_MediaMime = Properties.Settings.Default.MediaType_MP3;
            }
            else
            {
                DataText_MediaMime = Properties.Settings.Default.MediaType_WAV;
            }
            
            Label_MediaItem = multiMediaItem.Name;
           
            ConfigureNav();
        }

        private void PrepareNextMultimediaItem()
        {

            var thumbNailItem = multimediaViewItems.Last();

            ConfigureNav();
            if (isCurrentPDFPrepared) return;

            var imageItem = multimediaViewItems[Position_PDFItems];
            IsEnabled_OpenPdf = imageItem != null;
            
            if (IsChecked_AutoOpen)
            {
                Url_PDF = imageItem != null ? imageItem.MultimediaUrl : "";
            }
            Label_MediaItem = imageItem != null ? imageItem.Name : "";

            isCurrentPDFPrepared = true;

        }

        private void ConfigureNav()
        {
            var toggleBack = false;
            var toggleForward = false;

            if (Position_PDFItems > 0)
            {
                toggleBack = true;
            }

            if (multimediaViewItems != null && multimediaViewItems.Any() && Position_PDFItems < multimediaViewItems.Count - 1)
            {
                toggleForward = true;
            }

            ToggleNavBack(toggleBack);
            ToggleNavForward(toggleForward);

            var ix = (multimediaViewItems == null || !multimediaViewItems.Any() ? 0 : Position_PDFItems + 1);
            var count = (multimediaViewItems == null ? 0 : multimediaViewItems.Count);

            Label_Nav = ix + "/" + count;
        }

        private void ToggleNavBack(bool toggleTo)
        {
            IsEnabled_NavFirst = IsEnabled_NavPrevious = toggleTo;
        }

        private void ToggleNavForward(bool toggleTo)
        {
            IsEnabled_NavLast = IsEnabled_NavNext = toggleTo;
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultData)
            {


                if (serviceAgentElastic.ResultData.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return;


                }
                PDFsLoadedFromDb = true;
                TestInitializationFactory();

            }


        }

        private void TestInitializationFactory()
        {
            if (PDFsLoadedFromDb)
            {
                if (viewMediaFactory == null)
                {
                    viewMediaFactory = new ViewMediaFactory(localConfig, MultimediaItemType.PDF, webSocketServiceAgent.oItemUser, webSocketServiceAgent.oItemGroup);
                    viewMediaFactory.PropertyChanged += ViewMediaItemFactory_PropertyChanged;
                }

                var result = viewMediaFactory.SaveMultimediaFiles(serviceAgentElastic.RefToMediafiles,
                    serviceAgentElastic.MediaAttributes,
                    serviceAgentElastic.MediaFiles,
                    webSocketServiceAgent.GetFileSystemObject(),
                    webSocketServiceAgent.DataText_SessionId);

            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;
            dedicatedSender = webSocketServiceAgent.Context.QueryString["Sender"];
            

        }

       
        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {
                Initialize();

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavFirst)
                {
                    Position_PDFItems = 0;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavPrevious)
                {
                    Position_PDFItems--;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavNext)
                {
                    Position_PDFItems++;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavLast)
                {
                    Position_PDFItems = multimediaViewItems.Count - 1;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_OpenPdf)
                {
                    var imageItem = multimediaViewItems[Position_PDFItems];
                    Url_PDF = imageItem != null ? imageItem.MultimediaUrl : "";

                }


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.ViewModel_IsChecked_AutoOpen)
                {
                    var value = webSocketServiceAgent.ChangedProperty.Value;
                    if (value == null) return;
                    
                    IsChecked_AutoOpen = (bool)value;
                }
            }
            else if (e.PropertyName == NotifyChanges.Websocket_DedicatedSenderArgument)
            {
                if (webSocketServiceAgent.DedicatedSenderArgument != null)
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.ReceiverOfDedicatedSender,
                        ReceiverId = webSocketServiceAgent.DedicatedSenderArgument.Value
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }
            }

        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }
        
        private void LoadMedia()
        {
            if (viewMediaFactory != null)
            {
                viewMediaFactory.StopFactory();
            }
            Position_PDFItems = 0;
            Count_PlaylistItems = 0;
            multimediaViewItems = new List<MultimediaViewItem>();
            Url_PDF = "";
            Count_ToSave = 0;
            isCurrentPDFPrepared = false;
            ConfigureNav();
            PDFsLoadedFromDb = false;
            serviceAgentElastic.GetReferencedMediaItems(referenceItem);
        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (!string.IsNullOrEmpty(dedicatedSender) && message.SenderId != dedicatedSender) return;
            if (message.ChannelId == Channels.ParameterList ||
                message.ChannelId == Notifications.NotifyChanges.Channel_PDFViewer)
            {
                var objectItem = message.OItems.LastOrDefault();

                if (objectItem == null) return;

                if (referenceItem != null && referenceItem.GUID == objectItem.GUID) return;
                referenceItem = objectItem;
                LoadMedia();
            }
            else if (message.ChannelId == Notifications.NotifyChanges.Channel_MediaItemList)
            {
                var genericParameters = message.GenericParameterItems;

                if (genericParameters == null) return;

                var firstParam = genericParameters.FirstOrDefault();

                if (firstParam == null) return;

                var mediaListItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(firstParam.ToString()).Select(dynItm => new MediaListItem
                {
                    IdAttributeCreated = dynItm.IdAttributeCreated,
                    BookmarkCount = dynItm.BookmarkCount,
                    Created = dynItm.Created,
                    IdFile = dynItm.IdFile,
                    NameFile = dynItm.NameFile,
                    IdItem = dynItm.IdItem,
                    IdRef = dynItm.IdRef,
                    IdRow = dynItm.IdRow,
                    NameItem = dynItm.NameItem,
                    NameRef = dynItm.NameRef,
                    OrderId = dynItm.OrderId,
                    Random = dynItm.Random
                }).ToList();

                PDFsLoadedFromDb = true;
                if (PDFsLoadedFromDb)
                {
                    if (viewMediaFactory == null)
                    {
                        viewMediaFactory = new ViewMediaFactory(localConfig, MultimediaItemType.PDF, webSocketServiceAgent.oItemUser, webSocketServiceAgent.oItemGroup);
                        viewMediaFactory.PropertyChanged += ViewMediaItemFactory_PropertyChanged;
                    }

                    var result = viewMediaFactory.SaveMultimediaFiles(mediaListItems, webSocketServiceAgent.GetFileSystemObject(), webSocketServiceAgent.DataText_SessionId);

                }
            }

        }

       

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
