﻿using MediaViewerController.Factories;
using MediaViewerController.Models;
using MediaViewerController.Services;
using MediaViewerController.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.ComponentModel;

namespace MediaViewerController.Controllers
{
    public class MediaBookmarkViewController : MediaBookmarkViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private BookmarkServiceAgent_Elastic serviceAgentElastic;
        private BookmarkFactory bookmarkFactory;

        private clsOntologyItem referenceItem;

        private List<BookmarkViewItem> bookmarkViewItems = new List<BookmarkViewItem>();

        private Timer errorHideTimer;

        private clsLocalConfig localConfig;

        private SessionFile sessionFile;

        private MultimediaViewItem currentItem;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public MediaBookmarkViewController()
        {

            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            errorHideTimer = new Timer();
            errorHideTimer.Interval = 3000;
            errorHideTimer.Elapsed += ErrorHideTimer_Elapsed;

            PropertyChanged += AudioPlayerViewController_PropertyChanged;
        }

        private void ErrorHideTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            errorHideTimer.Stop();
        }

        private void AudioPlayerViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            if (e.PropertyName == Notifications.NotifyChanges.ViewModel_IsSuccessful_Login ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Url_DateBookmarkItems ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsLoading_Bookmarks ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsToggled_Listen)
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new BookmarkServiceAgent_Elastic(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;

        }

        private void StateMachine_closedSocket()
        {
            bookmarkFactory.StopFactory();
            webSocketServiceAgent.RemoveAllResources();
            bookmarkFactory = null;
            serviceAgentElastic = null;
            translationController = null;
            bookmarkViewItems = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

        }

        private void StateMachine_loginSucceded()
        {
            IsToggled_Listen = true;

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });



            if (DataText_AttributeTypeId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_AttributeTypeId, localConfig.Globals.Type_AttributeType);

                if (referenceItem != null)
                {
                    LoadBookmarks();
                }
            }
            else if (DataText_RelationTypeId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_RelationTypeId, localConfig.Globals.Type_RelationType);

                if (referenceItem != null)
                {
                    LoadBookmarks();
                }
            }
            else if (DataText_ClassId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_ClassId, localConfig.Globals.Type_Class);

                if (referenceItem != null)
                {
                    LoadBookmarks();
                }
            }
            else if (DataText_ObjectId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_ObjectId, localConfig.Globals.Type_Object);

                if (referenceItem != null)
                {
                    LoadBookmarks();
                }
            }
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void ViewMediaItemFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultDateItems)
            {
                if (bookmarkFactory.ResultDateItems.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    Url_DateBookmarkItems = sessionFile.FileUri.AbsoluteUri;
                }
                IsLoading_Bookmarks = false;
            }
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultData)
            {


                if (serviceAgentElastic.ResultData.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return;


                }

                TestInitializationFactory();

            }


        }

        private void TestInitializationFactory()
        {
            if (bookmarkFactory == null)
            {
                

                bookmarkFactory = new BookmarkFactory(localConfig, webSocketServiceAgent.oItemUser, webSocketServiceAgent.oItemGroup);
                bookmarkFactory.PropertyChanged += ViewMediaItemFactory_PropertyChanged;
            }
            var fileName = Guid.NewGuid().ToString() + ".json";
            sessionFile = webSocketServiceAgent.RequestWriteStream(fileName);
            
            bookmarkFactory.SaveDateItems(serviceAgentElastic.BookmarksOfReferences,
                serviceAgentElastic.BookmarksOfMediaItems,
                serviceAgentElastic.SecondsOfBookmarks,
                serviceAgentElastic.DateTimeStampsOfBookmarks,
                serviceAgentElastic.TypesOfBookmarks,
                serviceAgentElastic.UsersOfBookmarks,
                sessionFile);


        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;

        }

       

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_Refresh)
                {
                    serviceAgentElastic.GetBookmarkItems(referenceItem);
                }


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.ViewModel_Text_SelectedId && webSocketServiceAgent.ChangedProperty.Value != null)
                {
                    var selectedMediaItem = bookmarkFactory.BookmarkItems.FirstOrDefault(item => item.IdItem == webSocketServiceAgent.ChangedProperty.Value.ToString());

                    if (selectedMediaItem == null || !selectedMediaItem.IsBookmark) return;

                    var selectedBookMark = new InterServiceMessage
                    {
                        ChannelId = Notifications.NotifyChanges.Channel_SelectedBookmark,
                        SenderId = webSocketServiceAgent.EndpointId,
                        OItems = new List<clsOntologyItem>
                        {
                            new clsOntologyItem
                            {
                                GUID = selectedMediaItem.IdParent,
                                Val_Real = selectedMediaItem.Seconds
                            }
                        }
                    };

                    webSocketServiceAgent.SendInterModMessage(selectedBookMark);
                }
            }


        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void LoadBookmarks()
        {
            if (bookmarkFactory != null)
            {
                bookmarkFactory.StopFactory();
            }

            

            IsLoading_Bookmarks = true;
            serviceAgentElastic.GetBookmarkItems(referenceItem);
        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ParameterList && IsToggled_Listen)
            {
                var objectItem = message.OItems.LastOrDefault();

                if (objectItem == null) return;

                if (referenceItem != null && referenceItem.GUID == objectItem.GUID) return;
                referenceItem = objectItem;
                LoadBookmarks();
            }
            
        }


        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
