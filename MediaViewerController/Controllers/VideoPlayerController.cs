﻿using MediaViewerController.Factories;
using MediaViewerController.Models;
using MediaViewerController.Services;
using MediaViewerController.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.ComponentModel;

namespace MediaViewerController.Controllers
{
    public class VideoPlayerController : VideoPlayerViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private MediaServiceAgent_Elastic serviceAgentElastic;
        private ViewMediaFactory viewMediaFactory;
        private bool isCurrentVideoPrepared;

        private clsOntologyItem referenceItem;

        private bool VideosLoadedFromDb;

        private List<MultimediaViewItem> multimediaViewItems = new List<MultimediaViewItem>();

        private Timer errorHideTimer;

        private clsLocalConfig localConfig;
        private double lastPosSaved = 0;

        private MultimediaViewItem currentItem;
        private CultureInfo positionFormatCulture = CultureInfo.CreateSpecificCulture("en-US");
        private NumberStyles positionNumberStyle = NumberStyles.Number;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public VideoPlayerController()
        {

            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            errorHideTimer = new Timer();
            errorHideTimer.Interval = 3000;
            errorHideTimer.Elapsed += ErrorHideTimer_Elapsed;

            PropertyChanged += VideoPlayerViewController_PropertyChanged;
        }

        private void ErrorHideTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            errorHideTimer.Stop();
            Error_BookMark = null;
        }

        private void VideoPlayerViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            if (e.PropertyName == Notifications.NotifyChanges.ViewModel_IsSuccessful_Login ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_NavFirst ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_NavLast ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_NavNext ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_NavPrevious ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Label_Nav ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Url_Video ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Count_ToSave ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Label_MediaItem ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_DataText_MediaMime ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsChecked_AutoPlay ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_DataText_LastSavedBookmark ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Error_BookMark ||
                e.PropertyName == nameof(ItemToAdd))
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_Position_VideoItems)
            {
                NavMultiMediaItems();
            }
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new MediaServiceAgent_Elastic(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;

        }

        private void StateMachine_closedSocket()
        {
            if (viewMediaFactory != null)
            {
                viewMediaFactory.StopFactory();
            }

            webSocketServiceAgent.RemoveAllResources();
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

        }

        private void StateMachine_loginSucceded()
        {
            
            
            IsToggled_Listen = true;
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            if (DataText_AttributeTypeId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_AttributeTypeId, localConfig.Globals.Type_AttributeType);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }
            else if (DataText_RelationTypeId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_RelationTypeId, localConfig.Globals.Type_RelationType);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }
            else if (DataText_ClassId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_ClassId, localConfig.Globals.Type_Class);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }
            else if (DataText_ObjectId != null)
            {
                referenceItem = serviceAgentElastic.GetOItem(DataText_ObjectId, localConfig.Globals.Type_Object);

                if (referenceItem != null)
                {
                    LoadMedia();
                }
            }

            if (webSocketServiceAgent.DedicatedSenderArgument != null)
            {
                var viewReadyMessage = new InterServiceMessage
                {
                    ChannelId = Channels.ViewReady,
                    ReceiverId = webSocketServiceAgent.DedicatedSenderArgument.Value,
                    ViewId = webSocketServiceAgent.IdEntryView
                };

                webSocketServiceAgent.SendInterModMessage(viewReadyMessage);
            }


         
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ControllerStateMachine.LoginSuccessful))
            {
                IsChecked_AutoPlay = false;
                IsToggled_ToggleBookmarks = false;
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
                
            }
        }

        private void ViewMediaItemFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.ViewMediaFactory_NextMultimediaItem)
            {
                ItemToAdd = new KendoMediaItem
                {
                    idMediaItem = viewMediaFactory.NextMultimediaItem.Id,
                    title = viewMediaFactory.NextMultimediaItem.Name,
                    poster = "https://www.omodules.de/OntologyModules/Images/Ontology-Module2.png",
                    source = viewMediaFactory.NextMultimediaItem.MultimediaUrl,
                    CreateDate = viewMediaFactory.NextMultimediaItem.OMultimediaItem.Val_Date
                };
                multimediaViewItems.Add(viewMediaFactory.NextMultimediaItem);
                Count_PlaylistItems = multimediaViewItems.Count;
                PrepareNextMultimediaItem();
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewMediaFactory_MultimediaItemCountToSave)
            {
                Count_ToSave = viewMediaFactory.MultiMediaCountToSave;
            }
        }

        private void NavMultiMediaItems()
        {
            currentItem = multimediaViewItems[Position_VideoItems];
            if (currentItem.NameFile.ToLower().EndsWith("." + Properties.Settings.Default.ExtensionMP3))
            {
                DataText_MediaMime = Properties.Settings.Default.MediaType_MP3;
            }
            else
            {
                DataText_MediaMime = Properties.Settings.Default.MediaType_WAV;
            }

            Url_Video = currentItem.MultimediaUrl;
            Label_MediaItem = currentItem.Name;
            lastPosSaved = 0;

            ConfigureNav();
        }

        private void PrepareNextMultimediaItem()
        {
          
            lastPosSaved = 0;
            ConfigureNav();
            if (isCurrentVideoPrepared) return;

            currentItem = multimediaViewItems[Position_VideoItems];
            if (currentItem.NameFile.ToLower().EndsWith("." + Properties.Settings.Default.ExtensionVMW))
            {
                DataText_MediaMime = Properties.Settings.Default.MediaType_WMV;
            }
            else if (currentItem.NameFile.ToLower().EndsWith("." + Properties.Settings.Default.ExtensionAVI))
            {
                DataText_MediaMime = Properties.Settings.Default.MediaType_AVI;
            }
            else if (currentItem.NameFile.ToLower().EndsWith("." + Properties.Settings.Default.ExtensionMP4))
            {
                DataText_MediaMime = Properties.Settings.Default.MediaType_MP4;
            }

            Url_Video = currentItem != null ? currentItem.MultimediaUrl : "";
            Label_MediaItem = currentItem != null ? currentItem.Name : "";

            

            isCurrentVideoPrepared = true;

        }

        private void ConfigureNav()
        {
            var toggleBack = false;
            var toggleForward = false;

            if (IsPossibleBackward())
            {
                toggleBack = true;
            }

            if (IsPossibleForward())
            {
                toggleForward = true;
            }

            ToggleNavForward(toggleForward);

            var ix = (multimediaViewItems == null || !multimediaViewItems.Any() ? 0 : Position_VideoItems + 1);
            var count = (multimediaViewItems == null ? 0 : multimediaViewItems.Count);

            Label_Nav = ix + "/" + count;
        }

        private bool IsPossibleBackward()
        {
            return Position_VideoItems > 0;
        }

        private bool IsPossibleForward()
        {
            return multimediaViewItems != null && multimediaViewItems.Any() && Position_VideoItems < multimediaViewItems.Count - 1;
        }

        private void ToggleNavForward(bool toggleTo)
        {
            IsEnabled_NavLast = IsEnabled_NavNext = toggleTo;
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultData)
            {


                if (serviceAgentElastic.ResultData.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return;


                }
                VideosLoadedFromDb = true;
                TestInitializationFactory();

            }


        }

        private void TestInitializationFactory()
        {
            if (VideosLoadedFromDb)
            {
                if (viewMediaFactory == null)
                {
                    viewMediaFactory = new ViewMediaFactory(localConfig, MultimediaItemType.Video, webSocketServiceAgent.oItemUser, webSocketServiceAgent.oItemGroup);
                    viewMediaFactory.PropertyChanged += ViewMediaItemFactory_PropertyChanged;
                }

                var result = viewMediaFactory.SaveMultimediaFiles(serviceAgentElastic.RefToMediafiles,
                    serviceAgentElastic.MediaAttributes,
                    serviceAgentElastic.MediaFiles,
                    webSocketServiceAgent.GetFileSystemObject(),
                    webSocketServiceAgent.DataText_SessionId);

            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;

        }

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavFirst)
                {
                    Position_VideoItems = 0;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavPrevious)
                {
                    Position_VideoItems--;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavNext)
                {
                    Position_VideoItems++;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_NavLast)
                {
                    Position_VideoItems = multimediaViewItems.Count - 1;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_Stopped)
                {
                    var pos = webSocketServiceAgent.Request["Pos"].ToString();
                    SaveBookmark(localConfig.OItem_token_logstate_stop, pos);
                    if (IsChecked_AutoPlay && IsPossibleForward())
                    {
                        Position_VideoItems++;
                    }
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_CurPos)
                {
                    var pos = webSocketServiceAgent.Request["Pos"].ToString();
                    SaveBookmark(localConfig.OItem_object_position, pos);
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_Started)
                {
                    var pos = webSocketServiceAgent.Request["Pos"].ToString();
                    SaveBookmark(localConfig.OItem_token_logstate_start, pos);
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_Paused)
                {
                    var pos = webSocketServiceAgent.Request["Pos"].ToString();
                    SaveBookmark(localConfig.OItem_token_logstate_pause, pos);
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "started.mediaItem")
                {
                    //var idMediaItem = webSocketServiceAgent.Request["idMediaItem"].ToString();
                    //var oItemMediaItem = serviceAgentElastic.GetOItem(idMediaItem, localConfig.Globals.Type_Object);

                    //var interServiceMessage = new InterServiceMessage
                    //{
                    //    ChannelId = Channels.ParameterList,
                    //    OItems = new List<clsOntologyItem>
                    //    {
                    //        oItemMediaItem
                    //    }
                    //};
                    
                    //webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                
                
            }
            else if (e.PropertyName == NotifyChanges.Websocket_DedicatedSenderArgument)
            {
                if (webSocketServiceAgent.DedicatedSenderArgument != null)
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.ReceiverOfDedicatedSender,
                        ReceiverId = webSocketServiceAgent.DedicatedSenderArgument.Value
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }
            }


        }

        private void SaveBookmark(clsOntologyItem logState, string posString)
        {
            double pos;
            if (!IsToggled_ToggleBookmarks) return;
            if (double.TryParse(posString, positionNumberStyle, positionFormatCulture, out pos))
            {
                if (pos - lastPosSaved > 10)
                {
                    var result = serviceAgentElastic.SavePos(pos, currentItem.OMultimediaItem, webSocketServiceAgent.oItemUser, referenceItem, logState);

                    if (result.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        Error_BookMark = new OntoMsg_Module.Models.ViewMessage("No Bookmark", "Save-Error", OntoMsg_Module.Models.ViewMessageType.Exclamation);
                        errorHideTimer.Start();

                    }
                    else
                    {
                        DataText_LastSavedBookmark = "Last Bookmark:" + result.Val_Date.ToString();
                        lastPosSaved = pos;
                    }

                }
            }
        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void LoadMedia()
        {
            if (viewMediaFactory != null)
            {
                viewMediaFactory.StopFactory();
            }
            Position_VideoItems = 0;
            Count_PlaylistItems = 0;
            multimediaViewItems = new List<MultimediaViewItem>();
            Url_Video = "";
            Count_ToSave = 0;
            isCurrentVideoPrepared = false;
            ConfigureNav();
            
            VideosLoadedFromDb = false;
            serviceAgentElastic.GetReferencedMediaItems(referenceItem);
        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ParameterList && IsToggled_Listen)
            {
                IsToggled_Listen = true;
                webSocketServiceAgent.SendCommand("Reset");
                var objectItem = message.OItems.LastOrDefault();

                if (objectItem == null) return;

                if (referenceItem != null && referenceItem.GUID == objectItem.GUID) return;
                referenceItem = objectItem;
                LoadMedia();
            }
            else if (message.ChannelId == Notifications.NotifyChanges.Channel_MediaItemList && IsToggled_Listen)
            {
                IsToggled_Listen = true;
                webSocketServiceAgent.SendCommand("Reset");
                var genericParameters = message.GenericParameterItems;

                if (genericParameters == null) return;

                var firstParam = genericParameters.FirstOrDefault();

                if (firstParam == null) return;

                var mediaListItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(firstParam.ToString()).Select(dynItm => new MediaListItem
                {
                    IdAttributeCreated = dynItm.IdAttributeCreated,
                    BookmarkCount = dynItm.BookmarkCount,
                    Created = dynItm.Created,
                    IdFile = dynItm.IdFile,
                    NameFile = dynItm.NameFile,
                    IdItem = dynItm.IdItem,
                    IdRef = dynItm.IdRef,
                    IdRow = dynItm.IdRow,
                    NameItem = dynItm.NameItem,
                    NameRef = dynItm.NameRef,
                    OrderId = dynItm.OrderId,
                    Random = dynItm.Random
                }).ToList();

                VideosLoadedFromDb = true;
                if (VideosLoadedFromDb)
                {
                    if (viewMediaFactory == null)
                    {
                        viewMediaFactory = new ViewMediaFactory(localConfig, MultimediaItemType.Video, webSocketServiceAgent.oItemUser, webSocketServiceAgent.oItemGroup);
                        viewMediaFactory.PropertyChanged += ViewMediaItemFactory_PropertyChanged;
                    }

                    var result = viewMediaFactory.SaveMultimediaFiles(mediaListItems, webSocketServiceAgent.GetFileSystemObject(), webSocketServiceAgent.DataText_SessionId);

                }
            }
        }

     
        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
