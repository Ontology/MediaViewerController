﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Attributes 
{
    public class DhtmlXGridAttribute : Attribute
    {
        public string Header { get; set; }
        public string InitWith { get; set; }
        public string ColAlign { get; set; }
        public string ColType { get; set; }
        public string ColSort { get; set; }
        public string Filter { get; set; }
        public int OrderId { get; set; }
        public bool Hidden { get; set; }

    }
}
