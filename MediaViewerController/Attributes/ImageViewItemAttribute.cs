﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Attributes
{
    public class ImageViewItemAttribute : Attribute
    {
        public string JsonProperty { get; set; }
    }
}
