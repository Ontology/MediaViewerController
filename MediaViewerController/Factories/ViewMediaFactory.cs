﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediaStore_Module;
using MediaViewerController.Models;
using OntoMsg_Module.Services;
using OntoMsg_Module.Models;
using System.Reflection;
using MediaViewerController.Attributes;
using MediaViewerController.Converters;
using System.Drawing;
using OntoMsg_Module.Base;
using MediaViewerController.Notifications;
using System.Threading;
using OntologyAppDBConnector.Base;
using OntoMsg_Module.WebSocketServices;

namespace MediaViewerController.Factories
{
    public class ViewMediaFactory : NotifyPropertyChange
    {
        private object itemLocker = new object();
        private object serviceLocker = new object();
        private clsLocalConfig localConfig;

        private List<clsObjectRel> refToMultimediaItems;
        private List<clsObjectAtt> multimediaAttributes;
        private List<clsObjectRel> multimediaFiles;
        private FileSystemServiceAgent webServerServiceAgent;
        private string sessionId;

        private MultimediaItemType multimediaItemType;

        private clsOntologyItem oItemUser;
        private clsOntologyItem oItemGroup;

        private int maxWidth;
        private int maxHeight;

        private bool random;

        private Thread saveMultimediaAsync;

        private int multiMediaCountToSave;
        public int MultiMediaCountToSave
        {
            get
            {
                lock(itemLocker)
                {
                    return multiMediaCountToSave;
                }
                
            }
            set
            {
                lock(itemLocker)
                {
                    multiMediaCountToSave = value;
                }
                
                RaisePropertyChanged(NotifyChanges.ViewMediaFactory_MultimediaItemCountToSave);
            }
        }

        private MultimediaViewItem nextMultimediaItem;
        public MultimediaViewItem NextMultimediaItem
        {
            get
            {
                lock(itemLocker)
                {
                    return nextMultimediaItem;
                }
                
            }
            set
            {
                lock(itemLocker)
                {
                    nextMultimediaItem = value;
                }
                
                RaisePropertyChanged(NotifyChanges.ViewMediaFactory_NextMultimediaItem);
            }
        }

        private clsOntologyItem resultDateItems;
        public clsOntologyItem ResultDateItems
        {
            get { return resultDateItems; }
            set
            {
                resultDateItems = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultDateItems);
            }
        }

        private ResultSaveMultimediaItems resultSaveMultimediaItems;
        public ResultSaveMultimediaItems ResultSaveMultimediaItems
        {
            get
            {
                lock(serviceLocker)
                {
                    return resultSaveMultimediaItems;
                }
            }
            set
            {
                lock(serviceLocker)
                {
                    resultSaveMultimediaItems = value;
                }

                RaisePropertyChanged(nameof(ResultSaveMultimediaItems));
            }
        }

        private bool stopFactory = false;

        public void StopFactory()
        {
            if (saveMultimediaAsync == null || saveMultimediaAsync.ThreadState != ThreadState.Running) return;
            stopFactory = true;
        }

        private List<MultimediaViewItem> errorItems = new List<MultimediaViewItem>();
        public List<MultimediaViewItem> ErrorItems
        {
            get { return errorItems; }
            set
            {
                errorItems = value;
            }
        }

        private ThumbNailImage thumbNailImage;

        private MediaStoreConnector mediaStoreConnector;

        public clsOntologyItem SaveMultimediaFiles(List<MediaListItem> mediaListItems, FileSystemServiceAgent webServerServiceAgent, string sessionId)
        {
            this.webServerServiceAgent = webServerServiceAgent;
            this.sessionId = sessionId;
            var result = localConfig.Globals.LState_Success.Clone();

            try
            {
                if (saveMultimediaAsync != null)
                {
                    saveMultimediaAsync.Abort();
                }

            }
            catch (Exception ex)
            {

            }
            saveMultimediaAsync = new Thread(SaveMultimediaFilesByMediaListItemsAsync);
            saveMultimediaAsync.Start(mediaListItems);

            return result;
        }

        public async Task<ResultSaveMultimediaItems> SaveMultimediaFiles(List<clsObjectRel> mediaRels,
            WebsocketServiceAgent serviceAgent)
        {
            var fileRels = mediaRels.Where(mediaRel => mediaRel.ID_Parent_Other == localConfig.OItem_type_file.GUID).ToList();

            var factoryResult = new ResultSaveMultimediaItems
            {
                Result = localConfig.Globals.LState_Success.Clone(),
                SessionFiles = new List<SessionFile>()
            };

            foreach (var fileRel in fileRels)
            {
                var fileName = fileRel.ID_Other + System.IO.Path.GetExtension(fileRel.Name_Other);
                var sessionFile = serviceAgent.RequestUserFile(serviceAgent.oItemUser.GUID, fileName);


                
                if (fileName.EndsWith(Properties.Settings.Default.Extension3GP) ||
                    fileName.EndsWith(Properties.Settings.Default.ExtensionAVI) ||
                    fileName.EndsWith(Properties.Settings.Default.ExtensionMOD) ||
                    fileName.EndsWith(Properties.Settings.Default.ExtensionMP4) ||
                    fileName.EndsWith(Properties.Settings.Default.ExtensionMTS) ||
                    fileName.EndsWith(Properties.Settings.Default.ExtensionVMW))
                {
                    multimediaItemType = MultimediaItemType.Video;
                    
                }
                else if (fileRel.ID_Parent_Object == localConfig.OItem_type_media_item.GUID)
                {
                    multimediaItemType = MultimediaItemType.Audio;
                }
                else if (fileRel.ID_Parent_Object == localConfig.OItem_type_pdf_documents.GUID)
                {
                    multimediaItemType = MultimediaItemType.PDF;
                }
                else if (fileRel.ID_Parent_Object == localConfig.OItem_type_images__graphic_.GUID)
                {
                    multimediaItemType = MultimediaItemType.Image;
                }

                if (sessionFile != null)
                {
                    factoryResult.Result = SaveImage(fileRel.ID_Other, fileRel.Name_Other, sessionFile);
                    factoryResult.SessionFiles.Add(sessionFile);        
                }
                else
                {
                    factoryResult.Result = localConfig.Globals.LState_Error.Clone();
                }
                
            }

           
            ResultSaveMultimediaItems = factoryResult;
            return factoryResult;
        }

        public async Task<clsOntologyItem> SaveMultimediaFiles(List<clsObjectRel> refToMultiMediaItems,
            List<clsObjectAtt> multimediaAttributes,
            List<clsObjectRel> multimediaFiles,
            FileSystemServiceAgent webServerServiceAgent,
            string sessionId,
            bool random = false)
        {
            lock(serviceLocker)
            {
                this.random = random;
                this.webServerServiceAgent = webServerServiceAgent;
                var result = localConfig.Globals.LState_Success.Clone();
                this.sessionId = sessionId;
                this.refToMultimediaItems = refToMultiMediaItems;
                this.multimediaAttributes = multimediaAttributes;
                this.multimediaFiles = multimediaFiles;
                SaveMultimediaFilesAsync();
            }
            

            return ResultDateItems;
        }

        private void SaveMultimediaFilesByMediaListItemsAsync(object threadParam)
        {
            var mediaListItems = (List<MediaListItem>)threadParam;

            if (multimediaItemType == MultimediaItemType.Audio)
            {
                var items = mediaListItems.Where(mediaItem => mediaItem.NameFile.ToLower()
                            .EndsWith("." + Properties.Settings.Default.ExtensionMP3) || mediaItem.NameFile.ToLower()
                            .EndsWith("." + Properties.Settings.Default.ExtensionWAV)).Select(medItm => new MultimediaViewItem
                            {
                                Id = medItm.IdItem,
                                Name = medItm.NameItem,
                                IdFile = medItm.IdFile,
                                NameFile = medItm.NameFile,
                                OrderId = medItm.OrderId.Value,
                                OMultimediaItem = new clsOntologyItem
                                {
                                    GUID = medItm.IdItem,
                                    Name = medItm.NameItem,
                                    GUID_Parent = localConfig.OItem_type_media_item.GUID,
                                    Type = localConfig.Globals.Type_Object,
                                    Val_Date = medItm.Created
                                },
                                RandomOrderId = medItm.Random.Value,
                                SessionFile = SaveImage(medItm.IdFile, medItm.NameFile, sessionId)
                            }).ToList();

                MultiMediaCountToSave = items.Count;
                items.ForEach(multimwdiaDataSource =>
                {
                    if (stopFactory)
                    {
                        stopFactory = false;
                        return;
                    }

                    

                    if (multimwdiaDataSource.SessionFile != null)
                    {
                        NextMultimediaItem = multimwdiaDataSource;
                    }
                    else
                    {
                        ErrorItems.Add(multimwdiaDataSource);
                    }

                });
            }
            else if (multimediaItemType == MultimediaItemType.Image)
            {
                var items = mediaListItems.Select(medItm => new MultimediaViewItem
                             {
                                 Id = medItm.IdItem,
                                 Name = medItm.NameItem,
                                 IdFile = medItm.IdFile,
                                 NameFile = medItm.NameFile,
                                 OrderId = medItm.OrderId.Value,
                                 OMultimediaItem = new clsOntologyItem
                                 {
                                     GUID = medItm.IdItem,
                                     Name = medItm.NameItem,
                                     GUID_Parent = localConfig.OItem_type_media_item.GUID,
                                     Type = localConfig.Globals.Type_Object,
                                     Val_Date = medItm.Created
                                 },
                                 RandomOrderId = medItm.Random.Value,
                                 SessionFile = SaveImage(medItm.IdFile, medItm.NameFile, sessionId)
                             }).ToList();
                MultiMediaCountToSave = items.Count;
                items.ForEach(multimwdiaDataSource =>
                {


                    if (multimwdiaDataSource.SessionFile != null)
                    {
                        NextMultimediaItem = multimwdiaDataSource;
                    }
                    else
                    {
                        ErrorItems.Add(multimwdiaDataSource);
                    }

                });
            }
            else if (multimediaItemType == MultimediaItemType.PDF)
            {
                var items = mediaListItems.Where(mediaItem => mediaItem.NameFile.ToLower()
                            .EndsWith("." + Properties.Settings.Default.ExtensionPDF)).Select(medItm => new MultimediaViewItem
                            {
                                Id = medItm.IdItem,
                                Name = medItm.NameItem,
                                IdFile = medItm.IdFile,
                                NameFile = medItm.NameFile,
                                OrderId = medItm.OrderId.Value,
                                OMultimediaItem = new clsOntologyItem
                                {
                                    GUID = medItm.IdItem,
                                    Name = medItm.NameItem,
                                    GUID_Parent = localConfig.OItem_type_media_item.GUID,
                                    Type = localConfig.Globals.Type_Object,
                                    Val_Date = medItm.Created
                                },
                                RandomOrderId = medItm.Random.Value,
                                SessionFile = SaveImage(medItm.IdFile, medItm.NameFile, sessionId)
                            }).ToList();
               

                MultiMediaCountToSave = items.Count;
                items.ForEach(multimwdiaDataSource =>
                {
                    

                    if (multimwdiaDataSource.SessionFile != null)
                    {
                        NextMultimediaItem = multimwdiaDataSource;
                    }
                    else
                    {
                        ErrorItems.Add(multimwdiaDataSource);
                    }

                });
            }
            else if (multimediaItemType == MultimediaItemType.Video)
            {
                var items = mediaListItems.Where(mediaItem => mediaItem.NameFile.ToLower()
                            .EndsWith("." + Properties.Settings.Default.ExtensionMP4) || mediaItem.NameFile.ToLower()
                            .EndsWith("." + Properties.Settings.Default.ExtensionAVI) || mediaItem.NameFile.ToLower()
                            .EndsWith("." + Properties.Settings.Default.ExtensionVMW) || mediaItem.NameFile.ToLower()
                            .EndsWith("." + Properties.Settings.Default.ExtensionMOD) || mediaItem.NameFile.ToLower()
                            .EndsWith("." + Properties.Settings.Default.ExtensionMTS) || mediaItem.NameFile.ToLower()
                            .EndsWith("." + Properties.Settings.Default.Extension3GP)).Select(medItm => new MultimediaViewItem
                            {
                                Id = medItm.IdItem,
                                Name = medItm.NameItem,
                                IdFile = medItm.IdFile,
                                NameFile = medItm.NameFile,
                                OrderId = medItm.OrderId.Value,
                                OMultimediaItem = new clsOntologyItem
                                {
                                    GUID = medItm.IdItem,
                                    Name = medItm.NameItem,
                                    GUID_Parent = localConfig.OItem_type_media_item.GUID,
                                    Type = localConfig.Globals.Type_Object,
                                    Val_Date = medItm.Created
                                },
                                RandomOrderId = medItm.Random.Value,
                                SessionFile = SaveImage(medItm.IdFile, medItm.NameFile, sessionId)
                            }).ToList();


                MultiMediaCountToSave = items.Count;
                items.ForEach(multimwdiaDataSource =>
                {
                    

                    if (multimwdiaDataSource.SessionFile != null)
                    {
                        NextMultimediaItem = multimwdiaDataSource;
                    }
                    else
                    {
                        ErrorItems.Add(multimwdiaDataSource);
                    }

                });
            }

            ResultDateItems = localConfig.Globals.LState_Success.Clone();
        }

        private void SaveMultimediaFilesAsync()
        {
            Random r = new Random();
            if (multimediaItemType ==  MultimediaItemType.Audio)
            {
                var items = (from refToMultimedia in refToMultimediaItems
                             join idOfMultimedia in multimediaAttributes.Where(imgAtt => imgAtt.ID_AttributeType == localConfig.OItem_attribute_id.GUID) on refToMultimedia.ID_Object equals idOfMultimedia.ID_Object into idsOfMultimedia
                             from idOfMultimedia in idsOfMultimedia.DefaultIfEmpty()
                             join taking in multimediaAttributes.Where(imgAtt => imgAtt.ID_AttributeType == localConfig.OItem_attribute_taking.GUID) on refToMultimedia.ID_Object equals taking.ID_Object into takings
                             from taking in takings.DefaultIfEmpty()
                             join fileItem in multimediaFiles on refToMultimedia.ID_Object equals fileItem.ID_Object
                             select new { refToMultimedia, idOfMultimedia, taking, fileItem, randomId = r.Next() })
                            .OrderBy(mediaItem => random ? mediaItem.randomId : mediaItem.refToMultimedia.OrderID)
                            .ThenBy(mediaItem => mediaItem.refToMultimedia.Name_Object)
                            .Where(mediaItem => mediaItem.fileItem.Name_Other.ToLower()
                            .EndsWith("." + Properties.Settings.Default.ExtensionMP3) || mediaItem.fileItem.Name_Other.ToLower()
                            .EndsWith("." + Properties.Settings.Default.ExtensionWAV)).ToList();

                MultiMediaCountToSave = items.Count;
                items.ForEach(multimwdiaDataSource =>
                {
                    if (stopFactory)
                    {
                        stopFactory = false;
                        return;
                    }

                    var imageItem = new MultimediaViewItem
                    {
                        Id = multimwdiaDataSource.refToMultimedia.ID_Object,
                        Name = multimwdiaDataSource.refToMultimedia.Name_Object,
                        IdFile = multimwdiaDataSource.fileItem.ID_Other,
                        NameFile = multimwdiaDataSource.fileItem.Name_Other,
                        OrderId = multimwdiaDataSource.refToMultimedia.OrderID.Value,
                        OMultimediaItem = new clsOntologyItem
                        {
                            GUID = multimwdiaDataSource.refToMultimedia.ID_Object,
                            Name = multimwdiaDataSource.refToMultimedia.Name_Object,
                            GUID_Parent = localConfig.OItem_type_media_item.GUID,
                            Type = localConfig.Globals.Type_Object,
                            Val_Date = multimwdiaDataSource.taking != null ? multimwdiaDataSource.taking.Val_Date : null
                        },
                        RandomOrderId = multimwdiaDataSource.randomId,
                        SessionFile = SaveImage(multimwdiaDataSource.fileItem.ID_Other, multimwdiaDataSource.fileItem.Name_Other, sessionId)
                    };


                    if (imageItem.SessionFile != null)
                    {
                        NextMultimediaItem = imageItem;
                    }
                    else
                    {
                        ErrorItems.Add(imageItem);
                    }

                });
            }
            else if (multimediaItemType == MultimediaItemType.Image)
            {
                var items = (from refToMultimedia in refToMultimediaItems
                             join idOfMultimedia in multimediaAttributes.Where(imgAtt => imgAtt.ID_AttributeType == localConfig.OItem_attribute_id.GUID) on refToMultimedia.ID_Object equals idOfMultimedia.ID_Object into idsOfMultimedia
                             from idOfMultimedia in idsOfMultimedia.DefaultIfEmpty()
                             join taking in multimediaAttributes.Where(imgAtt => imgAtt.ID_AttributeType == localConfig.OItem_attribute_taking.GUID) on refToMultimedia.ID_Object equals taking.ID_Object into takings
                             from taking in takings.DefaultIfEmpty()
                             join fileItem in multimediaFiles on refToMultimedia.ID_Object equals fileItem.ID_Object
                             select new { refToMultimedia, idOfMultimedia, taking, fileItem, randomId = r.Next() })
                            .OrderBy(mediaItem => random ? mediaItem.randomId : mediaItem.refToMultimedia.OrderID)
                            .ThenBy(mediaItem => mediaItem.refToMultimedia.Name_Object).ToList();

                MultiMediaCountToSave = items.Count;
                items.ForEach(multimwdiaDataSource =>
                {
                    if (stopFactory)
                    {
                        stopFactory = false;
                        return;
                    }

                    var imageItem = new MultimediaViewItem
                    {
                        Id = multimwdiaDataSource.refToMultimedia.ID_Object,
                        Name = multimwdiaDataSource.refToMultimedia.Name_Object,
                        IdFile = multimwdiaDataSource.fileItem.ID_Other,
                        NameFile = multimwdiaDataSource.fileItem.Name_Other,
                        OrderId = multimwdiaDataSource.refToMultimedia.OrderID.Value,
                        RandomOrderId = r.Next(),
                        OMultimediaItem = new clsOntologyItem
                        {
                            GUID = multimwdiaDataSource.refToMultimedia.ID_Object,
                            Name = multimwdiaDataSource.refToMultimedia.Name_Object,
                            GUID_Parent = localConfig.OItem_type_images__graphic_.GUID,
                            Type = localConfig.Globals.Type_Object,
                            Val_Date = multimwdiaDataSource.taking != null ? multimwdiaDataSource.taking.Val_Date : null
                        },
                        SessionFile = SaveImage(multimwdiaDataSource.fileItem.ID_Other, multimwdiaDataSource.fileItem.Name_Other, sessionId)
                    };


                    if (imageItem.SessionFile != null)
                    {
                        NextMultimediaItem = imageItem;
                    }
                    else
                    {
                        ErrorItems.Add(imageItem);
                    }

                });
            }
            else if (multimediaItemType == MultimediaItemType.PDF)
            {
                var items = (from refToMultimedia in refToMultimediaItems
                             join idOfMultimedia in multimediaAttributes.Where(imgAtt => imgAtt.ID_AttributeType == localConfig.OItem_attribute_id.GUID) on refToMultimedia.ID_Object equals idOfMultimedia.ID_Object into idsOfMultimedia
                             from idOfMultimedia in idsOfMultimedia.DefaultIfEmpty()
                             join taking in multimediaAttributes.Where(imgAtt => imgAtt.ID_AttributeType == localConfig.OItem_attribute_taking.GUID) on refToMultimedia.ID_Object equals taking.ID_Object into takings
                             from taking in takings.DefaultIfEmpty()
                             join fileItem in multimediaFiles on refToMultimedia.ID_Object equals fileItem.ID_Object
                             select new { refToMultimedia, idOfMultimedia, taking, fileItem, randomId = r.Next() })
                            .OrderBy(mediaItem => random ? mediaItem.randomId : mediaItem.refToMultimedia.OrderID)
                            .ThenBy(mediaItem => mediaItem.refToMultimedia.Name_Object).Where(mediaItem => mediaItem.fileItem.Name_Other.ToLower()
                            .EndsWith("." + Properties.Settings.Default.ExtensionPDF)).ToList();

                MultiMediaCountToSave = items.Count;
                items.ForEach(multimwdiaDataSource =>
                {
                    if (stopFactory)
                    {
                        stopFactory = false;
                        return;
                    }

                    var imageItem = new MultimediaViewItem
                    {
                        Id = multimwdiaDataSource.refToMultimedia.ID_Object,
                        Name = multimwdiaDataSource.refToMultimedia.Name_Object,
                        IdFile = multimwdiaDataSource.fileItem.ID_Other,
                        NameFile = multimwdiaDataSource.fileItem.Name_Other,
                        OrderId = multimwdiaDataSource.refToMultimedia.OrderID.Value,
                        OMultimediaItem = new clsOntologyItem { GUID = multimwdiaDataSource.refToMultimedia.ID_Object,
                            Name = multimwdiaDataSource.refToMultimedia.Name_Object,
                            GUID_Parent = localConfig.OItem_type_pdf_documents.GUID,
                            Type = localConfig.Globals.Type_Object,
                            Val_Date = multimwdiaDataSource.taking != null ? multimwdiaDataSource.taking.Val_Date : null
                        },
                        RandomOrderId = r.Next(),
                        SessionFile = SaveImage(multimwdiaDataSource.fileItem.ID_Other, multimwdiaDataSource.fileItem.Name_Other, sessionId)
                    };


                    if (imageItem.SessionFile != null)
                    {
                        NextMultimediaItem = imageItem;
                    }
                    else
                    {
                        ErrorItems.Add(imageItem);
                    }

                });
            }
            else if (multimediaItemType == MultimediaItemType.Video)
            {
                var items = (from refToMultimedia in refToMultimediaItems
                             join idOfMultimedia in multimediaAttributes.Where(imgAtt => imgAtt.ID_AttributeType == localConfig.OItem_attribute_id.GUID) on refToMultimedia.ID_Object equals idOfMultimedia.ID_Object into idsOfMultimedia
                             from idOfMultimedia in idsOfMultimedia.DefaultIfEmpty()
                             join taking in multimediaAttributes.Where(imgAtt => imgAtt.ID_AttributeType == localConfig.OItem_attribute_taking.GUID) on refToMultimedia.ID_Object equals taking.ID_Object into takings
                             from taking in takings.DefaultIfEmpty()
                             join fileItem in multimediaFiles on refToMultimedia.ID_Object equals fileItem.ID_Object
                             select new { refToMultimedia, idOfMultimedia, taking, fileItem, randomId = r.Next() })
                            .OrderBy(mediaItem => random ? mediaItem.randomId : mediaItem.refToMultimedia.OrderID)
                            .ThenBy(mediaItem => mediaItem.refToMultimedia.Name_Object)
                            .Where(mediaItem => mediaItem.fileItem.Name_Other.ToLower()
                            .EndsWith("." + Properties.Settings.Default.ExtensionMP4) || mediaItem.fileItem.Name_Other.ToLower()
                            .EndsWith("." + Properties.Settings.Default.ExtensionAVI) || mediaItem.fileItem.Name_Other.ToLower()
                            .EndsWith("." + Properties.Settings.Default.ExtensionVMW) || mediaItem.fileItem.Name_Other.ToLower()
                            .EndsWith("." + Properties.Settings.Default.ExtensionMOD) || mediaItem.fileItem.Name_Other.ToLower()
                            .EndsWith("." + Properties.Settings.Default.ExtensionMTS) || mediaItem.fileItem.Name_Other.ToLower()
                            .EndsWith("." + Properties.Settings.Default.Extension3GP)).ToList();

                MultiMediaCountToSave = items.Count;
                items.ForEach(multimwdiaDataSource =>
                {
                    if (stopFactory)
                    {
                        stopFactory = false;
                        return;
                    }

                    var imageItem = new MultimediaViewItem
                    {
                        Id = multimwdiaDataSource.refToMultimedia.ID_Object,
                        Name = multimwdiaDataSource.refToMultimedia.Name_Object,
                        IdFile = multimwdiaDataSource.fileItem.ID_Other,
                        NameFile = multimwdiaDataSource.fileItem.Name_Other,
                        OrderId = multimwdiaDataSource.refToMultimedia.OrderID.Value,
                        OMultimediaItem = new clsOntologyItem
                        {
                            GUID = multimwdiaDataSource.refToMultimedia.ID_Object,
                            Name = multimwdiaDataSource.refToMultimedia.Name_Object,
                            GUID_Parent = localConfig.OItem_type_media_item.GUID,
                            Type = localConfig.Globals.Type_Object,
                            Val_Date = multimwdiaDataSource.taking != null ? multimwdiaDataSource.taking.Val_Date : null
                        },
                        RandomOrderId = r.Next(),
                        SessionFile = SaveImage(multimwdiaDataSource.fileItem.ID_Other, multimwdiaDataSource.fileItem.Name_Other, sessionId)
                    };


                    if (imageItem.SessionFile != null)
                    {
                        NextMultimediaItem = imageItem;
                    }
                    else
                    {
                        ErrorItems.Add(imageItem);
                    }

                });
            }

            ResultDateItems = localConfig.Globals.LState_Success.Clone();
        }



        private SessionFile SaveImage(string idFile, string nameFile, string sessionId)
        {
            SessionFile sessionFile;
            if (multimediaItemType == MultimediaItemType.Image)
            {
                sessionFile = webServerServiceAgent.RequestUserFile(oItemUser.GUID, idFile + System.IO.Path.GetExtension(nameFile), false);
            }
            else
            {
                sessionFile = webServerServiceAgent.RequestUserFile(oItemUser.GUID, idFile + System.IO.Path.GetExtension(nameFile), false);
            }
            
            var oItemFile = new clsOntologyItem
            {
                GUID = idFile,
                Name = nameFile,
                GUID_Parent = localConfig.OItem_type_file.GUID,
                Type = localConfig.Globals.Type_Object
            };
            
            try
            {
                if (!sessionFile.Exists)
                {
                    
                    if (multimediaItemType == MultimediaItemType.Image)
                    {
                        var readStream = mediaStoreConnector.GetManagedMediaStream(oItemFile, true);
                        var image = Image.FromStream(readStream);

                        var viewImage = thumbNailImage.GetThumbNailOfImageByMax(image, 1400);
                        var thumbNail = thumbNailImage.GetThumbNailOfImageByMax(image, 100);
                        readStream.Close();
                        viewImage.Save(sessionFile.FilePath);
                        thumbNail.Save(sessionFile.FilePath.Replace(idFile + System.IO.Path.GetExtension(nameFile), "thumb_" + idFile + System.IO.Path.GetExtension(nameFile)));
                        sessionFile.ExtraUrl = sessionFile.FileUri.AbsolutePath.Replace(idFile + System.IO.Path.GetExtension(nameFile), "thumb_" + idFile + System.IO.Path.GetExtension(nameFile));
                    }
                    else
                    {
                        var result = localConfig.Globals.LState_Success.Clone();
                        if (!System.IO.File.Exists(sessionFile.FilePath))
                        {
                            result = mediaStoreConnector.SaveManagedMediaToFile(oItemFile, sessionFile.FilePath, false, true);

                        }

                        if (result.GUID == localConfig.Globals.LState_Error.GUID)
                        {
                            sessionFile = null;
                        }
                    }
                    

                }
                else
                {
                    sessionFile.ExtraUrl = sessionFile.FileUri.AbsolutePath.Replace(idFile + System.IO.Path.GetExtension(nameFile), "thumb_" + idFile + System.IO.Path.GetExtension(nameFile));
                }
                
                

                return sessionFile;
            }
            catch(Exception ex)
            {
                return null;
            }
            

            
        }

        private clsOntologyItem SaveImage(string idFile, string nameFile, SessionFile sessionFile)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            var oItemFile = new clsOntologyItem
            {
                GUID = idFile,
                Name = nameFile,
                GUID_Parent = localConfig.OItem_type_file.GUID,
                Type = localConfig.Globals.Type_Object
            };

            try
            {
                if (!sessionFile.Exists)
                {

                    if (multimediaItemType == MultimediaItemType.Image)
                    {
                        var readStream = mediaStoreConnector.GetManagedMediaStream(oItemFile, true);
                        var image = Image.FromStream(readStream);

                        var viewImage = thumbNailImage.GetThumbNailOfImageByMax(image, 1400);
                        var thumbNail = thumbNailImage.GetThumbNailOfImageByMax(image, 100);
                        readStream.Close();
                        viewImage.Save(sessionFile.FilePath);
                        thumbNail.Save(sessionFile.FilePath.Replace(idFile + System.IO.Path.GetExtension(nameFile), "thumb_" + idFile + System.IO.Path.GetExtension(nameFile)));
                        sessionFile.ExtraUrl = sessionFile.FileUri.AbsolutePath.Replace(idFile + System.IO.Path.GetExtension(nameFile), "thumb_" + idFile + System.IO.Path.GetExtension(nameFile));
                    }
                    else
                    {
                        result = localConfig.Globals.LState_Success.Clone();
                        if (!System.IO.File.Exists(sessionFile.FilePath))
                        {
                            result = mediaStoreConnector.SaveManagedMediaToFile(oItemFile, sessionFile.FilePath, false, true);

                        }

                        if (result.GUID == localConfig.Globals.LState_Error.GUID)
                        {
                            sessionFile = null;
                        }
                    }


                }
                else
                {
                    sessionFile.ExtraUrl = sessionFile.FileUri.AbsolutePath.Replace(idFile + System.IO.Path.GetExtension(nameFile), "thumb_" + idFile + System.IO.Path.GetExtension(nameFile));
                }



                return result;
            }
            catch (Exception ex)
            {
                return localConfig.Globals.LState_Error.Clone() ;
            }



        }

        public ViewMediaFactory(clsLocalConfig localConfig, int maxWidth, int maxHeight, clsOntologyItem oItemUser, clsOntologyItem oItemGroup)
        {
            this.multimediaItemType = MultimediaItemType.Image;
            this.localConfig = localConfig;
            this.maxHeight = maxHeight;
            this.maxWidth = maxWidth;
            this.oItemUser = oItemUser;
            this.oItemGroup = oItemGroup;
            Initialize();

        }

        public ViewMediaFactory(clsLocalConfig localConfig, MultimediaItemType multimediaItemType, clsOntologyItem oItemUser, clsOntologyItem oItemGroup)
        {
            this.multimediaItemType = multimediaItemType;
            this.oItemUser = oItemUser;
            this.oItemGroup = oItemGroup;
            this.localConfig = localConfig;
            Initialize();

        }

        private void Initialize()
        {
            mediaStoreConnector = new MediaStoreConnector(localConfig.Globals);
            thumbNailImage = new ThumbNailImage();
        }

    }

    public class ResultSaveMultimediaItems
    {
        public clsOntologyItem Result { get; set; }
        public List<SessionFile> SessionFiles { get; set; } 
    }
}
