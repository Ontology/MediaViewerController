﻿using MediaViewerController.Models;
using MediaViewerController.Notifications;
using Newtonsoft.Json;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MediaViewerController.Factories
{
    public class BookmarkFactory : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;
        public clsOntologyItem oItemUser;
        public clsOntologyItem oItemGroup;

        private StreamWriter streamWriter;

        private Thread saveDateitemsAsync;

        private bool stopThread = false;

        public List<BookmarkTreeItem> BookmarkItems { get; private set; }

        private clsOntologyItem resultDateItems;
        public clsOntologyItem ResultDateItems
        {
            get { return resultDateItems; }
            set
            {
                resultDateItems = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultDateItems);
            }
        }

        private List<clsObjectRel> bookmarksOfReferenceItems;
        private List<clsObjectRel> bookmarksOfMediaItems;
        private List<clsObjectAtt> secondsOfBookmarks;
        private List<clsObjectAtt> datetimeCreateOfBookmarks;
        private List<clsObjectRel> typesOfBookmarks;
        private List<clsObjectRel> usersOfBookmarks;

        public BookmarkFactory(clsLocalConfig localConfig, clsOntologyItem oItemUser, clsOntologyItem oItemGroup)
        {
            this.localConfig = localConfig;
            this.oItemUser = oItemUser;
            this.oItemGroup = oItemGroup;
        }

        public void SaveDateItems(List<clsObjectRel> bookMarksOfReferenceItems,
            List<clsObjectRel> bookmarksOfMediaItems,
            List<clsObjectAtt> secondsOfBookmarks,  
            List<clsObjectAtt> datetimeCreateOfBookmarks,
            List<clsObjectRel> typesOfBookmarks,    
            List<clsObjectRel> usersOfBookmarks,
            SessionFile sessionFile)
        {

            this.streamWriter = sessionFile.StreamWriter;

            this.bookmarksOfReferenceItems = bookMarksOfReferenceItems;
            this.bookmarksOfMediaItems = bookmarksOfMediaItems;
            this.secondsOfBookmarks = secondsOfBookmarks;
            this.datetimeCreateOfBookmarks = datetimeCreateOfBookmarks;
            this.typesOfBookmarks = typesOfBookmarks;
            this.usersOfBookmarks = usersOfBookmarks;


            if (saveDateitemsAsync != null)
            {
                try
                {
                    saveDateitemsAsync.Abort();
                }
                catch (Exception ex)
                {

                }
            }
            saveDateitemsAsync = new Thread(SaveDateItemsAsync);
            saveDateitemsAsync.Start();

        }

        private void SaveDateItemsAsync()
        {
            var bookmarkItems = (from bookmark in bookmarksOfMediaItems
                                 join second in secondsOfBookmarks on bookmark.ID_Object equals second.ID_Object
                                 join user in usersOfBookmarks.Where(userOfBookMark => userOfBookMark.ID_Other == oItemUser.GUID) on bookmark.ID_Object equals user.ID_Object
                                 join bookMarkType in typesOfBookmarks on bookmark.ID_Object equals bookMarkType.ID_Object
                                 join dateTimeCreate in datetimeCreateOfBookmarks on bookmark.ID_Object equals dateTimeCreate.ID_Object
                                 select new BookmarkViewItem
                                 {
                                     IdMediaItem = bookmark.ID_Other,
                                     NameMediaItem = bookmark.Name_Other,
                                     IdBookmark = bookmark.ID_Object,
                                     NameBookmark = bookmark.Name_Other + " (" + second.Val_Name + ")",
                                     IdAttributeSeconds = second.ID_Attribute,
                                     Seconds = second.Val_Double.Value,
                                     IdUser = user.ID_Other,
                                     NameUser = user.Name_Other,
                                     IdTypeOfBookmark = bookMarkType.ID_Other,
                                     NameTypeOfBookmark = bookMarkType.Name_Other,
                                     IdAttributeDateCreate = dateTimeCreate.ID_Attribute,
                                     DateCreate = dateTimeCreate.Val_Date.Value,

                                 }).OrderByDescending(bookMarkItem => bookMarkItem.DateCreate).ToList();

            var dates = bookmarkItems.GroupBy(bookMarkItem => new { IdMediaItem = bookMarkItem.IdMediaItem, NameMediaItem = bookMarkItem.NameMediaItem, CreateDate = bookMarkItem.DateCreate.Date })
                .OrderByDescending(groupItem => groupItem.Key.CreateDate).Select(dateGroup => new BookmarkTreeItem
            {
                    IdItem = dateGroup.Key.IdMediaItem,
                    LabelItem = dateGroup.Key.NameMediaItem + " (" + dateGroup.Key.CreateDate.ToString("dd.MM.yyyy") + ")",
                    BookMarkDate = dateGroup.Key.CreateDate,
                    IdParent = "-1",
                    IsBookmark = false
            }).ToList();

            List<BookmarkTreeItem> bookmarksToAdd = new List<BookmarkTreeItem>();
            dates.ForEach(dateItem =>
            {
                bookmarksToAdd.AddRange(bookmarkItems.Where(bookmarkItem => bookmarkItem.DateCreate.Date == dateItem.BookMarkDate && bookmarkItem.IdMediaItem == dateItem.IdItem).Select(bookMarkItem => new BookmarkTreeItem
                {
                    IdItem = bookMarkItem.IdBookmark,
                    LabelItem = bookMarkItem.SecondFullString,
                    BookMarkDate = dateItem.BookMarkDate,
                    Seconds = bookMarkItem.Seconds,
                    IsBookmark = true,
                    IdParent = dateItem.IdItem
                }));
                
            });

            dates.AddRange(bookmarksToAdd);

            BookmarkItems = dates;
            var serializerSettings = new Newtonsoft.Json.JsonSerializerSettings();
            serializerSettings.NullValueHandling = NullValueHandling.Ignore;
            var jsonSerialized = JsonConvert.SerializeObject(dates,Formatting.Indented, serializerSettings);
            streamWriter.Write(jsonSerialized);
            streamWriter.Close();

            ResultDateItems = localConfig.Globals.LState_Success.Clone();
        }

        public void StopFactory()
        {
            if (saveDateitemsAsync != null && saveDateitemsAsync.ThreadState == ThreadState.Running)
            {
                saveDateitemsAsync.Abort();
            }
        }
    }
}
