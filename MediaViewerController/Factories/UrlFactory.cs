﻿using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Factories
{
    public class UrlFactory : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        public List<string> GetUrlList(List<clsObjectRel> leftRightUrls,
            List<clsObjectRel> rightLeftUrls)
        {
            var resultList = leftRightUrls.Select(leftRightUrl => leftRightUrl.Name_Other).ToList();

            resultList.AddRange(rightLeftUrls.Select(rightLeftUrl => rightLeftUrl.Name_Object));

            return resultList;
        }

        public UrlFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }
    }
}
