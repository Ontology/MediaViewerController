﻿using MediaViewerController.Models;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MediaViewerController.Factories
{
    public class MediaListFactory : NotifyPropertyChange
    {

        private clsLocalConfig localConfig;

        private Thread getMediaItemListAsync;

        private List<MediaListItem> mediaListItems;
        public List<MediaListItem> MediaListItems
        {
            get { return mediaListItems; }
            set
            {
                mediaListItems = value;
                RaisePropertyChanged(Notifications.NotifyChanges.MediaListFactory_MediaListItems);
            }
        }

        public clsOntologyItem GetMediaItemList(List<clsObjectRel> mediaItemsToRef,
            List<clsObjectRel> mediaItemsToFile,
            List<clsObjectAtt> filesToCreateDate,
            List<clsObjectRel> bookmarksToMediaItems)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var threadParam = new ThreadParam
            {
                MediaItemsToRef = mediaItemsToRef,
                MediaItemsToFiles = mediaItemsToFile,
                FilesToCreatedate = filesToCreateDate,
                BookmarksToMediaItems = bookmarksToMediaItems
            };

            StopReadMediaItemList();

            getMediaItemListAsync = new Thread(GetMediaItemListAsync);
            getMediaItemListAsync.Start(threadParam);

            return result;
        }

        private void GetMediaItemListAsync(object threadParam)
        {
            var paramItem = (ThreadParam)threadParam;

            var rnd = new Random();

            var mediaListItems = (from mediaItemToRef in paramItem.MediaItemsToRef
                              join mediaItemToFile in paramItem.MediaItemsToFiles on mediaItemToRef.ID_Object equals mediaItemToFile.ID_Object
                              join fileToCreateDate in paramItem.FilesToCreatedate on mediaItemToFile.ID_Other equals fileToCreateDate.ID_Object into filesToCreateDates
                              from fileToCreateDate in filesToCreateDates.DefaultIfEmpty()
                              select new MediaListItem
                              {
                                  IdItem = mediaItemToRef.ID_Object,
                                  NameItem = mediaItemToRef.Name_Object,
                                  IdFile = mediaItemToFile.ID_Other,
                                  NameFile = mediaItemToFile.Name_Other,
                                  IdRef = mediaItemToRef.ID_Other,
                                  NameRef = mediaItemToRef.Name_Other,
                                  IdRow = localConfig.Globals.NewGUID,
                                  OrderId = mediaItemToRef.OrderID,
                                  IdAttributeCreated = fileToCreateDate != null ? fileToCreateDate.ID_Attribute : null,
                                  Created = fileToCreateDate != null ? fileToCreateDate.Val_Date : null,
                                  Random = rnd.Next(1, Int32.MaxValue)
                              }).ToList();

            var bookMarkCounts = paramItem.BookmarksToMediaItems.GroupBy(bookMark => new { ID_MediaItem = bookMark.ID_Other, ID_BookMark = bookMark.ID_Object });

            mediaListItems.ForEach(mediaListItem =>
            {
                mediaListItem.BookmarkCount = bookMarkCounts.Where(bookMarkCnt => bookMarkCnt.Key.ID_MediaItem == mediaListItem.IdItem).Count();
            });

            MediaListItems = mediaListItems;
        }

        public void StopReadMediaItemList()
        {
            if (getMediaItemListAsync == null)
            {
                try
                {
                    getMediaItemListAsync.Abort();
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void StopRead()
        {
            StopReadMediaItemList();
        }

        public MediaListFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }
    }

    class ThreadParam
    {
        public List<clsObjectRel> MediaItemsToRef { get; set; }
        public List<clsObjectRel> MediaItemsToFiles { get; set; }
        public List<clsObjectAtt> FilesToCreatedate { get; set; }
        public List<clsObjectRel> BookmarksToMediaItems { get; set; }
    }
}
