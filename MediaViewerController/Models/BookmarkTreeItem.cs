﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Models
{
    public class BookmarkTreeItem
    {
        public string IdItem { get; set; }
        public string LabelItem { get; set; }
        public string IdParent { get; set; }
        public DateTime BookMarkDate { get; set; }
        public double Seconds { get; set; }
        public bool IsBookmark { get; set; }
    }
}
