﻿using MediaViewerController.Attributes;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum MultimediaItemType
{
    Image = 0,
    Audio = 1,
    PDF = 2,
    Video = 4
}
namespace MediaViewerController.Models
{
    public class MultimediaViewItem
    {
        [ImageViewItem(JsonProperty = "id")]
        public string Id { get; set; }

        [ImageViewItem(JsonProperty = "name")]
        public string Name { get; set; }

        [ImageViewItem(JsonProperty = "description")]
        public string Description { get; set; }

        [ImageViewItem(JsonProperty = "multimediaUrl")]
        public string MultimediaUrl
        {
            get
            {
                return SessionFile != null ? SessionFile.FileUri != null ? SessionFile.FileUri.AbsolutePath : null : null;
            }
        }

        [ImageViewItem(JsonProperty = "multimediaAbsoluteUrl")]
        public string MultimediaAbsoluteUrl
        {
            get
            {
                return SessionFile != null ? SessionFile.FileUri != null ? SessionFile.FileUri.AbsoluteUri : null : null;
            }
        }

        [ImageViewItem(JsonProperty = "thumbImageUrl")]
        public string ThumbImageUrl
        {
            get
            {
                return SessionFile != null ? SessionFile.ExtraUrl != null ? SessionFile.ExtraUrl : null : null;
            }
        }

        public string IdFile { get; set; }
        public string NameFile { get; set; }
        public long OrderId { get; set; }
        public long RandomOrderId { get; set; }
        public SessionFile SessionFile { get; set; }
        public MultimediaItemType MultimediaItemType { get; set; }
        public clsOntologyItem OMultimediaItem { get; set; }
    }
}
