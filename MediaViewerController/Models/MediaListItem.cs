﻿using MediaViewerController.Notifications;
using OntologyAppDBConnector.Base;
using OntoMsg_Module.Base;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Models
{
    public class MediaListItem : NotifyPropertyChange
    {
        private string idRow;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = true, IsVisible = false)]
        public string IdRow
        {
            get { return idRow; }
            set
            {
                if (idRow == value) return;

                idRow = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdRow);

            }
        }

        private string idRef;
        [Json()]
        [DataViewColumn(CellType = CellType.String, IsVisible = false)]
        public string IdRef
        {
            get { return idRef; }
            set
            {
                if (idRef == value) return;

                idRef = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdRef);

            }
        }

        private string nameRef;
        [Json()]
        [DataViewColumn(Caption = "Media-Item", CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string NameRef
        {
            get { return nameRef; }
            set
            {
                if (nameRef == value) return;

                nameRef = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameRef);

            }
        }

        private string idItem;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsVisible = false)]
        public string IdItem
        {
            get { return idItem; }
            set
            {
                if (idItem == value) return;

                idItem = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdItem);

            }
        }

        private string idFile;
        [Json()]
        [DataViewColumn(CellType = CellType.String, IsVisible = false)]
        public string IdFile
        {
            get { return idFile; }
            set
            {
                if (idFile == value) return;

                idFile = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdFile);

            }
        }

        private string nameFile;
        [Json()]
        [DataViewColumn(CellType = CellType.String, IsVisible = false)]
        public string NameFile
        {
            get { return nameFile; }
            set
            {
                if (nameFile == value) return;

                nameFile = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameFile);

            }
        }

        private string nameItem;
        [Json()]
		[DataViewColumn(Caption = "Media-Item", CellType = CellType.String, DisplayOrder = 0, IsIdField = false, IsVisible = true)]
        public string NameItem
        {
            get { return nameItem; }
            set
            {
                if (nameItem == value) return;

                nameItem = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameItem);

            }
        }

        private string idAttributeCreated;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdAttributeCreated
        {
            get { return idAttributeCreated; }
            set
            {
                if (idAttributeCreated == value) return;

                idAttributeCreated = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdAttributeCreated);

            }
        }

        private DateTime? created;
        [Json()]
		[DataViewColumn(Caption = "Created", CellType = CellType.Date, DisplayOrder = 1, IsIdField = false, IsVisible = true, CellsFormat = "dd.MM.yyyy hh:mm:ss")]
        public DateTime? Created
        {
            get { return created; }
            set
            {
                if (created == value) return;

                created = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Created);

            }
        }

        private long? orderId;
        [Json()]
		[DataViewColumn(Caption = "Order-Id", CellType = CellType.Integer, DisplayOrder = 2, IsIdField = false, IsVisible = true, CellsFormat = "n")]
        public long? OrderId
        {
            get { return orderId; }
            set
            {
                if (orderId == value) return;

                orderId = value;

                RaisePropertyChanged(NotifyChanges.DataModel_OrderId);

            }
        }

        private long? bookmarkCount;
        [Json()]
		[DataViewColumn(Caption = "Bookmarked", CellType = CellType.Integer, DisplayOrder = 3, IsIdField = false, IsVisible = true, CellsFormat = "n")]
        public long? BookmarkCount
        {
            get { return bookmarkCount; }
            set
            {
                if (bookmarkCount == value) return;

                bookmarkCount = value;

                RaisePropertyChanged(NotifyChanges.DataModel_BookmarkCount);

            }
        }

        private long? random;
        [Json()]
		[DataViewColumn(Caption = "Random-Id", CellType = CellType.Integer, DisplayOrder = 4, IsIdField = false, IsVisible = true, CellsFormat = "n")]
        public long? Random
        {
            get { return random; }
            set
            {
                if (random == value) return;

                random = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Random);

            }
        }
    }
}
