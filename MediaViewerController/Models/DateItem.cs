﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Models
{
    public class DateItem
    {
        public DateTime DateOfBookmarks { get; set; }
        public string DateString
        {
            get
            {
                return DateOfBookmarks.ToString("dd.MM.yyyy");
            }
        }
        public List<BookmarkViewItem> BookmarkItems { get; set; }
    }
}
