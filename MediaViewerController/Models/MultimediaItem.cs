﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Models
{
    public class MultimediaItem
    {
        public string IdMultimediaItem { get; set;  }
        public string NameMultimediaItem { get; set; }
        public string IdClassMultimediaItem { get; set; }
        public string IdFile { get; set; }
        public string NameFile { get; set; }
        public DateTime FileCreateDate { get; set; }
        public string IdRef { get; set; }
        public string NameRef { get; set; }
    }
}
