﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Models
{
    public class BookmarkViewItem
    {
        public string IdMediaItem { get; set; }
        public string NameMediaItem { get; set; }
        public string IdBookmark { get; set; }
        public string NameBookmark { get; set; }
        public string IdTypeOfBookmark { get; set; }
        public string NameTypeOfBookmark { get; set; }
        public string IdUser { get; set; }
        public string NameUser { get; set; }
        public string IdAttributeSeconds { get; set; }
        public double Seconds { get; set; }
        public string IdAttributeDateCreate { get; set; }
        public DateTime DateCreate { get; set; }
        public string DateString
        {
            get
            {
                return DateCreate.ToString("HH:mm:ss");
            }
        }
        public string SecondFullString
        {
            get
            {
                return Math.Floor(Seconds).ToString() + " s";
            }
        }

    
    }
}
