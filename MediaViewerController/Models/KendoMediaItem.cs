﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Models
{
    public class KendoMediaItem
    {
        public string idMediaItem { get; set; }
        public string title { get; set; }
        public string poster { get; set; }
        public string source { get; set; }
        public string createDate
        {
            get
            {
                return CreateDate != null ? CreateDate.ToString() : "-";
            }
        }
        public DateTime? CreateDate { get; set; }
    }
}
