﻿using MediaViewerController.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MediaViewerController.Services
{
    public class MediaServiceAgent_Elastic : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private object serviceLocker = new object();

        private clsRelationConfig relationConfig;
        private clsTransaction transactionManager;
        private OntologyModDBConnector dbReaderRefToMediaItem;
        private OntologyModDBConnector dbReaderMediaAttirbutes;
        private OntologyModDBConnector dbReaderMediaFiles;
        private OntologyModDBConnector dbReaderOitems;

        private Thread searchMediaItemsAsync;

        public List<clsObjectRel> RefToMediafiles
        {
            get
            {
                return dbReaderRefToMediaItem.ObjectRels;
            }
        }

        public List<clsObjectAtt> MediaAttributes
        {
            get
            {
                return dbReaderMediaAttirbutes.ObjAtts;
            }
        
        }

        public List<clsObjectRel> MediaFiles
        {
            get
            {
                return dbReaderMediaFiles.ObjectRels;
            }
        }

        private clsOntologyItem resultData;
        public clsOntologyItem ResultData
        {
            get { return resultData; }
            set
            {
                resultData = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultData);
            }
        }

        private ResultMultimediaItem resultMultimediaItem;
        public ResultMultimediaItem ResultMultimediaItem
        {
            get
            {
                lock(serviceLocker)
                {
                    return resultMultimediaItem;
                }
                
            }
            set
            {
                resultMultimediaItem = value;
                RaisePropertyChanged(nameof(ResultMultimediaItem));
            }
        }

        private clsOntologyItem oItemReference;

        public void GetReferencedMediaItems(clsOntologyItem oItemReference)
        {
            this.oItemReference = oItemReference;
            
            try
            {
                if (searchMediaItemsAsync != null)
                {
                    searchMediaItemsAsync.Abort();
                }
            }
            catch(Exception ex)
            {

            }

            searchMediaItemsAsync = new Thread(SearchMediaItemsAsync);
            searchMediaItemsAsync.Start();

        }

        public async Task<ResultMultimediaItem> GetDataOfMultimediaItem(clsOntologyItem oMultimediaItem)
        {
            var result = new ResultMultimediaItem
            {
                OMultimediaItem = oMultimediaItem,
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchAttributes = new List<clsObjectAtt> { new clsObjectAtt
                {
                    ID_Object = oMultimediaItem.GUID,
                    ID_AttributeType = localConfig.OItem_attribute_id.GUID
                },
                new clsObjectAtt {
                    ID_Object = oMultimediaItem.GUID,
                    ID_AttributeType = localConfig.OItem_attribute_taking.GUID
                }
            };


            var dbReaderAttributes = new OntologyModDBConnector(localConfig.Globals);

            result.Result = dbReaderAttributes.GetDataObjectAtt(searchAttributes);

            if (result.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultMultimediaItem = result;
                return result;
            }

            result.MediaAttributes = dbReaderAttributes.ObjAtts;

            var searchAudioItems = new List<clsObjectRel> { new  clsObjectRel
                {
                    ID_Object = oMultimediaItem.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_belonging_source.GUID,
                    ID_Parent_Other = localConfig.OItem_type_file.GUID
                }
            };

            var dbReaderFiles = new OntologyModDBConnector(localConfig.Globals);

            result.Result = dbReaderFiles.GetDataObjectRel(searchAudioItems);

            if (result.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultMultimediaItem = result;
                return result;
            }

            result.MediaRels = dbReaderFiles.ObjectRels;

            ResultMultimediaItem = result;
            return result;
        }

        private void SearchMediaItemsAsync()
        {
            
            var result = Search_001_RefToMediaItem();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultData = result;
                return;
            }


            result = Search_002_Attributes();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultData = result;
                return;
            }

            

            result = Search_003_Files();

            ResultData = result;
        }

        private clsOntologyItem Search_001_RefToMediaItem()
        {
            var searchImages = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = oItemReference.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                    ID_Parent_Object = localConfig.OItem_type_media_item.GUID
                }
            };

            

            var result = dbReaderRefToMediaItem.GetDataObjectRel(searchImages);

            return result;
        }

        private clsOntologyItem Search_002_Attributes()
        {
            var searchAttributes = dbReaderRefToMediaItem.ObjectRels.Select(refToImage => new clsObjectAtt
            {
                ID_Object = refToImage.ID_Object,
                ID_AttributeType = localConfig.OItem_attribute_id.GUID
            }).ToList();

            if (searchAttributes.Any())
            {
                var result = dbReaderMediaAttirbutes.GetDataObjectAtt(searchAttributes);
                return result;
            }
            else
            {
                return localConfig.Globals.LState_Success.Clone();
            }
        }

        private clsOntologyItem Search_003_Files()
        {
            var searchAudioItems = dbReaderRefToMediaItem.ObjectRels.Select(refToMedia => new clsObjectRel
            {
                ID_Object = refToMedia.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_belonging_source.GUID,
                ID_Parent_Other = localConfig.OItem_type_file.GUID
            }).ToList();


            if (searchAudioItems.Any())
            {
                var result = dbReaderMediaFiles.GetDataObjectRel(searchAudioItems);
                return result;
            }
            else
            {
                return localConfig.Globals.LState_Success.Clone();
            }

            

            
        }

        public clsOntologyItem SavePos(double pos, clsOntologyItem mediaItem, clsOntologyItem oItemUser, clsOntologyItem referenceItem, clsOntologyItem logState)
        {
            var nameItem = mediaItem.Name;
            var posString = pos.ToString();
            if (posString.Length + nameItem.Length>255)
            {
                nameItem = mediaItem.Name.Substring(0, 254 - posString.Length) + posString;
            }
            else
            {
                nameItem += posString;
            }

            var saveBookMark = new clsOntologyItem
            {
                GUID = localConfig.Globals.NewGUID,
                Name = nameItem,
                GUID_Parent = localConfig.OItem_type_media_item_bookmark.GUID,
                Type = localConfig.Globals.Type_Object
            };

            var result = transactionManager.do_Transaction(saveBookMark);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;

            var saveMediaItemToBookmark = relationConfig.Rel_ObjectRelation(saveBookMark, mediaItem, localConfig.OItem_relationtype_belongsto);
            result = transactionManager.do_Transaction(saveMediaItemToBookmark);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                transactionManager.rollback();
                return result;
            }

            var saveSeconds = relationConfig.Rel_ObjectAttribute(saveBookMark, localConfig.OItem_attributetype_second, pos);
            result = transactionManager.do_Transaction(saveSeconds);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                transactionManager.rollback();
                return result;
            }

            var saveUser = relationConfig.Rel_ObjectRelation(saveBookMark, oItemUser, localConfig.OItem_relationtype_belongsto);
            result = transactionManager.do_Transaction(saveUser);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                transactionManager.rollback();
                return result;
            }

            var saveState = relationConfig.Rel_ObjectRelation(saveBookMark, logState, localConfig.OItem_relationtype_is_of_type);
            result = transactionManager.do_Transaction(saveState);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                transactionManager.rollback();
                return result;
            }

            var timeStamp = DateTime.Now;
            var saveTimeStamp = relationConfig.Rel_ObjectAttribute(saveBookMark, localConfig.OItem_attribute_datetimestamp__create_, timeStamp);
            result = transactionManager.do_Transaction(saveTimeStamp);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                transactionManager.rollback();
                return result;
            }

            var saveBookMarkToReference = relationConfig.Rel_ObjectRelation(saveBookMark, referenceItem, localConfig.OItem_relationtype_belonging_source);

            result = transactionManager.do_Transaction(saveBookMarkToReference);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                transactionManager.rollback();
                return result;
            }

            result.Val_Date = timeStamp;
            return result;
        }

        public clsOntologyItem GetOItem(string idItem, string typeItem)
        {
            return dbReaderOitems.GetOItem(idItem, typeItem);
        }

        public MediaServiceAgent_Elastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            dbReaderRefToMediaItem = new OntologyModDBConnector(localConfig.Globals);
            dbReaderMediaAttirbutes = new OntologyModDBConnector(localConfig.Globals);
            dbReaderMediaFiles = new OntologyModDBConnector(localConfig.Globals);
            dbReaderOitems = new OntologyModDBConnector(localConfig.Globals);
            relationConfig = new clsRelationConfig(localConfig.Globals);
            transactionManager = new clsTransaction(localConfig.Globals);
        }
    }

    public class ResultMultimediaItem
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem OMultimediaItem { get; set; }
        public List<clsObjectAtt> MediaAttributes { get; set; }
        public List<clsObjectRel> MediaRels { get; set; }
    }
}
