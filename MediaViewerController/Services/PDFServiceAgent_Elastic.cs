﻿using MediaViewerController.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MediaViewerController.Services
{
    public class PDFServiceAgent_Elastic : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private OntologyModDBConnector dbReaderRefToMediaItem;
        private OntologyModDBConnector dbReaderMediaAttirbutes;
        private OntologyModDBConnector dbReaderMediaFiles;
        private OntologyModDBConnector dbReaderOitems;

        private Thread searchMediaItemsAsync;

        public List<clsObjectRel> RefToMediafiles
        {
            get
            {
                return dbReaderRefToMediaItem.ObjectRels;
            }
        }

        public List<clsObjectAtt> MediaAttributes
        {
            get
            {
                return dbReaderMediaAttirbutes.ObjAtts;
            }
        
        }

        public List<clsObjectRel> MediaFiles
        {
            get
            {
                return dbReaderMediaFiles.ObjectRels;
            }
        }

        private clsOntologyItem resultData;
        public clsOntologyItem ResultData
        {
            get { return resultData; }
            set
            {
                resultData = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultData);
            }
        }

        private clsOntologyItem oItemReference;

        public void GetReferencedMediaItems(clsOntologyItem oItemReference)
        {
            this.oItemReference = oItemReference;
            
            try
            {
                if (searchMediaItemsAsync != null)
                {
                    searchMediaItemsAsync.Abort();
                }
            }
            catch(Exception ex)
            {

            }

            searchMediaItemsAsync = new Thread(SearchMediaItemsAsync);
            searchMediaItemsAsync.Start();

        }

        private void SearchMediaItemsAsync()
        {
            
            var result = Search_001_RefToMediaItem();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultData = result;
                return;
            }


            result = Search_002_Attributes();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultData = result;
                return;
            }

            

            result = Search_003_Files();

            ResultData = result;
        }

        private clsOntologyItem Search_001_RefToMediaItem()
        {
            var searchImages = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = oItemReference.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                    ID_Parent_Object = localConfig.OItem_type_pdf_documents.GUID
                }
            };

            

            var result = dbReaderRefToMediaItem.GetDataObjectRel(searchImages);

            return result;
        }

        private clsOntologyItem Search_002_Attributes()
        {
            var searchAttributes = dbReaderRefToMediaItem.ObjectRels.Select(refToImage => new clsObjectAtt
            {
                ID_Object = refToImage.ID_Object,
                ID_AttributeType = localConfig.OItem_attribute_id.GUID
            }).ToList();

            if (searchAttributes.Any())
            {
                var result = dbReaderMediaAttirbutes.GetDataObjectAtt(searchAttributes);
                return result;
            }
            else
            {
                return localConfig.Globals.LState_Success.Clone();
            }
        }

        private clsOntologyItem Search_003_Files()
        {
            var searchAudioItems = dbReaderRefToMediaItem.ObjectRels.Select(refToMedia => new clsObjectRel
            {
                ID_Object = refToMedia.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_belonging_source.GUID,
                ID_Parent_Other = localConfig.OItem_type_file.GUID
            }).ToList();


            if (searchAudioItems.Any())
            {
                var result = dbReaderMediaFiles.GetDataObjectRel(searchAudioItems);
                return result;
            }
            else
            {
                return localConfig.Globals.LState_Success.Clone();
            }

            

            
        }

        public clsOntologyItem GetOItem(string idItem, string typeItem)
        {
            return dbReaderOitems.GetOItem(idItem, typeItem);
        }

        public PDFServiceAgent_Elastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            dbReaderRefToMediaItem = new OntologyModDBConnector(localConfig.Globals);
            dbReaderMediaAttirbutes = new OntologyModDBConnector(localConfig.Globals);
            dbReaderMediaFiles = new OntologyModDBConnector(localConfig.Globals);
            dbReaderOitems = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
