﻿using MediaViewerController.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MediaViewerController.Services
{
    public class BookmarkServiceAgent_Elastic : NotifyPropertyChange
    {

        private clsLocalConfig localConfig;

        private Thread bookmarkReaderAsync;

        private OntologyModDBConnector dbReaderBookmarksOfReference;
        private OntologyModDBConnector dbReaderBookmarksOfMediaItems;
        private OntologyModDBConnector dbReaderUsersAndTypeOfBookmarks;
        private OntologyModDBConnector dbReaderAttributes;
        private OntologyModDBConnector dbReaderOitems;

        private clsOntologyItem referencedItem;

        public List<clsObjectRel> BookmarksOfReferences
        {
            get { return dbReaderBookmarksOfReference.ObjectRels; }
        }
        public List<clsObjectRel>  BookmarksOfMediaItems
        {
            get { return dbReaderBookmarksOfMediaItems.ObjectRels; }
        }
        public List<clsObjectAtt> SecondsOfBookmarks
        {
            get { return dbReaderAttributes.ObjAtts.Where(objAtt => objAtt.ID_AttributeType == localConfig.OItem_attributetype_second.GUID).ToList(); }
        }
        public List<clsObjectAtt> DateTimeStampsOfBookmarks
        {
            get
            {
                return dbReaderAttributes.ObjAtts.Where(objAtt => objAtt.ID_AttributeType == localConfig.OItem_attribute_datetimestamp__create_.GUID).ToList();
            }
        }
        public List<clsObjectRel> TypesOfBookmarks
        {
            get
            {
                return dbReaderUsersAndTypeOfBookmarks.ObjectRels.Where(relItem => relItem.ID_Parent_Other == localConfig.OItem_class_logstate.GUID).ToList();
            }
        }
        public List<clsObjectRel> UsersOfBookmarks
        {
            get
            {
                return dbReaderUsersAndTypeOfBookmarks.ObjectRels.Where(relItem => relItem.ID_Parent_Other == localConfig.OItem_type_user.GUID).ToList();
            }
        }

        private clsOntologyItem resultData;
        public clsOntologyItem ResultData
        {
            get { return resultData; }
            set
            {
                resultData = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultData);
            }
        }

        public clsOntologyItem GetBookmarkItems(clsOntologyItem referenceItem)
        {
            this.referencedItem = referenceItem;

            if (bookmarkReaderAsync != null)
            {
                try
                {
                    bookmarkReaderAsync.Abort();

                }
                catch(Exception ex)
                {

                }
            }
            if (referencedItem == null) return localConfig.Globals.LState_Nothing.Clone();
            bookmarkReaderAsync = new Thread(GetBookmarkItemsAsync);
            bookmarkReaderAsync.Start();

            return localConfig.Globals.LState_Success.Clone();
        }

        private void GetBookmarkItemsAsync()
        {
            var result = GetData_001_Bookmarks();
            
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultData = result;
                return;
            }

            result = GetData_002_MediaItemsOfBookmarks();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultData = result;
                return;
            }

            result = GetData_003_Attributes();
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultData = result;
                return;
            }

            result = GetData_004_UsersAndTypesOfBookmarks();
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultData = result;
                return;
            }


            ResultData = result;
        }

        private clsOntologyItem GetData_001_Bookmarks()
        {
            var searchMediaItems = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = referencedItem.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_belonging_source.GUID,
                    ID_Parent_Object = localConfig.OItem_type_media_item_bookmark.GUID
                }
            };

            var result = dbReaderBookmarksOfReference.GetDataObjectRel(searchMediaItems);

            return result;
        }

        private clsOntologyItem GetData_002_MediaItemsOfBookmarks()
        {
            var searchMediaItems = dbReaderBookmarksOfReference.ObjectRels.Select(mediaRel => new clsObjectRel
            {
                ID_Object = mediaRel.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                ID_Parent_Other = localConfig.OItem_type_media_item.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchMediaItems.Any())
            {
                result = dbReaderBookmarksOfMediaItems.GetDataObjectRel(searchMediaItems);
            }
            else
            {
                dbReaderBookmarksOfMediaItems.ObjectRels.Clear();
            }

            return result;
        }

        private clsOntologyItem GetData_003_Attributes()
        {
            var searchAttributes = dbReaderBookmarksOfReference.ObjectRels.Select(bookMark => new clsObjectAtt
            {
                ID_Object = bookMark.ID_Object
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchAttributes.Any())
            {
                result = dbReaderAttributes.GetDataObjectAtt(searchAttributes);
            }
            else
            {
                dbReaderAttributes.ObjAtts.Clear();
            }

            return result;
        }
        
        private clsOntologyItem GetData_004_UsersAndTypesOfBookmarks()
        {
            var searchUsersAndTypes = dbReaderBookmarksOfMediaItems.ObjectRels.Select(bookMark => new clsObjectRel
            {
                ID_Object = bookMark.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                ID_Parent_Other = localConfig.OItem_type_user.GUID
            }).ToList();

            searchUsersAndTypes.AddRange(dbReaderBookmarksOfMediaItems.ObjectRels.Select(bookMark => new clsObjectRel
            {
                ID_Object = bookMark.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_is_of_type.GUID,
                ID_Parent_Other = localConfig.OItem_class_logstate.GUID
            }));

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchUsersAndTypes.Any())
            {
                result = dbReaderUsersAndTypeOfBookmarks.GetDataObjectRel(searchUsersAndTypes);
            }
            else
            {
                dbReaderUsersAndTypeOfBookmarks.ObjectRels.Clear();
            }

            return result;

        }

        public clsOntologyItem GetOItem(string idItem, string typeItem)
        {
            return dbReaderOitems.GetOItem(idItem, typeItem);
        }


        public BookmarkServiceAgent_Elastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            dbReaderBookmarksOfReference = new OntologyModDBConnector(localConfig.Globals);
            dbReaderBookmarksOfMediaItems = new OntologyModDBConnector(localConfig.Globals);
            dbReaderUsersAndTypeOfBookmarks = new OntologyModDBConnector(localConfig.Globals);
            dbReaderOitems = new OntologyModDBConnector(localConfig.Globals);
            dbReaderAttributes = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
