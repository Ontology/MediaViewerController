﻿using MediaViewerController.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MediaViewerController.Services
{
    public class MediaListServiceAgent : NotifyPropertyChange
    {
        
        private clsLocalConfig localConfig;

        private Thread getMediaItemListAsync;

        private clsOntologyItem resultMediaItemList;
        public clsOntologyItem ResultMediaItemList
        {
            get
            {
                return resultMediaItemList;
            }
            set
            {
                resultMediaItemList = value;
                RaisePropertyChanged(NotifyChanges.ElasticService_ResultMediaItemList);
            }
        }

        public List<clsObjectRel> MediaItemsToRef
        {
            get; private set;
        }

        public List<clsObjectRel> MediaItemsToFiles
        {
            get; private set;
        }

        public List<clsObjectAtt> FilesToCreateDate
        {
            get; private set;
        }

        public List<clsObjectRel> BookmarksToMediaItems
        {
            get; private set;
        }

        private clsOntologyItem mediaItemType;

        public MediaListServiceAgent(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            return dbReader.GetOItem(id, type);
        }

        public clsOntologyItem GetMediaItemList(clsOntologyItem refItem, clsOntologyItem mediaItemType)
        {
            this.mediaItemType = mediaItemType;

            StopReadMediaList();

            getMediaItemListAsync = new Thread(GetMediaItemListAsync);
            getMediaItemListAsync.Start(refItem);

            return localConfig.Globals.LState_Success.Clone();
        }

        private void GetMediaItemListAsync(object item)
        {
            var refItem = (clsOntologyItem)item;


            var result = localConfig.Globals.LState_Success.Clone();   

            if (mediaItemType.GUID == localConfig.OItem_type_images__graphic_.GUID)
            {
                result = Get_001_ImageItemList(refItem);
            }
            else if (mediaItemType.GUID == localConfig.OItem_type_media_item.GUID)
            {
                result = Get_001_MediaItemList(refItem);
            }
            else if (mediaItemType.GUID == localConfig.OItem_type_pdf_documents.GUID)
            {
                result = Get_001_PdfItemList(refItem);
            }
            else
            {
                ResultMediaItemList = localConfig.Globals.LState_Error.Clone();
            }

            

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultMediaItemList = result;
                return;
            }

            result = Get_002_Files();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultMediaItemList = result;
                return;
            }

            result = Get_003_FilesToCreateDate();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultMediaItemList = result;
                return;
            }

            result = Get_004_BookmarksToMediaItems();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultMediaItemList = result;
                return;
            }

            ResultMediaItemList = result;
        }

        private clsOntologyItem Get_001_MediaItemList(clsOntologyItem refItem)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchMediaItems = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = refItem.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                    ID_Parent_Object = localConfig.OItem_type_media_item.GUID
                }
            };

            var result = dbReader.GetDataObjectRel(searchMediaItems);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                MediaItemsToRef = dbReader.ObjectRels;
            }
            else
            {
                MediaItemsToRef = null;
            }
            return result;
        }

        private clsOntologyItem Get_001_ImageItemList(clsOntologyItem refItem)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchMediaItems = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = refItem.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                    ID_Parent_Object = localConfig.OItem_type_images__graphic_.GUID
                }
            };

            var result = dbReader.GetDataObjectRel(searchMediaItems);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                MediaItemsToRef = dbReader.ObjectRels;
            }
            else
            {
                MediaItemsToRef = null;
            }
            return result;
        }

        private clsOntologyItem Get_001_PdfItemList(clsOntologyItem refItem)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchMediaItems = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = refItem.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                    ID_Parent_Object = localConfig.OItem_type_pdf_documents.GUID
                }
            };

            var result = dbReader.GetDataObjectRel(searchMediaItems);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                MediaItemsToRef = dbReader.ObjectRels;
            }
            else
            {
                MediaItemsToRef = null;
            }
            return result;
        }

        private clsOntologyItem Get_002_Files()
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchFiles = MediaItemsToRef.Select(medItmToRef => new clsObjectRel
            {
                ID_Object = medItmToRef.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_belonging_source.GUID,
                ID_Parent_Other = localConfig.OItem_type_file.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();

            if (searchFiles.Any())
            {
                result = dbReader.GetDataObjectRel(searchFiles);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    MediaItemsToFiles = dbReader.ObjectRels;
                }
                else
                {
                    MediaItemsToFiles = null;
                }
            }
            else
            {
                MediaItemsToFiles = new List<clsObjectRel>();
            }
            

            return result;
        }

        private clsOntologyItem Get_003_FilesToCreateDate()
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var result = localConfig.Globals.LState_Success.Clone();

            var searchFilesToCreateDate = MediaItemsToFiles.Select(medToFile => new clsObjectAtt
            {
                ID_Object = medToFile.ID_Other,
                ID_AttributeType = localConfig.OItem_attribute_datetimestamp__create_.GUID
            }).ToList();

            if (searchFilesToCreateDate.Any())
            {
                result = dbReader.GetDataObjectAtt(searchFilesToCreateDate);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    FilesToCreateDate = dbReader.ObjAtts;
                }
                else
                {
                    FilesToCreateDate = null;
                }
            }
            else
            {
                FilesToCreateDate = new List<clsObjectAtt>();
            }

            return result;
        }

        private clsOntologyItem Get_004_BookmarksToMediaItems()
        {
            var dbReader1 = new OntologyModDBConnector(localConfig.Globals);
            var dbReader2 = new OntologyModDBConnector(localConfig.Globals);

            var result = localConfig.Globals.LState_Success.Clone();

            var searchStart = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Parent_Object = localConfig.OItem_type_media_item_bookmark.GUID,
                    ID_Other = localConfig.OItem_token_logstate_start.GUID
                }
            };

            result = dbReader1.GetDataObjectRel(searchStart, doIds: true);

            var searchBookmarksToMediaItems = dbReader1.ObjectRelsId.Select(bookMarks => new clsObjectRel
            {
                ID_Object = bookMarks.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                ID_Parent_Other = localConfig.OItem_type_media_item.GUID
            }).ToList();

            if (searchBookmarksToMediaItems.Any())
            {
                result = dbReader2.GetDataObjectRel(searchBookmarksToMediaItems, doIds: true);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    BookmarksToMediaItems = (from mediaItems in MediaItemsToRef
                                             join mediaItemToBookmarks in dbReader2.ObjectRelsId on mediaItems.ID_Object equals mediaItemToBookmarks.ID_Other
                                             join bookMarks in dbReader1.ObjectRelsId on mediaItemToBookmarks.ID_Object equals bookMarks.ID_Object
                                             select mediaItemToBookmarks).ToList();
                }
                else
                {
                    BookmarksToMediaItems = null;
                }
            }
            else
            {
                BookmarksToMediaItems = new List<clsObjectRel>();
            }

            return result;
        }

        private void StopReadMediaList()
        {
            if (getMediaItemListAsync == null) return;

            try
            {
                getMediaItemListAsync.Abort();
            }
            catch(Exception ex)
            {

            }
        }

        public void StopRead()
        {
            StopReadMediaList();
        }

        private void Initialize()
        {

        }
    }
}
