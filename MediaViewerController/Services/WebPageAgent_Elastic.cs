﻿using MediaViewerController.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MediaViewerController.Services
{
    public class WebPageAgent_Elastic : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private OntologyModDBConnector dbReader_LeftRight;
        private OntologyModDBConnector dbReader_RightLeft;
        private OntologyModDBConnector dbReaderOitems;

        private Thread getUrlsAsync;

        private string idRef;

        private clsOntologyItem resultData;
        public clsOntologyItem ResultData
        {
            get { return resultData; }
            set
            {
                resultData = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultData);
            }
        }

        public List<clsObjectRel> LeftRightUrls
        {
            get
            {
                return dbReader_LeftRight.ObjectRels;
            }
        }

        public List<clsObjectRel> RightLeftUrls
        {
            get
            {
                return dbReader_RightLeft.ObjectRels;
            }
        }

        public clsOntologyItem GetDataUrls(string idRef)
        {
            try
            {
                if (getUrlsAsync != null)
                {
                    getUrlsAsync.Abort();
                }
            }
            catch (Exception ex)
            {

            }
            this.idRef = idRef;
            getUrlsAsync = new Thread(GetUrlsAsync);
            getUrlsAsync.Start();

            return localConfig.Globals.LState_Success.Clone();
        }

        private void GetUrlsAsync()
        {
            var result = Search_001_LeftRight();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultData = result;
            }

            result = Search_002_RightLeft();

            ResultData = result;
        }

        private clsOntologyItem Search_001_LeftRight()
        {
            var searchUrlsLeftRight = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = idRef,
                    ID_Parent_Other = localConfig.OItem_type_url.GUID
                }
            };

            var result = dbReader_LeftRight.GetDataObjectRel(searchUrlsLeftRight);

            return result;
        }

        private clsOntologyItem Search_002_RightLeft()
        {
            var searchUrlsRightLeft = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = idRef,
                    ID_Parent_Object = localConfig.OItem_type_url.GUID
                }
            };

            var result = dbReader_RightLeft.GetDataObjectRel(searchUrlsRightLeft);

            return result;
        }


        public clsOntologyItem GetOItem(string idItem, string typeItem)
        {
            return dbReaderOitems.GetOItem(idItem, typeItem);
        }

        public WebPageAgent_Elastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;

            Initialize();
        }

        private void Initialize()
        {
            dbReader_LeftRight = new OntologyModDBConnector(localConfig.Globals);
            dbReader_RightLeft = new OntologyModDBConnector(localConfig.Globals);
            dbReaderOitems = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
