﻿using MediaViewerController.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MediaViewerController.Services
{
    public class ImageServiceAgent_Elastic : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private OntologyModDBConnector dbReaderRefToImages;
        private OntologyModDBConnector dbReaderImageAttirbutes;
        private OntologyModDBConnector dbReaderImageFiles;
        private OntologyModDBConnector dbReaderOitems;

        private Thread searchImagesAsync;



        public List<clsObjectRel> RefToImages
        {
            get
            {
                return dbReaderRefToImages.ObjectRels;
            }
        }

        public List<clsObjectAtt> ImageAttributes
        {
            get
            {
                return dbReaderImageAttirbutes.ObjAtts;
            }
        
        }

        public List<clsObjectRel> ImageFiles
        {
            get
            {
                return dbReaderImageFiles.ObjectRels;
            }
        }

        private clsOntologyItem resultData;
        public clsOntologyItem ResultData
        {
            get { return resultData; }
            set
            {
                resultData = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultData);
            }
        }

        private clsOntologyItem oItemReference;

        public void GetReferencedImages(clsOntologyItem oItemReference)
        {
            this.oItemReference = oItemReference;
            
            try
            {
                if (searchImagesAsync != null)
                {
                    searchImagesAsync.Abort();
                }
            }
            catch(Exception ex)
            {

            }

            searchImagesAsync = new Thread(SearchImagesAsync);
            searchImagesAsync.Start();

        }

        private void SearchImagesAsync()
        {
            
            var result = Search_001_RefToImageItem();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultData = result;
                return;
            }


            result = Search_002_Attributes();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultData = result;
                return;
            }

            

            result = Search_003_Files();

            ResultData = result;
        }

        private clsOntologyItem Search_001_RefToImageItem()
        {
            var result = localConfig.Globals.LState_Success.Clone();
            if (oItemReference.GUID_Parent == localConfig.OItem_type_images__graphic_.GUID)
            {
                dbReaderRefToImages.ObjectRels = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = oItemReference.GUID,
                        Name_Object = oItemReference.Name,
                        ID_Parent_Object = oItemReference.GUID_Parent,
                        OrderID = 1
                    }
                };
            }
            else
            {
                var searchImages = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = oItemReference.GUID,
                        ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                        ID_Parent_Object = localConfig.OItem_type_images__graphic_.GUID
                    }
                };



                result = dbReaderRefToImages.GetDataObjectRel(searchImages);
            }
            

            return result;
        }

        private clsOntologyItem Search_002_Attributes()
        {
            var searchAttributes = dbReaderRefToImages.ObjectRels.Select(refToImage => new clsObjectAtt
            {
                ID_Object = refToImage.ID_Object,
                ID_AttributeType = localConfig.OItem_attribute_id.GUID
            }).ToList();

            searchAttributes.AddRange(dbReaderRefToImages.ObjectRels.Select(refToImage => new clsObjectAtt
            {
                ID_Object = refToImage.ID_Object,
                ID_AttributeType = localConfig.OItem_attribute_taking.GUID
            }));

            if (searchAttributes.Any())
            {
                var result = dbReaderImageAttirbutes.GetDataObjectAtt(searchAttributes);
                return result;
            }
            else
            {
                return localConfig.Globals.LState_Success.Clone();
            }
        }

        private clsOntologyItem Search_003_Files()
        {
            var searchImages = dbReaderRefToImages.ObjectRels.Select(refToImage => new clsObjectRel
            {
                ID_Object = refToImage.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_belonging_source.GUID,
                ID_Parent_Other = localConfig.OItem_type_file.GUID
            }).ToList();


            if (searchImages.Any())
            {
                var result = dbReaderImageFiles.GetDataObjectRel(searchImages);
                return result;
            }
            else
            {
                return localConfig.Globals.LState_Success.Clone();
            }

            

            
        }
        public clsOntologyItem GetOItem(string idItem, string typeItem)
        {
            return dbReaderOitems.GetOItem(idItem, typeItem);
        }

        public ImageServiceAgent_Elastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            dbReaderRefToImages = new OntologyModDBConnector(localConfig.Globals);
            dbReaderImageAttirbutes = new OntologyModDBConnector(localConfig.Globals);
            dbReaderImageFiles = new OntologyModDBConnector(localConfig.Globals);
            dbReaderOitems = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
