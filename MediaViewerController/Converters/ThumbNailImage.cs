﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Converters
{
    public class ThumbNailImage
    {

        public bool ThumbnailCallback()
        {
            return false;
        }
        public Image GetThumbNailOfImage(Image image, int width, int height)
        {
            Bitmap myThumbnail = new Bitmap(width, height);

            using (Graphics g = Graphics.FromImage(myThumbnail))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(image, new Rectangle(0, 0, width, height));
            }

            //using (var ms = new MemoryStream())
            //{
            //    BitmapExtensions.SaveJPG100(myThumbnail, ms);
            //    var imageThumbNail = Image.FromStream(ms);
            //    return myThumbnail;
            //}

            return myThumbnail;
        }

        public Image GetThumbNailOfImageByMaxWidth(Image image, int maxWidth)
        {
            double width = image.Width;
            double height = image.Height;

            if (maxWidth < width)
            {
                var percent = (100 / width * maxWidth);
                return GetThumbNailOfImageByPercent(image, percent);
            }
            else
            {
                return image;
            }

            
        }

        public Image GetThumbNailOfImageByMax(Image image, int max)
        {
            double width = image.Width;
            double height = image.Height;

            var percent = 100.0;
            var widthPercent = 100.0;
            var heightPercent = 100.0;
            if (max < width)
            {
                widthPercent = (100 / width * max);
            }

            if (max < height)
            {
                heightPercent = (100 / height * max);
            }

            if (widthPercent < heightPercent)
            {
                percent = widthPercent;
            }
            else if (heightPercent < widthPercent)
            {
                percent = heightPercent;
            }

            return GetThumbNailOfImageByPercent(image, percent);
        }

        public Image GetThumbNailOfImageByPercent(Image image, double percent)
        {
            double width = image.Width;
            double height = image.Height;

            var newWidth = width / 100 * percent;
            var newHeight = height / 100 * percent;

            return GetThumbNailOfImage(image, (int)newWidth, (int)newHeight);
        }

        public Image GetThumbNailOfImageByMax(Image image, int maxWidth, int maxHeight)
        {
            double width = image.Width;
            double height = image.Height;

            var percent = 100.0;
            var widthPercent = 100.0;
            var heightPercent = 100.0;

            if (width > maxWidth)
            {
                widthPercent = 100.0 / width * maxWidth;
            }

            if (height > maxHeight)
            {
                heightPercent = 100 / height * maxHeight;
            }

            if (widthPercent > heightPercent)
            {
                percent = heightPercent;
            }
            else if (heightPercent > widthPercent)
            {
                percent = widthPercent;
            }
            
            return GetThumbNailOfImageByPercent(image, percent);
        }
    }

    public static class BitmapExtensions
    {
        public static void SaveJPG100(this Bitmap bmp, string filename)
        {
            EncoderParameters encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
            bmp.Save(filename, GetEncoder(ImageFormat.Jpeg), encoderParameters);
        }

        public static void SaveJPG100(this Bitmap bmp, Stream stream)
        {
            EncoderParameters encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
            bmp.Save(stream, GetEncoder(ImageFormat.Jpeg), encoderParameters);
        }

        public static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }

            return null;
        }
    }
}
