﻿using MediaViewerController.Connectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Converters
{
    public static class MediaTypeEnumToMediaTypeClassId
    {
        public static string Convert(MediaType mediaType, clsLocalConfig localConfig)
        {
            switch (mediaType)
            {
                case MediaType.Audio:
                    return localConfig.OItem_type_media_item.GUID;
                case MediaType.Image:
                    return localConfig.OItem_type_images__graphic_.GUID;
                case MediaType.PDF:
                    return localConfig.OItem_type_pdf_documents.GUID;
                case MediaType.Video:
                    return localConfig.OItem_type_media_item.GUID;
            }

            return null;
        }

        
    }
}
