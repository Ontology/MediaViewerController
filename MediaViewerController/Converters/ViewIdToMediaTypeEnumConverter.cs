﻿using MediaViewerController.Connectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerController.Converters
{
    public static class ViewIdToMediaTypeEnumConverter
    {
        private static string viewIdImageItemList = "cf260392e0a24d32a2f2162e9795de0a";
        private static string viewIdAudioItemList = "aad87369aa564a439fe6bd727750e44b";
        private static string viewIdVideoItemList = "0ecd03c9162c45839331260af4336af7";
        private static string viewIdPDFItemList = "77ea69778d4e4d9fbd2ffee8bd122636";

        public static MediaType Convert(string viewId)
        {
            if (viewId == viewIdImageItemList)
            {
                return MediaType.Image;
            }
            
            if (viewId == viewIdAudioItemList)
            {
                return MediaType.Audio;
            }

            if (viewId == viewIdPDFItemList)
            {
                return MediaType.PDF;
            }

            if (viewId == viewIdVideoItemList)
            {
                return MediaType.Video;
            }

            return MediaType.Image;
        }
    }
}
